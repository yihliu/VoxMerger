import matplotlib.pyplot as plt
from skimage.transform import rescale, resize, downscale_local_mean
import numpy as np
from skimage import io
import tifffile as tif
from skimage.filters import threshold_mean, median, gaussian, try_all_threshold, threshold_otsu
import pandas as pd
import scanpy as sc
import os
import anndata
import itertools
from skimage.morphology import disk, area_closing, remove_small_objects
import matplotlib.colors as mcolors
import trimesh
from trimesh.voxel.ops import matrix_to_marching_cubes
from trimesh.visual.color import ColorVisuals
from skimage.measure import marching_cubes
from trimesh.smoothing import filter_taubin
import pymeshfix
from sklearn.metrics import silhouette_score
import seaborn as sns
import hdf5plugin
from scipy.spatial import distance_matrix
import networkx as nx
import ot
# from ipynb.fs.defs.HCR_mask import mask_based_on_dapi
from skimage.measure import label
from scipy import ndimage as ndi
from skimage import measure
from sklearn.neighbors import kneighbors_graph
from scipy import sparse
import igraph as ig
import leidenalg
import cairo
from scipy.cluster import hierarchy
from scipy.spatial.distance import squareform
import matplotlib.patches as mpatches
from scipy.linalg import block_diag
from scipy.sparse import csc_matrix, csr_matrix
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from fa2 import ForceAtlas2
from matplotlib.collections import LineCollection
from scipy.stats import spearmanr
from scipy.stats import ttest_ind, entropy
from skimage.exposure import rescale_intensity
from sklearn.neighbors import NearestNeighbors
from scipy.linalg import sqrtm
from scipy.spatial.distance import pdist, squareform, cdist
from joblib import Parallel, delayed
import heapq
import novosparc
from scipy.stats import zscore
from sklearn.metrics import pairwise_distances
from scipy.stats import gaussian_kde, ks_2samp
from scipy.special import kl_div
from scipy.signal import find_peaks
from scipy.sparse import csc_matrix, save_npz, load_npz

# TODO: clean up the package used

# replace the cmap with cmap used in clusters
# edge_alpha, width, color; node size, alpha
# save plot or not


def draw_graph_with_color(adata, color_param, edge=False, save=False):
    # Check if X_draw_graph_fa exists in adata.obsm
    if 'X_draw_graph_fa' not in adata.obsm_keys():
        raise ValueError("The 'X_draw_graph_fa' key is missing in adata.obsm. "
                         "Please compute the graph layout using the appropriate method first.")

    # Get position data
    positions = adata.obsm['X_draw_graph_fa']

    if all(elem in adata.obs_keys() for elem in color_param):
        color_param_cat = 'obs'
    elif all(elem in adata.var_names for elem in color_param):
        color_param_cat = 'var'
    else:
        raise ValueError("Please do not mix var and obs.")

    if len(color_param) == 1:
        if color_param_cat == 'var':
            # Get gene expressions
            gene_name = color_param
            gene_expressions = adata[:, gene_name].X
            cmap = plt.get_cmap('viridis')
            norm = plt.Normalize(gene_expressions.min(),
                                 gene_expressions.max())
            colors = cmap(norm(gene_expressions))
            colorbar_label = f'Gene Expression ({gene_name})'

            # Create a single subplot
            fig, ax = plt.subplots(1, 1, figsize=(10, 8))

            if edge:
                # Convert sparse matrix to COO format for faster indexing
                coo_matrix = adata.obsp['connectivities_sp'].tocoo()
                row_indices = coo_matrix.row
                col_indices = coo_matrix.col

                # Create a list of edge positions
                edge_positions = np.column_stack(
                    (positions[row_indices], positions[col_indices]))

                # Reshape edge_positions to 2D array
                edge_positions = edge_positions.reshape(-1, 2, 2)

                # Create a LineCollection object for efficient edge plotting
                edges = LineCollection(
                    edge_positions, colors='grey', linewidths=0.05, alpha=0.01)

                # Draw edges
                ax.add_collection(edges)

            # Draw nodes
            ax.scatter(positions[:, 0], positions[:, 1],
                       c=colors, alpha=1, s=5)

            # Draw colorbar
            sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
            sm.set_array([])
            cbar = plt.colorbar(sm, ax=ax, label=colorbar_label)

        else:
            color_values = adata.obs[color_param]
            num_colors = len(np.unique(color_values))
            # Use categorical colormap
            cmap = plt.get_cmap('viridis', num_colors)
            colors = cmap(color_values)
            colorbar_label = None
            norm = None

            # Create a single subplot
            fig, ax = plt.subplots(1, 1, figsize=(10, 8))

            if edge:
                # Convert sparse matrix to COO format for faster indexing
                coo_matrix = adata.obsp['connectivities_sp'].tocoo()
                row_indices = coo_matrix.row
                col_indices = coo_matrix.col

                # Create a list of edge positions
                edge_positions = np.column_stack(
                    (positions[row_indices], positions[col_indices]))

                # Reshape edge_positions to 2D array
                edge_positions = edge_positions.reshape(-1, 2, 2)

                # Create a LineCollection object for efficient edge plotting
                edges = LineCollection(
                    edge_positions, colors='grey', linewidths=0.05, alpha=0.01)

                # Draw edges
                ax.add_collection(edges)

            # Draw nodes with cluster membership annotation
            cluster_names = np.unique(color_values)
            for cluster_label in cluster_names:
                cluster_indices = np.where(color_values == cluster_label)[0]
                ax.scatter(positions[cluster_indices, 0], positions[cluster_indices, 1],
                           c=colors[cluster_indices], label=cluster_label, alpha=1, s=5)

            # Display cluster membership annotation on the side
            ax.legend(title=color_param, loc='center left',
                      bbox_to_anchor=(1, 0.5), markerscale=2)

        # Remove x-axis and y-axis labels
        ax.axis('off')

        # Set x-axis and y-axis labels
        ax.set_xlabel('FA1')
        ax.set_ylabel('FA2')

        ax.set_title(color_param[0])

    else:
        # Create subplots for each gene in the list
        num_var_names = len(color_param)
        num_rows = int(num_var_names / 5) + (num_var_names % 5 > 0)
        num_cols = min(num_var_names, 5)
        fig, axes = plt.subplots(
            num_rows, num_cols, figsize=(6*num_cols, 5*num_rows))
        axes = axes.flatten()

        if color_param_cat == 'var':
            # Loop over genes in the list
            for i, gene_name in enumerate(color_param):
                # Get gene expressions
                gene_expressions = adata[:, gene_name].X
                cmap = plt.get_cmap('viridis')
                norm = plt.Normalize(gene_expressions.min(),
                                     gene_expressions.max())
                colors = cmap(norm(gene_expressions))
                colorbar_label = f'Gene Expression ({gene_name})'

                if edge:
                    # Convert sparse matrix to COO format for faster indexing
                    coo_matrix = adata.obsp['connectivities_sp'].tocoo()
                    row_indices = coo_matrix.row
                    col_indices = coo_matrix.col

                    # Create a list of edge positions
                    edge_positions = np.column_stack(
                        (positions[row_indices], positions[col_indices]))

                    # Reshape edge_positions to 2D array
                    edge_positions = edge_positions.reshape(-1, 2, 2)

                    # Create a LineCollection object for efficient edge plotting
                    edges = LineCollection(
                        edge_positions, colors='grey', linewidths=0.05, alpha=0.01)

                    # Draw edges
                    axes[i].add_collection(edges)

                # Draw nodes
                axes[i].scatter(positions[:, 0], positions[:, 1],
                                c=colors, alpha=1, s=5)

                # Draw colorbar
                sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
                sm.set_array([])
                cbar = plt.colorbar(sm, ax=axes[i], label=colorbar_label)

                # Remove x-axis and y-axis labels
                axes[i].axis('off')

                # Set x-axis and y-axis labels
                axes[i].set_xlabel('FA1')
                axes[i].set_ylabel('FA2')

                axes[i].set_title(gene_name)

        else:
            for i, obs_param in enumerate(color_param):
                color_values = adata.obs[obs_param]
                num_colors = len(np.unique(color_values))
                # Use categorical colormap
                cmap = plt.get_cmap('viridis', num_colors)
                colors = cmap(color_values)
                colorbar_label = None
                norm = None

                if edge:
                    # Convert sparse matrix to COO format for faster indexing
                    coo_matrix = adata.obsp['connectivities_sp'].tocoo()
                    row_indices = coo_matrix.row
                    col_indices = coo_matrix.col

                    # Create a list of edge positions
                    edge_positions = np.column_stack(
                        (positions[row_indices], positions[col_indices]))

                    # Reshape edge_positions to 2D array
                    edge_positions = edge_positions.reshape(-1, 2, 2)

                    # Create a LineCollection object for efficient edge plotting
                    edges = LineCollection(
                        edge_positions, colors='grey', linewidths=0.05, alpha=0.01)

                    # Draw edges
                    axes[i].add_collection(edges)

                # Draw nodes with cluster membership annotation
                cluster_names = np.unique(color_values)
                for cluster_label in cluster_names:
                    cluster_indices = np.where(
                        color_values == cluster_label)[0]
                    axes[i].scatter(positions[cluster_indices, 0], positions[cluster_indices, 1],
                                    c=colors[cluster_indices], label=cluster_label, alpha=1, s=5)

                # Display cluster membership annotation on the side
                axes[i].legend(title=obs_param, loc='center left',
                               bbox_to_anchor=(1, 0.5), markerscale=2)

                # Remove x-axis and y-axis labels
                axes[i].axis('off')

                # Set x-axis and y-axis labels
                axes[i].set_xlabel('FA1')
                axes[i].set_ylabel('FA2')

                axes[i].set_title(obs_param)
        # Remove any extra subplots if the number of genes is not divisible by 5
        if num_var_names % 5 != 0:
            for j in range(num_var_names, num_rows * num_cols):
                fig.delaxes(axes[j])

    plt.tight_layout()
    if save != False:
        plt.savefig('results/meta_organoid{}'.format(save))
        # plt.show()
    else:
        plt.show()


def complex_array_pdist(x, y=None):
    """
    Computes pairwise Euclidean distances for arrays of complex numbers.

    Parameters:
    - x (numpy.ndarray): A 2D array where each row represents an array of complex numbers.
    - y (numpy.ndarray, optional): Another 2D array of complex numbers. If provided, the function computes distances 
        between rows of `x` and `y`. If not provided, the function computes pairwise distances within `x`. Default is None.

    Returns:
    - numpy.ndarray: If `y` is not provided, returns pairwise distances in a condensed form. If `y` is provided, returns 
        pairwise distances in a matrix form where rows represent elements from `x` and columns represent elements from `y`.

    Notes:
    The function computes the Euclidean distance between two arrays of complex numbers by treating the real and imaginary 
    parts as separate dimensions. The distance is then computed using the formula:
    \[ \text{distance} = \sqrt{\text{sum}((\text{real}_1 - \text{real}_2)^2 + (\text{imag}_1 - \text{imag}_2)^2)} \]
    """
    if y is None:
        n = len(x)
        distances = []

        for i in range(n):
            diff = x[i] - x[i+1:]
            distance = np.sqrt(np.sum(diff.real**2 + diff.imag**2, axis=1))
            distances.append(distance)
        # Concatenate results from all iterations and return condensed matrix
        return squareform(np.concatenate(distances))

    else:
        n, m = len(x), len(y)
        distances = np.zeros((n, m))
        for i in range(n):
            diff = x[i] - y
            distances[i, :] = np.sqrt(
                np.sum(diff.real**2 + diff.imag**2, axis=1))

        return distances


def knn_adjacency_matrix_from_distance(distances, k=50):
    """
    Constructs a k-nearest neighbors (k-NN) adjacency matrix from a distance matrix.

    Parameters:
    - distances (numpy.ndarray): A 2D square matrix representing pairwise distances between points.
    - k (int, optional): The number of nearest neighbors to consider for each point. Default is 50.

    Returns:
    - scipy.sparse.csc_matrix: A sparse adjacency matrix in Compressed Sparse Column (CSC) format. For each point, 
      the matrix has ones in positions corresponding to its k-nearest neighbors and zeros elsewhere.

    Notes:
    The function constructs the adjacency matrix by iterating over each point in the distance matrix and selecting 
    its k-nearest neighbors (excluding itself). The resulting adjacency matrix is a binary matrix where a cell at 
    position (i, j) is 1 if point j is among the k-nearest neighbors of point i, and 0 otherwise.
    """
    num_points = distances.shape[0]
    rows = []
    cols = []
    for i in range(num_points):
        # Exclude the diagonal (distance to self is always 0)
        neighbors = heapq.nsmallest(
            k+1, range(num_points), key=lambda j: distances[i, j])[1:]
        rows.extend(neighbors)
        cols.extend([i] * k)

    data = np.ones(len(rows))
    adjacency_matrix = csc_matrix(
        (data, (rows, cols)), shape=(num_points, num_points))

    return adjacency_matrix


# replace NovoSpaRc plot_exp_loc_dists, because it cannot save fig
def plot_exp_loc_dists_(exp, locations, tit_size=15, nbins=10, save=None):
    """
    Plots the relationship between expression distances and physical distances across spatial locations.

    Parameters:
    - exp (numpy.ndarray): A matrix representing spatial expression across locations, with dimensions [locations x genes].
    - locations (numpy.ndarray): A matrix representing spatial coordinates of locations, with dimensions [locations x dimensions].
    - tit_size (int, optional): Font size for plot title and axis labels. Default is 15.
    - nbins (int, optional): Number of bins to use when grouping physical distances for box plot visualization. Default is 10.
    - save (str, optional): Filename to save the resulting plot. If None, the plot is not saved. Default is None.

    Notes:
    The function first computes pairwise distances for both the expression data and the physical locations.
    It then groups the physical distances into bins and generates a box plot to visualize the distribution of expression distances 
    within each physical distance bin.

    The resulting plot provides insights into how expression similarity varies as a function of spatial proximity.
    """
    locs_exp_dist = squareform(pdist(exp))
    locs_phys_dist = squareform(pdist(locations))
    exp_col = 'Locations expression distance'
    phys_col = 'Locations physical distance'
    phys_col_bin = 'Locations physical distance bin'

    df = pd.DataFrame({exp_col: locs_exp_dist.flatten(),
                      phys_col: locs_phys_dist.flatten()})

    lower, higher = int(df[phys_col].min()), int(np.ceil(df[phys_col].max()))
    # the number of edges is 8
    edges = range(lower, higher, int((higher - lower)/nbins))
    lbs = ['(%d, %d]' % (edges[i], edges[i+1]) for i in range(len(edges)-1)]
    df[phys_col_bin] = pd.cut(df[phys_col], bins=nbins,
                              labels=lbs, include_lowest=True)

    df.boxplot(column=[exp_col], by=[phys_col_bin],
               grid=False, fontsize=tit_size)
    plt.xticks(rotation=45, ha='right')
    plt.ylabel(exp_col, size=tit_size)
    plt.xlabel(phys_col, size=tit_size)
    plt.title('')
    if save is not None:
        plt.savefig('results/{}'.format(save))
    # plt.show()
    # plt.close()


def embedding_(dataset, color, title=None, size_x=None, size_y=None,
               pt_size=10, tit_size=15, dpi=100, save=None):
    """
    Visualizes fields (color) of a Scanpy AnnData object on spatial coordinates in a scatter plot.

    Parameters:
    - dataset (Scanpy AnnData): A Scanpy AnnData object containing a 'spatial' matrix in its obsm attribute, 
      which represents the spatial coordinates of the tissue.
    - color (list of str): A list of fields, which can be either gene names or columns from the `obs` attribute of the dataset,
      to use for coloring the scatter plot.
    - title (list of str, optional): Titles for each subplot. If not provided, the values from `color` are used as titles. Default is None.
    - size_x (float, optional): Width of the entire figure in inches. Default is dynamically calculated based on the number of colors.
    - size_y (float, optional): Height of the entire figure in inches. Default is dynamically calculated based on the number of colors.
    - pt_size (int, optional): Point size for scatter plots. Default is 10.
    - tit_size (int, optional): Font size for subplot titles. Default is 15.
    - dpi (int, optional): Resolution of the resulting figure in dots per inch. Default is 100.
    - save (str, optional): Filename to save the resulting plot. If None, the plot is not saved. Default is None.

    Notes:
    The function generates a scatter plot for each field provided in the `color` parameter. Each plot visualizes the spatial 
    distribution of a specific field (gene or metadata column). The fields are represented as colors in the scatter plot, 
    with each point corresponding to a location in the tissue and its color representing the expression or metadata value 
    for that location.

    The resulting figure is organized in a grid, with up to three plots per row. The size and layout of the figure are adjustable 
    via the function's parameters.
    """
    title = color if title is None else title
    ncolor = len(color)
    per_row = 3
    per_row = ncolor if ncolor < per_row else per_row
    nrows = int(np.ceil(ncolor / per_row))
    size_x = 5 * per_row if size_x is None else size_x
    size_y = 3 * nrows if size_y is None else size_y
    fig, axs = plt.subplots(nrows, per_row, figsize=(size_x, size_y), dpi=dpi)
    xy = dataset.obsm['spatial']
    x = xy[:, 0]
    y = xy[:, 1] if xy.shape[1] > 1 else np.ones_like(x)
    axs = axs.flatten() if type(axs) == np.ndarray else [axs]
    for ax in axs:
        ax.axis('off')

    for i, g in enumerate(color):
        if g in dataset.var_names:
            values = dataset[:, g].X
        elif g in dataset.obs.columns:
            values = dataset.obs[g]
        else:
            continue
        axs[i].scatter(x, y, c=np.array(values), s=pt_size)
        axs[i].set_title(title[i], size=tit_size)
    if save is not None:
        plt.savefig('results/{}'.format(save))
    # plt.show()
    # plt.tight_layout()


def plot_clusters_with_entropy_annotation(proportions, title, file_name_format,cmap=None):
    """
    Generates a stacked bar plot to visualize cluster proportions with entropy annotations.

    Parameters:
    - proportions (pandas.DataFrame): A DataFrame where each row represents an organoid and columns represent cluster proportions.
      There should be an additional 'entropy' column with entropy values for each organoid.
    - title (str): Title for the resulting plot.
    - file_name_format (str): The file name (and path) to save the resulting plot.
    - cmap (str): The color map used for plot.

    Notes:
    The function generates a stacked bar plot where each bar represents an organoid and the height of each segment within a bar 
    corresponds to the proportion of a cluster. The entropy values for each organoid are annotated at the top of each bar.

    This visualization provides insights into the distribution of clusters across organoids and the diversity (entropy) of 
    cluster assignments for each organoid.
    """

    fig, ax = plt.subplots(figsize=(10, 6))

    proportions.drop('entropy', axis=1).plot(kind='bar', stacked=True, ax=ax, colormap=cmap)
    ax.set_ylabel('Proportion')

    # Annotate bars with the entropy value
    for idx, value in enumerate(proportions['entropy']):
        ax.text(idx, 1, f"H {value:.2f}",
                ha='center', va='bottom', fontsize=10)

    ax.legend(loc='upper left', bbox_to_anchor=(1, 1), title='cluster')
    plt.title(title)
    plt.xlabel('Organoid Name')
    plt.xticks(rotation=45)
    plt.tight_layout()
    plt.savefig(file_name_format)
    # plt.show()




os.chdir("/cluster/project/treutlein/USERS/yihliu")
# general information
'''
adata_list_order = ["HCR18_D15_pos1", "HCR18_D15_pos2",
                    "HCR18_D15_pos3", "HCR18_D15_pos4", "HCR17_D15_pos3", "HCR17_D15_pos4"]
ram = [500, 500, 500, 500, 500]
weights_coord = [0, 0.1, 0.2, 0.3, 0.4]
weights_niche = [0, 0.1, 0.2, 0.3, 0.4]
'''

adata_list = [file for file in os.listdir(os.getcwd()) if file.startswith(
    "HCR") and file.endswith("preprocessed_thresh0.2_0.0625.h5ad")]

weights_coord = [0.1]
weights_niche = [0.1]
divide_ratio = 10  # depends on the size of the matrix, computational resource

scrna = anndata.read_h5ad('scrna.h5ad')


# For analysis of organoids in two conditions
'''
the order is as following
HCR18_D15_pos3 and HCR18_D15_pos4
HCR18_D15_pos1 and HCR18_D15_pos2
HCR17_D15_pos3 and HCR17_D15_pos4
'''

adata_ex = anndata.read_h5ad("HCR17_D15_pos4_preprocessed_thresh0.2_0.0625.h5ad")
adata_in = anndata.read_h5ad("HCR17_D15_pos3_preprocessed_thresh0.2_0.0625.h5ad")
hcr = 'HCR17'
# hcr = 'HCR17'
day = 'D15'
pos = 'pos3_pos4'
# pos = 'pos1_pos2'
quantile_ratio = 0.2
rescale_ratio = 0.0625
adata = anndata.concat([adata_ex,adata_in],label='condition',keys=['extra ECM','intrinsic ECM'])


# For analysis of organoids in single condition (we need the for loop for the file)
# for file in adata_list:
for w_coord in weights_coord:
    for w_niche in weights_niche:

        # adata = anndata.read_h5ad(file)
        # hcr = file.split("_")[0]
        # day = file.split("_")[1]
        # pos = file.split("_")[2]
        # quantile_ratio = float(file.split("_")[-2][-3:])
        # rescale_ratio = float(file.split("_")[-1].split("h5ad")[0][:-1])
        
        file_name_format = [hcr, day, pos, w_coord, w_niche, str(
            quantile_ratio), str(rescale_ratio)]
        
        if hcr == 'HCR18' and day == 'D15':
            adata = adata[adata.obs['name'] != "4_3"]
            adata = adata[adata.obs['name'] != "4_4"]

        adata.obs_names_make_unique()
        sc.pp.filter_cells(adata, min_genes=1)
        sc.pp.filter_cells(adata, min_counts=1)
        sc.pp.log1p(adata)
        adata.raw = adata
        sc.pp.scale(adata)

        voxel_x = 1.1838
        voxel_y = 1.1838
        voxel_z = 10

        # distance matrix based on the coordinates (should I consider the difference between the scale of z and x, y)
        voxels = adata.obs_names

        # Split the voxel names into separate dimensions"
        voxel_index = [voxel.strip("\'\'").split("_") for voxel in voxels]

        # Convert this list of lists into a DataFrame"
        voxel_df = pd.DataFrame(voxel_index, columns=[
                                'x', 'y', 'z', 'sample', 'batch'])
        
        voxel_df['name'] = voxel_df['sample'] + '_' + voxel_df['batch']
        # Convert the string coordinates into integers (or floats if necessary)
        for col in ['x', 'y', 'z']:
            # Change to .astype(float) if these are floating point numbers
            voxel_df[col] = voxel_df[col].astype(int)

        # voxel_df['y'] = voxel_df['y'] * voxel_y / voxel_x
        voxel_df['z_real'] = voxel_df['z'] * \
            voxel_z / voxel_x * rescale_ratio

        adata.obs['z'] = voxel_df['z_real'].values

        voxel_df[adata.var_names] = adata.X
        grouped = voxel_df.groupby(['sample', 'batch'])

        coordinates_dfs = [group_df.copy() for _, group_df in grouped]
        # where each row corresponds to a voxel and each column corresponds to a channel.
        n_neighbors = 50
        # Create a graph based on voxel intensities
        intensity_graph = kneighbors_graph(
            adata.X, n_neighbors=n_neighbors, mode='connectivity')
        # intensity_graph = intensity_graph > 0

        # Create a graph based on voxel coordinates within one organoid separately
        coordinate_graphs = []
        coordinate_cov = []

        # Voxel niche
        genes = adata.var_names
        voxel_niches = []
        for df in coordinates_dfs:
            # Only consider the x, y, z columns for the KNN graph
            X = df[['x', 'y', 'z_real']].values

            # Create the coordinate KNN graph
            A = kneighbors_graph(
                X, n_neighbors=n_neighbors, mode='connectivity')

            # Calculate covariance
            # cov = np.cov(X)

            # Calculate voxel niche
            nbrs = NearestNeighbors(n_neighbors=9, algorithm='auto', metric='euclidean').fit(
                df[['x', 'y', 'z_real']])
            distances, indices = nbrs.kneighbors(df[['x', 'y', 'z_real']])

            filtered_indices = [indices[i][dist <= 10]
                                for i, dist in enumerate(distances)]

            voxel_niche = np.zeros((df.shape[0], 9, len(genes)))

            for i, idx in enumerate(filtered_indices):
                # Use iloc here
                voxel_niche[i, :len(idx), :] = df.iloc[idx][genes].values

            voxel_niches.append(voxel_niche)

            # Append the resulting graph to the list
            coordinate_graphs.append(A)
            # coordinate_cov.append(cov)

        # Convert sparse matrices to dense? or just to array? what's the difference
        # is it possible to optimise this step?
        dense_coordinate_graphs = [A.toarray() for A in coordinate_graphs]

        # Create block diagonal matrix
        block_coordinate_graph = block_diag(*dense_coordinate_graphs)

        # block_coordinate_graph = block_coordinate_graph > 0

        voxel_niches_concatenated = np.concatenate(voxel_niches, axis=0)
        total_mean_exp = np.mean(voxel_df[genes])
        # Initialize an array to store the niche covariances
        niche_covariances = np.zeros(
            (voxel_niches_concatenated.shape[0], len(genes), len(genes)))

        # Loop through each row and calculate the covariance
        for i, niche in enumerate(voxel_niches_concatenated):
            deviations = niche - total_mean_exp.values
            cov_matrix = np.dot(
                deviations.T, deviations) / (niche.shape[0] - 1)
            niche_covariances[i] = cov_matrix

        msqr_results = []
        for cov_matrix in niche_covariances:
            # .real.flatten()  # Compute the matrix square root
            msqr_result = sqrtm(cov_matrix)
            msqr_results.append(msqr_result)

        # Convert the MSQR results to a 2D array with shape (number of niches, 5 * 5)
        msqr_array = np.array([msqr_result.flatten()
                                for msqr_result in msqr_results])

        # Divide the array into 5 chunks
        chunk_size = msqr_array.shape[0] // divide_ratio
        pairwise_distances_matrix = np.zeros(
            (msqr_array.shape[0], msqr_array.shape[0]))

        for i in range(divide_ratio):
            # compute diagonal covariance distance
            start = i * chunk_size
            end = start + chunk_size
            if i == (divide_ratio-1):
                end = msqr_array.shape[0]

            chunk = msqr_array[start:end, :]
            # pairwise_distances_chunk = complex_array_pdist(chunk)
            pairwise_distances_matrix[start:end,
                                        start:end] = complex_array_pdist(chunk)

            for j in range(i + 1, divide_ratio):
                start_j = j * chunk_size
                end_j = start_j + chunk_size
                if j == (divide_ratio-1):
                    end_j = msqr_array.shape[0]

                chunk_j = msqr_array[start_j:end_j, :]
                pairwise_distances_cross = complex_array_pdist(
                    chunk, chunk_j)
                pairwise_distances_matrix[start:end,
                                            start_j:end_j] = pairwise_distances_cross
                pairwise_distances_matrix[start_j:end_j,
                                            start:end] = pairwise_distances_cross.T

        # approx 40 mins
        # Assuming you already have the full_matrix from the previous steps

        niche_format = [file_name_format[0], file_name_format[1], file_name_format[2], file_name_format[5], file_name_format[6]]
            
        if os.path.exists('cell_niche_sparse_{}_{}_{}_{}_{}.npz'.format(*niche_format)):
            cell_niche_sparse = load_npz(
                'cell_niche_sparse_{}_{}_{}_{}_{}.npz'.format(*niche_format))
        else:
            k = 50  # or any desired value for k
            cell_niche_sparse = knn_adjacency_matrix_from_distance(
                pairwise_distances_matrix, k)
            save_npz('cell_niche_sparse_{}_{}_{}_{}_{}.npz'.format(*niche_format), cell_niche_sparse)

        # are KNN graphes for both undirected? no, but we should make them undirected
        # Convert intensity_graph to a sparse matrix
        intensity_sparse = csc_matrix(intensity_graph)
        block_coordinate_graph = csc_matrix(block_coordinate_graph)

        # Perform a weighted sum of the adjacency matrices of the two graphs

        # fused_adjacency = w * block_coordinate_graph + (1 - w) * intensity_graph.toarray()
        fused_adjacency = block_coordinate_graph.multiply(
            w_coord) + intensity_sparse.multiply(1-w_coord-w_niche) + cell_niche_sparse.multiply(w_niche)

        # Now `fused_adjacency` is the adjacency matrix of the fused graph
        # the normalization range do not influence the PAGA graph, I have tried 1-10 and 1-100, the PAGA graphs are the same
        # Create a graph based on voxel intensities
        intensity_graph_distances = kneighbors_graph(
            adata.X, n_neighbors=n_neighbors, mode='distance', include_self=False)
        # use the indices from adjacency KNN graph
        intensity_indices = intensity_graph.nonzero()
        # distinguish the (neighbor) voxel with the same value from the voxels that are not neighbors
        intensity_graph_distances[intensity_indices] += 1

        # Normalize the value between 1 and 10
        intensity_min = intensity_graph_distances[intensity_indices].min()
        intensity_max = intensity_graph_distances[intensity_indices].max()
        intensity_scale = 9 / (intensity_max-intensity_min)
        intensity_shift = 1 - intensity_min * intensity_scale

        intensity_graph_distances[intensity_indices] = intensity_graph_distances[intensity_indices] * \
            intensity_scale + intensity_shift

        # Create a graph based on voxel coordinates within one organoid separately
        coordinate_graphs_distances = []
        for df in coordinates_dfs:
            # Only consider the x, y, z columns for the KNN graph
            X = df[['x', 'y', 'z_real']].values

            # Create the KNN graph
            A = kneighbors_graph(
                X, n_neighbors=n_neighbors, mode='distance', include_self=False)

            # Append the resulting graph to the list
            # Normalize between 1 and 10
            A_indices = A.nonzero()
            A_min = A[A_indices].min()
            A_max = A[A_indices].max()
            A_scale = 9 / (A_max-A_min)
            A_shift = 1 - A_min * A_scale

            A[A_indices] = A[A_indices] * A_scale + A_shift
            coordinate_graphs_distances.append(A)

        # Convert sparse matrices to dense
        dense_coordinate_graphs_distances = [
            A.toarray() for A in coordinate_graphs_distances]

        # Create block diagonal matrix
        block_coordinate_graph_distances = block_diag(
            *dense_coordinate_graphs_distances)

        # Cell Niche Distance KNN Graph
        # Extract non-zero row and column indices from the cell_niche_sparse
        rows = cell_niche_sparse.indices
        cols = np.repeat(np.arange(cell_niche_sparse.shape[1]), np.diff(
            cell_niche_sparse.indptr))
        cell_niche_distances = np.zeros(pairwise_distances_matrix.shape)

        # Directly extract the pairwise distances using row and column indices
        cell_niche_distances[rows,
                                cols] = pairwise_distances_matrix[rows, cols]

        # Rescale the distance to 1-10
        niche_min = cell_niche_distances[rows, cols].min()
        niche_max = cell_niche_distances[rows, cols].max()
        niche_scale = 9 / (niche_max-niche_min)
        niche_shift = 1 - niche_min * niche_scale

        cell_niche_distances[rows, cols] = cell_niche_distances[rows,
                                                                cols] * niche_scale + niche_shift

        # are KNN graphes for both undirected? no, but we should make them undirected
        # Convert intensity_graph to a sparse matrix
        intensity_sparse_distances = csc_matrix(intensity_graph_distances)
        block_coordinate_graph_distances = csc_matrix(
            block_coordinate_graph_distances)
        cell_niche_sparse_distances = csc_matrix(cell_niche_distances)

        # Perform a weighted sum of the adjacency matrices of the two graphs

        # fused_adjacency = w * block_coordinate_graph + (1 - w) * intensity_graph.toarray()
        fused_adjacency_distances = block_coordinate_graph_distances.multiply(
            w_coord) + cell_niche_sparse_distances.multiply(w_niche) + intensity_sparse_distances.multiply(1-w_coord-w_niche)
        # clustering with Leiden
        # still need to find a way to speed up this step

        # Initialize an empty graph with the correct number of vertices
        n_vertices = fused_adjacency.shape[0]
        G = ig.Graph(n_vertices)

        # Add edges
        rows, cols = fused_adjacency.nonzero()
        edges = list(zip(rows, cols))
        G.add_edges(edges)

        # Add edge weights
        # Here, we directly access the data of the csc_matrix
        G.es['weight'] = fused_adjacency.data

        # compute the optimal partition with the Leiden algorithm
        leiden_partition = leidenalg.find_partition(
            G, leidenalg.ModularityVertexPartition, weights='weight', seed=42)

        # ig.plot(partition)
        cluster_sizes = leiden_partition.sizes()
        # get cluster memberships and total number of clusters
        membership = np.array(leiden_partition.membership)
        n_clusters = len(np.unique(membership))

        # Convert sparse matrix to CSR format for efficient row slicing
        fused_csr = fused_adjacency.tocsr()

        # Initialize connectivity matrix
        connectivity = np.zeros((n_clusters, n_clusters))

        # calculate connectivity
        for cluster in range(n_clusters):
            # get the nodes in this cluster
            nodes_in_cluster = np.where(membership == cluster)[0]

            # calculate connectivity for each pair of clusters
            for other_cluster in range(n_clusters):
                if cluster != other_cluster:
                    # get the nodes in the other cluster
                    nodes_in_other_cluster = np.where(
                        membership == other_cluster)[0]

                    # calculate connectivity
                    connectivity[cluster, other_cluster] = np.sum(
                        fused_csr[nodes_in_cluster][:, nodes_in_other_cluster])
        refined_connectivity = connectivity + connectivity.T
        # which one should I use?
        # refined_connectivity /= np.add.outer(cluster_sizes, cluster_sizes)

        refined_connectivity /= np.outer(cluster_sizes, cluster_sizes)
        # normalize the connectivity
        # the smaller the value the closer the clusters are
        refined_connectivity = refined_connectivity / \
            np.max(refined_connectivity)
        refined_connectivity = 1 - refined_connectivity
        np.fill_diagonal(refined_connectivity, 0)
        # Convert to condensed distance matrix
        condensed_dist = squareform(refined_connectivity)

        # Cluster number selection: Silhouette method, Akaike Information Criterion (AIC) and Bayesian Information Criterion (BIC)
        # Perform hierarchical clustering
        Z = hierarchy.linkage(
            condensed_dist, method='single', metric='euclidean')

        # Range of number of clusters to try (you can adjust this based on your needs)
        # Total number of data points
        n_clusters = len(refined_connectivity)
        k_values = range(2, n_clusters)

        # Store silhouette scores, AIC, and BIC for each number of clusters
        silhouette_scores = []
        aic_values = []
        bic_values = []

        # Loop over different values of k (number of clusters)
        for k in k_values:
            # Obtain the cluster assignments
            clusters = hierarchy.fcluster(Z, t=k, criterion='maxclust')

            # Calculate AIC and BIC
            n_data_points = len(refined_connectivity)
            cluster_distances = []
            for cluster_id in np.unique(clusters):
                cluster_mask = (clusters == cluster_id)
                cluster_data = refined_connectivity[cluster_mask,
                                                    :][:, cluster_mask]
                cluster_dist = np.sum(cluster_data)
                cluster_distances.append(cluster_dist)

            num_parameters = k

            aic = n_data_points * \
                np.log(np.sum(cluster_distances)) + 2 * num_parameters
            bic = n_data_points * \
                np.log(np.sum(cluster_distances)) + \
                num_parameters * np.log(n_data_points)

            aic_values.append(aic)
            bic_values.append(bic)

            if len(np.unique(clusters)) == 1:
                silhouette_scores.append(np.nan)
                continue
            # Compute the silhouette score for the current number of clusters
            silhouette_avg = silhouette_score(
                refined_connectivity, clusters, metric='precomputed')
            silhouette_scores.append(silhouette_avg)


        # Plot the silhouette scores, AIC, and BIC
        plt.figure(figsize=(12, 6))
        plt.subplot(1, 2, 1)
        plt.plot(k_values, silhouette_scores, marker='o')
        plt.xlabel('Number of Clusters (k)')
        plt.ylabel('Silhouette Score')
        plt.title('Silhouette Method for Hierarchical Clustering')
        plt.xlim(min(k_values), max(k_values))

        plt.subplot(1, 2, 2)
        plt.plot(k_values, aic_values, marker='o', label='AIC')
        plt.plot(k_values, bic_values, marker='o', label='BIC')
        plt.xlabel('Number of Clusters (k)')
        plt.ylabel('Information Criterion')
        plt.title('AIC and BIC for Hierarchical Clustering')
        plt.legend()
        plt.xlim(min(k_values), max(k_values))

        plt.tight_layout()
        plt.savefig(
            'results/clustering_info_criteria_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        # plt.show()

        # Specify the desired number of clusters
        # Replace with your desired number of clusters
        m = k_values[np.nanargmax(silhouette_scores)]

        # Obtain the cluster assignments
        clusters = hierarchy.fcluster(Z, t=m, criterion='maxclust')
        clusters -= 1
        # Plot the dendrogram
        plt.figure(figsize=(10, 6))
        dn = hierarchy.dendrogram(Z)
        plt.xlabel('Leiden Clusters')
        plt.ylabel('Distance')
        plt.title('Hierarchical Clustering Dendrogram')
        plt.savefig(
            'results/hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        # plt.show()

        # Print the cluster assignments
        # print("Cluster Assignments:", clusters)

        # hierarchy : leiden dictionary
        clusters_dict = {}
        for i, cluster in enumerate(clusters):
            if cluster not in clusters_dict:
                clusters_dict[cluster] = []
            clusters_dict[cluster].append(i)

        # leiden : voxel (index) dictionary
        leiden_clusters_dict = {}
        for i, leiden_cluster in enumerate(leiden_partition.membership):
            if leiden_cluster not in leiden_clusters_dict:
                leiden_clusters_dict[leiden_cluster] = []
            leiden_clusters_dict[leiden_cluster].append(i)

        # New dictionary for hierarchy to voxel (index) relationship
        hierarchy_voxel_dict = {}

        # Iterate over clusters_dict
        for hierarchy_cluster, leiden_clusters in clusters_dict.items():
            voxel_indices = []

            # Iterate over leiden_clusters in each hierarchy
            for leiden_cluster in leiden_clusters:
                # if leiden_cluster in leiden_clusters_dict:
                voxel_indices.extend(leiden_clusters_dict[leiden_cluster])

            # Add hierarchy and corresponding voxel indices to hierarchy_voxel_dict
            hierarchy_voxel_dict[hierarchy_cluster] = voxel_indices

        # 1. Determine N
        N = len(np.unique(list(hierarchy_voxel_dict.keys())))

        # Check if N exceeds the available colors in 'tab10'
        if N > 10:
            colortab = plt.cm.get_cmap('tab20', 20)

        # 2. Get the colormap and extract the first N colors
        else:
            colortab = plt.cm.get_cmap('tab10', 10) 
        newcolors = colortab(np.arange(N))  # Extract the first N colors
        newcmp = ListedColormap(newcolors)

        # 3. Assign colors to each voxel index based on hierarchy
        max_index = max(max(v) for v in hierarchy_voxel_dict.values())
        colors = np.zeros(max_index + 1)
        for hierarchy_cluster, voxel_indices in hierarchy_voxel_dict.items():
            color_index = int(hierarchy_cluster)
            # Assuming colors is an already initialized array
            colors[voxel_indices] = color_index

        # 4. Repeat it  for Leiden clusters
        newcolors_leiden = colortab(np.linspace(0, 1, len(clusters)))
        newcmp_leiden = ListedColormap(newcolors_leiden)

        # creat colors based on Leiden results
        colors_leiden = np.zeros(len(colors))

        for leiden_cluster, voxel_indices in leiden_clusters_dict.items():
            color_index = int(leiden_cluster)
            # rgb_index = newcmp(int(color_index/m*newcmp.N+1))[:-1]
            colors_leiden[voxel_indices] = color_index

        # Visualize the hierarchy clusters
        # Get the unique Z values in voxel_df and sort them in ascending order
        unique_z = voxel_df['z'].unique()
        unique_z.sort()
        # unique_z = unique_z / voxel_z * voxel_x / rescale_ratio
        unique_z = [z for z in unique_z if z % 5 == 0]

        reps = voxel_df['name'].unique()

        # Create a subplot for each unique Z value, arranging the subplots vertically
        n_rows = len(unique_z)
        n_cols = len(reps)

        fig, axes = plt.subplots(
            n_rows, n_cols, figsize=(5*n_cols+2, 5*n_rows))

        # Create a list to store the color patches for the legend
        color_patches = []

        for i, z in enumerate(unique_z):
            # Select only the rows in voxel_df where 'z' is the current Z value
            plane_df = voxel_df[voxel_df['z'] == z]
            for j, b in enumerate(reps):
                plane_batch_df = plane_df[plane_df['name'] == b]
                # Create a scatter plot for the current Z plane, with the color of each point determined by the cluster membership
                c = [newcmp(int(colors[i]))[:-1]
                        for i in plane_batch_df.index]
                scatter = axes[i, j].scatter(
                    plane_batch_df['x'], plane_batch_df['y'], c=c, s=10)

                axes[i, j].set_title(f'Hierarchy Clustering (Z = {z})')
                # you should save this information from the image
                axes[i, j].set_xlim([0, int(1024*rescale_ratio)])
                axes[i, j].set_ylim([int(1024*rescale_ratio), 0])
                plt.gca().invert_yaxis()

        # Create color patches for the legend
        for i in range(len(set(clusters))):
            color_patches.append(mpatches.Patch(
                color=newcmp(i), label=f'Cluster {i}'))

        # Add legend to the figure
        fig.legend(handles=color_patches, bbox_to_anchor=(1, 0.8))
        plt.savefig(
            'results/hierarchy_bp_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format), bbox_inches='tight')

        # Visualize the Leiden clusters
        fig, axes = plt.subplots(
            n_rows, n_cols, figsize=(5*n_cols+2, 5*n_rows))

        # Create a list to store the color patches for the legend
        color_patches = []

        for i, z in enumerate(unique_z):
            # Select only the rows in voxel_df where 'z' is the current Z value
            plane_df = voxel_df[voxel_df['z'] == z]
            for j, b in enumerate(reps):
                plane_batch_df = plane_df[plane_df['name'] == b]
                # Create a scatter plot for the current Z plane, with the color of each point determined by the cluster membership
                c = [newcmp_leiden(int(colors_leiden[i]))[:-1]
                        for i in plane_batch_df.index]
                scatter = axes[i, j].scatter(
                    plane_batch_df['x'], plane_batch_df['y'], c=c, s=10)

                axes[i, j].set_title(f'Leiden Clustering (Z = {z})')
                # you should save this information from the image
                axes[i, j].set_xlim([0, int(1024*rescale_ratio)])
                axes[i, j].set_ylim([int(1024*rescale_ratio), 0])
                plt.gca().invert_yaxis()

        # Create color patches for the legend
        for i in range(len(clusters)):
            color_patches.append(mpatches.Patch(
                color=newcmp_leiden(i), label=f'Cluster {i}'))

        # Add legend to the figure
        fig.legend(handles=color_patches, bbox_to_anchor=(1, 0.8))
        plt.savefig(
            'results/leiden_bp_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format), bbox_inches='tight')

        # PAGA based on leiden
        leiden_cat = pd.Series(
            leiden_partition.membership).astype('category')
        # leiden_cat = leiden_cat.astype('category')

        adata.obs['leiden'] = leiden_cat.values.astype(str)
        adata.uns['neighbors_sp'] = {'connectivities_key': 'connectivities_sp',
                                        'distances_key': 'distances_sp',
                                        'params': {'n_neighbors': n_neighbors,
                                                # 'method': 'umap',
                                                'random_state': 0,
                                                'metric': 'euclidean'}}
        adata.obsp['connectivities_sp'] = fused_adjacency
        adata.obsp['distances_sp'] = fused_adjacency_distances

        adata.uns['leiden_colors'] = [mcolors.rgb2hex(
            newcmp_leiden.colors[i, :-1]) for i in range(newcmp_leiden.N)]
        sc.tl.paga(adata, neighbors_key='neighbors_sp')
        sc.pl.paga(adata, node_size_scale=5, node_size_power=1.5,
                    save='_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.paga(adata, color='name', node_size_scale=5, node_size_power=1.5,
                    save='_leiden_name_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        for i in adata.var_names:
            sc.pl.paga(adata, color=i, node_size_scale=5, node_size_power=1.5,
                        save='_leiden_genes_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(i,*file_name_format))

        # PAGA based on Hierarchy
        hierarchy_cat = np.zeros(voxel_df.shape[0]).astype(np.int64)
        for i, v in hierarchy_voxel_dict.items():
            hierarchy_cat[v] = int(i)
        hierarchy_cat = pd.Series(hierarchy_cat).astype('category')

        adata.obs['hierarchy'] = hierarchy_cat.values.astype(str)
        adata.uns['hierarchy_colors'] = [mcolors.rgb2hex(
            newcmp.colors[i, :-1]) for i in range(m)]

        # define the name to save the paga
        sc.tl.paga(adata, groups='hierarchy', neighbors_key='neighbors_sp')
        sc.pl.paga(adata, node_size_scale=5, node_size_power=1.5,
                    save='_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.paga(adata, color='name', node_size_scale=5, node_size_power=1.5,
                    save='_hierarchy_name_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        # TODO: could return an error here, we can use for loop to avoid it
        for i in adata.var_names:
            sc.pl.paga(adata, color=i, node_size_scale=5, node_size_power=1.5,
                        save='_hierarchy_genes_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(i,*file_name_format))

        # Forced directed layout
        forceatlas2 = ForceAtlas2(
            # Behavior alternatives
            outboundAttractionDistribution=False,  # Dissuade hubs
            linLogMode=False,  # NOT IMPLEMENTED
            adjustSizes=False,  # Prevent overlap (NOT IMPLEMENTED)
            edgeWeightInfluence=1.0,
            # Performance
            jitterTolerance=1.0,  # Tolerance
            barnesHutOptimize=True,
            barnesHutTheta=1.2,
            multiThreaded=False,  # NOT IMPLEMENTED
            # Tuning
            scalingRatio=2.0,
            strongGravityMode=False,
            gravity=1.0,
            # Log
            verbose=False,
        )
        np.random.seed(0)
        init_coords = np.random.random((fused_adjacency.shape[0], 2))
        iterations = 100
        positions = forceatlas2.forceatlas2(
            fused_adjacency, pos=init_coords, iterations=iterations)
        positions = np.array(positions)
        adata.uns['draw_graph'] = {}
        adata.uns['draw_graph']['params'] = dict(
            layout='fa', random_state=0)
        key_added = f'X_draw_graph_fa'
        adata.obsm[key_added] = positions
        sc.pl.draw_graph(adata, color=adata.var_names, show=False, neighbors_key='neighbors_sp',
                            ncols=5, save='_FDG_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.draw_graph(adata, color='leiden', show=False, neighbors_key='neighbors_sp',
                            ncols=5, save='_FDG_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.draw_graph(adata, color='hierarchy', show=False, neighbors_key='neighbors_sp',
                            ncols=5, save='_FDG_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.draw_graph(adata, color='z', show=False, neighbors_key='neighbors_sp',
                            ncols=5, save='_FDG_z_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.draw_graph(adata, color='name', show=False, neighbors_key='neighbors_sp',
                            ncols=5, save='_FDG_name_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.draw_graph(adata, color='condition', show=False, neighbors_key='neighbors_sp',
                            ncols=5, save='_FDG_condition_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

        # Meta Organoid
        # Sample RGB values
        # in matplot lime has rgb (0,1,0) according to X11/CSS4
        # I need different weights for different colors, otherwise I only see purple
        # we'd better put this color_weight setting to the place where we can tune it without running all the code again
        hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
        # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']

        # Color weights
        # color_weights = [5,0,0,30,0]
        color_weights = [0.7, 0.7, 0.7, 0.7, 0.7]

        rgb_values = []
        for color_name, color_weight in zip(hcr_colors, color_weights):
            if color_name == 'green':
                color_name = 'lime'
            elif color_name == 'grey':
                color_name = 'white'
            # Extract the RGB values, ignoring alpha (transparency)
            rgb_tuple = mcolors.to_rgba(color_name)[:3]
            # print(rgb_tuple,color_weight)
            rgb_tuple = np.array(rgb_tuple)*color_weight
            # rgb_8bit = rgb_to_8bit(rgb_tuple)
            rgb_values.append(rgb_tuple)

        # Perform element-wise multiplication and summation using broadcasting
        result = adata.raw.X.copy()
        # Get minimum and maximum values for each column
        min_values = np.min(result, axis=0)
        max_values = np.max(result, axis=0)

        # Normalize each column to [0, 1]
        result = (result - min_values) / (max_values - min_values)


        for i in range(result.shape[1]):
            p2, p98 = np.percentile(result[:, i], (2, 98))
            result[:, i] = rescale_intensity(
                result[:, i], in_range=(p2, p98))

        result = result[:, :, np.newaxis] * np.array(rgb_values)

        # Sum along the appropriate axis to get the new RGB values for each row
        fiji_colors = np.sum(result, axis=1)  # this step create new color

        fiji_colors = np.clip(fiji_colors, 0, 1)
        fiji_colors_hex = [mcolors.to_hex(rgb) for rgb in fiji_colors]
        adata.obs['meta_organoid'] = fiji_colors_hex
        # Get position data
        positions = adata.obsm['X_draw_graph_fa']

        colors = adata.obs['meta_organoid']

        # Create a single subplot
        fig, ax = plt.subplots(1, 1, figsize=(10, 8), facecolor='black')

        # # if edge:
        # # Convert sparse matrix to COO format for faster indexing
        # coo_matrix = adata.obsp['connectivities_sp'].tocoo()
        # row_indices = coo_matrix.row
        # col_indices = coo_matrix.col

        # # Create a list of edge positions
        # edge_positions = np.column_stack(
        #     (positions[row_indices], positions[col_indices]))

        # # Reshape edge_positions to 2D array
        # edge_positions = edge_positions.reshape(-1, 2, 2)

        # # Create a LineCollection object for efficient edge plotting
        # edges = LineCollection(
        #     edge_positions, colors='white', linewidths=0.05, alpha=0.01)

        # # Draw edges
        # ax.add_collection(edges)

        # Draw nodes
        ax.scatter(positions[:, 0], positions[:, 1],
                    c=colors, alpha=1, s=5)

        # Set x-axis and y-axis labels
        ax.set_xlabel('FA1', color='white', fontsize=12)
        ax.set_ylabel('FA2', color='white', fontsize=12)

        # Set the plot title
        # ax.set_title('Meta Organoid', color='white', fontsize=14,fontweight='bold',x=0.01,y=1)

        # Add text in a different color to the top left of the plot
        # hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
        # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']
        for i, (text_color, text_content) in enumerate(zip(hcr_colors, adata.var_names)):
            if text_color == 'green':
                text_color = 'lime'
        #     elif text_color == 'grey':
        #         text_color = 'white'
        # white can also be the case the voxle has very high expression level
            plt.text(0.99, 1-0.03*i, text_content, transform=ax.transAxes,
                        color=text_color, fontsize=14, fontweight='bold')

        # Hide the axes
        ax.axis('off')

        # Save the plot
        plt.savefig('results/meta_organoid_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format), facecolor='black')
        plt.close()

        # Maximum projection of meta-organoids
        max_color_indices = np.argmax(np.max(result,axis=2),axis=1)

        # Use these indices to pick the corresponding RGB values
        fiji_colors = np.array([rgb_values[index] for index in max_color_indices])

        # Clip values to be within [0, 1]
        fiji_colors = np.clip(fiji_colors, 0, 1)

        # Convert to hex
        fiji_colors_hex = [mcolors.to_hex(rgb) for rgb in fiji_colors]
        adata.obs['meta_organoid_max'] = fiji_colors_hex

        colors = adata.obs['meta_organoid_max']

        # Create a single subplot
        fig, ax = plt.subplots(1, 1, figsize=(10, 8), facecolor='black')

        # Draw nodes
        ax.scatter(positions[:, 0], positions[:, 1],
                    c=colors, alpha=1, s=5)

        # Set x-axis and y-axis labels
        ax.set_xlabel('FA1', color='white', fontsize=12)
        ax.set_ylabel('FA2', color='white', fontsize=12)

        for i, (text_color, text_content) in enumerate(zip(hcr_colors, adata.var_names)):
            if text_color == 'green':
                text_color = 'lime'
        #     elif text_color == 'grey':
        #         text_color = 'white'
        # white can also be the case the voxle has very high expression level
            plt.text(0.99, 1-0.03*i, text_content, transform=ax.transAxes,
                        color=text_color, fontsize=14, fontweight='bold')

        # Hide the axes
        ax.axis('off')

        # Save the plot
        plt.savefig('results/meta_organoid_max_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format), facecolor='black')
        plt.close()

        # for each gene save the meta-organoid
        for idx_color in range(len(adata.var_names)):
            # Sample RGB values
            # in matplot lime has rgb (0,1,0) according to X11/CSS4
            # I need different weights for different colors, otherwise I only see purple
            # we'd better put this color_weight setting to the place where we can tune it without running all the code again
            hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
            # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']

            # Color weights
            # color_weights = [5,0,0,30,0]
            color_weights = [0, 0, 0, 0, 0]
            color_weights[idx_color] = 1

            rgb_values = []
            for color_name, color_weight in zip(hcr_colors, color_weights):
                if color_name == 'green':
                    color_name = 'lime'
                elif color_name == 'grey':
                    color_name = 'white'
                # Extract the RGB values, ignoring alpha (transparency)
                rgb_tuple = mcolors.to_rgba(color_name)[:3]
                # print(rgb_tuple,color_weight)
                rgb_tuple = np.array(rgb_tuple)*color_weight
                # rgb_8bit = rgb_to_8bit(rgb_tuple)
                rgb_values.append(rgb_tuple)

            # Perform element-wise multiplication and summation using broadcasting

            result = adata.raw.X.copy()
            # Get minimum and maximum values for each column
            min_values = np.min(result, axis=0)
            max_values = np.max(result, axis=0)

            # Normalize each column to [0, 1]
            result = (result - min_values) / (max_values - min_values)


            for i in range(result.shape[1]):
                p2, p98 = np.percentile(result[:, i], (2, 98))
                result[:, i] = rescale_intensity(
                    result[:, i], in_range=(p2, p98))

            result = result[:, :, np.newaxis] * np.array(rgb_values)


            # Sum along the appropriate axis to get the new RGB values for each row
            fiji_colors = np.sum(result, axis=1)
            fiji_colors = np.clip(fiji_colors, 0, 1)
            fiji_colors_hex = [mcolors.to_hex(rgb) for rgb in fiji_colors]

            adata.obs['meta_organoid'] = fiji_colors_hex

            # Get position data
            positions = adata.obsm['X_draw_graph_fa']

            colors = adata.obs['meta_organoid']

            # Create a single subplot
            fig, ax = plt.subplots(
                1, 1, figsize=(10, 8), facecolor='black')

            # Draw nodes
            ax.scatter(positions[:, 0], positions[:, 1],
                        c=colors, alpha=1, s=5)

            # Set x-axis and y-axis labels
            ax.set_xlabel('FA1', color='white', fontsize=12)
            ax.set_ylabel('FA2', color='white', fontsize=12)

            # Set the plot title
            # ax.set_title('Meta Organoid', color='white', fontsize=14,fontweight='bold',x=0.01,y=1)

            # Add text in a different color to the top left of the plot
            # hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
            # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']
            text_color = hcr_colors[idx_color]
            if text_color == 'green':
                text_color = 'lime'
            text_content = adata.var_names[idx_color]
            plt.text(0.97, 1-0.03, text_content, transform=ax.transAxes,
                        color=text_color, fontsize=14, fontweight='bold')

            # Hide the axes
            ax.axis('off')

            # Save the plot
            plt.savefig('results/meta_organoid_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
                text_content, *file_name_format), facecolor='black')
            plt.close()

        # best color weight searching
        for w_color in range(1, 10, 1):
            w_color = w_color/10.
            w_color = w_color * 3
            # Sample RGB values
            # in matplot lime has rgb (0,1,0) according to X11/CSS4
            # I need different weights for different colors, otherwise I only see purple
            # we'd better put this color_weight setting to the place where we can tune it without running all the code again
            hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
            # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']

            # Color weights
            # color_weights = [5,0,0,30,0]
            color_weights = [w_color, w_color, w_color, w_color, w_color]

            rgb_values = []
            for color_name, color_weight in zip(hcr_colors, color_weights):
                if color_name == 'green':
                    color_name = 'lime'
                elif color_name == 'grey':
                    color_name = 'white'
                # Extract the RGB values, ignoring alpha (transparency)
                rgb_tuple = mcolors.to_rgba(color_name)[:3]
                # print(rgb_tuple,color_weight)
                rgb_tuple = np.array(rgb_tuple)*color_weight
                # rgb_8bit = rgb_to_8bit(rgb_tuple)
                rgb_values.append(rgb_tuple)

            # Perform element-wise multiplication and summation using broadcasting

            result = adata.raw.X.copy()
            # Get minimum and maximum values for each column
            min_values = np.min(result, axis=0)
            max_values = np.max(result, axis=0)

            # Normalize each column to [0, 1]
            result = (result - min_values) / (max_values - min_values)

            for i in range(result.shape[1]):
                p2, p98 = np.percentile(result[:, i], (2, 98))
                result[:, i] = rescale_intensity(
                    result[:, i], in_range=(p2, p98))

            result = result[:, :, np.newaxis] * np.array(rgb_values)

            # Sum along the appropriate axis to get the new RGB values for each row
            fiji_colors = np.sum(result, axis=1)
            fiji_colors = np.clip(fiji_colors, 0, 1)
            fiji_colors_hex = [mcolors.to_hex(rgb) for rgb in fiji_colors]

            adata.obs['meta_organoid'] = fiji_colors_hex

            # Get position data
            positions = adata.obsm['X_draw_graph_fa']

            colors = adata.obs['meta_organoid']

            # Create a single subplot
            fig, ax = plt.subplots(
                1, 1, figsize=(10, 8), facecolor='black')

            # if edge:
            # Convert sparse matrix to COO format for faster indexing
            # coo_matrix = adata.obsp['connectivities_sp'].tocoo()
            # row_indices = coo_matrix.row
            # col_indices = coo_matrix.col

            # # Create a list of edge positions
            # edge_positions = np.column_stack((positions[row_indices], positions[col_indices]))

            # # Reshape edge_positions to 2D array
            # edge_positions = edge_positions.reshape(-1, 2, 2)

            # # Create a LineCollection object for efficient edge plotting
            # edges = LineCollection(edge_positions, colors='white', linewidths=0.05, alpha=0.01)

            # # Draw edges
            # ax.add_collection(edges)

            # Draw nodes
            ax.scatter(positions[:, 0], positions[:, 1],
                        c=colors, alpha=1, s=5)

            # Set x-axis and y-axis labels
            ax.set_xlabel('FA1', color='white', fontsize=12)
            ax.set_ylabel('FA2', color='white', fontsize=12)

            # Set the plot title
            # ax.set_title('Meta Organoid', color='white', fontsize=14,fontweight='bold',x=0.01,y=1)

            # Add text in a different color to the top left of the plot
            # hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
            # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']
            for i, (text_color, text_content) in enumerate(zip(hcr_colors, adata.var_names)):
                if text_color == 'green':
                    text_color = 'lime'
            #     elif text_color == 'grey':
            #         text_color = 'white'
            # white can also be the case the voxle has very high expression level
                plt.text(0.99, 1-0.03*i, text_content, transform=ax.transAxes,
                            color=text_color, fontsize=14, fontweight='bold')

            # Hide the axes
            ax.axis('off')

            # Save the plot
            plt.savefig('results/meta_organoid_{}_{}_{}_{}_{}_{}_{}_colorw{:.1f}.png'.format(
                *file_name_format, w_color), facecolor='black')
            plt.close()


            # Maximum projection of meta-organoids
            max_color_indices = np.argmax(np.max(result,axis=2),axis=1)

            # Use these indices to pick the corresponding RGB values
            fiji_colors = np.array([rgb_values[index] for index in max_color_indices])

            # Clip values to be within [0, 1]
            fiji_colors = np.clip(fiji_colors, 0, 1)

            # Convert to hex
            fiji_colors_hex = [mcolors.to_hex(rgb) for rgb in fiji_colors]
            adata.obs['meta_organoid_max'] = fiji_colors_hex

            colors = adata.obs['meta_organoid_max']

            # Create a single subplot
            fig, ax = plt.subplots(
                1, 1, figsize=(10, 8), facecolor='black')
            
            # Draw nodes
            ax.scatter(positions[:, 0], positions[:, 1],
                        c=colors, alpha=1, s=5)

            # Set x-axis and y-axis labels
            ax.set_xlabel('FA1', color='white', fontsize=12)
            ax.set_ylabel('FA2', color='white', fontsize=12)

            # Set the plot title
            # ax.set_title('Meta Organoid', color='white', fontsize=14,fontweight='bold',x=0.01,y=1)

            # Add text in a different color to the top left of the plot
            for i, (text_color, text_content) in enumerate(zip(hcr_colors, adata.var_names)):
                if text_color == 'green':
                    text_color = 'lime'

            # white can also be the case the voxle has very high expression level
                plt.text(0.99, 1-0.03*i, text_content, transform=ax.transAxes,
                            color=text_color, fontsize=14, fontweight='bold')

            # Hide the axes
            ax.axis('off')

            # Save the plot
            plt.savefig('results/meta_organoid_max_{}_{}_{}_{}_{}_{}_{}_colorw{:.1f}.png'.format(
                *file_name_format, w_color), facecolor='black')
            plt.close()
        # draw_graph_with_color(adata, adata.var_names, save='_{}_{}_{}_{}.png'.format(hcr,day,pos,w))
        # draw_graph_with_color(adata, 'leiden', save='_leiden_{}_{}_{}_{}.png'.format(hcr,day,pos,w))
        # draw_graph_with_color(adata, 'hierarchy', save='_hierarchy_{}_{}_{}_{}.png'.format(hcr,day,pos,w))
        # draw_graph_with_color(adata, 'name', save='_name_{}_{}_{}_{}.png'.format(hcr,day,pos,w))

        # Downstream analysis
        # 1. Barplot: clusters proportions (hierarchy and leiden)

        # barplot with entropy

        # Hierarchy
        adata_df = pd.DataFrame(
            data=adata.obs['name'], index=adata.obs_names, columns=['name'])
        adata_df['hierarchy'] = adata.obs['hierarchy']
        hierarchy_proportions = adata_df.groupby(['name', 'hierarchy']).size().unstack(
            fill_value=0).div(adata_df.groupby('name').size(), axis=0)
        hierarchy_proportions['entropy'] = hierarchy_proportions.apply(
            lambda x: entropy(x, base=2), axis=1)
        plot_clusters_with_entropy_annotation(hierarchy_proportions, 'Hierarchy Cluster Proportions and Entropy for Each Organoid',
                                                'results/barplot_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format),newcmp)

        # Leiden
        adata_df['leiden'] = adata.obs['leiden']
        leiden_proportions = adata_df.groupby(['name', 'leiden']).size().unstack(
            fill_value=0).div(adata_df.groupby('name').size(), axis=0)
        leiden_proportions['entropy'] = leiden_proportions.apply(
            lambda x: entropy(x, base=2), axis=1)
        plot_clusters_with_entropy_annotation(leiden_proportions, 'Leiden Cluster Proportions and Entropy for Each Organoid',
                                                'results/barplot_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format),newcmp_leiden)

        # 2. Matrixplot & Dotplot: gene expression across clusters
        sc.pl.matrixplot(adata, adata.var_names, 'hierarchy', standard_scale='var',
                            save='hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.matrixplot(adata, adata.var_names, 'leiden', standard_scale='var',
                            save='leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.dotplot(adata, adata.var_names, groupby='hierarchy', standard_scale='var',
                        save='hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        sc.pl.dotplot(adata, adata.var_names, groupby='leiden', standard_scale='var',
                        save='leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

        # Gene-gene correlation: while we only have five genes in HCR
        plt.figure(figsize=(8, 6))
        gene_names = adata.var_names
        # Calculate the Spearman correlation between all pairs of genes
        correlation_matrix, _ = spearmanr(adata.X)
        # cmap='PRGn'
        sns.heatmap(correlation_matrix, cmap='PRGn', center=0,
                    xticklabels=gene_names, yticklabels=gene_names, annot=True, fmt=".2f")
        # plt.xlabel("Genes")
        # plt.ylabel("Genes")
        plt.title("Gene-Gene Correlation Matrix")
        plt.savefig(
            'results/gene-gene_cor_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

        # 3. Violinplot: gene expression across clusters

        # Plot expression distribution across multiple organoids
        for gene in adata.var_names:

            plt.figure(figsize=(10, 6))
            sns.violinplot(x='name', y=gene, data=adata.obs.join(
                adata.to_df()), inner='quartile')
            plt.title(
                f'Gene Expression Distribution for {gene} Across Organoids')
            plt.savefig(
                'results/violin_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
            # plt.show()
            plt.close()

        # Convert the raw data to a DataFrame
        raw_df = pd.DataFrame(
            adata.raw.X, columns=adata.raw.var_names, index=adata.obs.index)

        # Join the observational data with the raw data
        combined_df = adata.obs.join(raw_df)
        for gene in adata.var_names:
            plt.figure(figsize=(10, 6))
            sns.violinplot(x='name', y=gene,
                            data=combined_df, inner='quartile')
            plt.title(
                f'Raw Gene Expression Distribution for {gene} Across Organoids')
            plt.savefig(
                'results/violin_raw_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
            # plt.show()
            plt.close()

        # 4. Heatmap: gene expression across clusters
        # Add cluster and organoid information to the DataFrame
        adata_df = adata.to_df()
        adata_df['hierarchy'] = adata.obs['hierarchy']
        adata_df['leiden'] = adata.obs['leiden']
        adata_df['name'] = adata.obs['name']

        # Aggregate the data
        # Here we're computing the mean expression of each gene across each cluster and organoid combination
        agg_data = adata_df.groupby(['hierarchy', 'name']).mean()

        # Draw the heatmap
        plt.figure(figsize=(15, 8))
        # viridis
        sns.heatmap(agg_data.T, cmap='viridis', cbar_kws={
                    'label': 'Expression Level'}, annot=True)
        plt.title('Gene Expression Across Clusters and Organoids')
        plt.tight_layout()
        plt.savefig(
            'results/heatmap_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        plt.close()

        # Aggregate the data
        # Here we're computing the mean expression of each gene across each cluster and organoid combination
        agg_data = adata_df.groupby(['leiden', 'name']).mean()

        # Draw the heatmap
        plt.figure(figsize=(20, 8))
        # viridis
        sns.heatmap(agg_data.T, cmap='viridis', cbar_kws={
                    'label': 'Expression Level'}, annot=True)
        plt.title('Gene Expression Across Clusters and Organoids')
        plt.tight_layout()
        plt.savefig(
            'results/heatmap_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
        plt.close()

        # 5. the coefficient of variation (CV) of each gene
        df_cv = pd.DataFrame(columns=adata.var_names)
        for name in np.unique(adata.obs['name']):
            cv_values = np.std(adata.raw.X[adata.obs['name'] == name], axis=0) / np.mean(
                adata.raw.X[adata.obs['name'] == name], axis=0)
            df_cv.loc[name] = cv_values

        cv_values_all = np.std(adata.raw.X, axis=0) / \
            np.mean(adata.raw.X, axis=0)
        print(cv_values_all)
        df_cv.loc['All'] = cv_values_all
        df_cv.to_csv(
            'results/cv_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format))

        # 6. the Entropy of each gene
        df_entropy = pd.DataFrame(columns=adata.var_names)

        for name in np.unique(adata.obs['name']):
            # Calculate Shannon index for each gene within an organoid
            pk = adata.raw.X[adata.obs['name'] == name]
            pk = (pk-np.min(pk, axis=0)) / \
                (np.max(pk, axis=0)-np.min(pk, axis=0))
            shannon_indices = entropy(pk, axis=0)
            df_entropy.loc[name] = shannon_indices

        pk = adata.raw.X
        pk = (pk-np.min(pk, axis=0)) / \
            (np.max(pk, axis=0)-np.min(pk, axis=0))
        shannon_indices = entropy(pk, axis=0)
        df_entropy.loc['All'] = shannon_indices
        df_entropy.to_csv(
            'results/entropy_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format))

        # Single Cell Analysis and Co analysis with HCR

        # read scalled RNA seq from csv file
        # if pos != '2' or != '4', we do not use scRNAseq data
        if pos == 'pos4' or pos == 'pos2':
            # region_mapping
            # cell type mapping based on correlation
            # Assuming you have adata and scrna with 'leiden' annotation in adata.obs and scrna.obs
            # Convert the expression matrices to pandas data frames

            scrna_df = scrna.to_df()
            scrna_df['leiden'] = scrna.obs['leiden']
            scrna_df['leiden0.1'] = scrna.obs['leiden0.1']
            scrna_df['leiden0.5'] = scrna.obs['leiden0.5']
            scrna_df['leiden2'] = scrna.obs['leiden2']
            scrna_df['leiden5'] = scrna.obs['leiden5']

            for leiden_resolution in ['leiden0.1', 'leiden0.5', 'leiden', 'leiden2', 'leiden5']:
                # Calculate the mean gene expression profile for each cluster
                hcr_means = adata_df.groupby('hierarchy').mean()
                scrna_means = scrna_df.groupby(leiden_resolution).mean()

                # Make sure that both data frames have the same columns (genes) in the same order
                common_genes = hcr_means.columns.intersection(
                    scrna_means.columns)
                hcr_means = hcr_means[common_genes]
                scrna_means = scrna_means[common_genes]

                # Standardize the data (optional, but can help with Pearson correlation)
                hcr_means = zscore(hcr_means, axis=1)
                scrna_means = zscore(scrna_means, axis=1)

                # Calculate the similarity matrix using Pearson correlation
                # I can apply the concept of COVET in cluster-cluster mapping
                # I can also try CCA
                # HCR uses markers to annotate region of organoids (mapping regions information to scRNA in probability / likelihood format)
                # dorsal ventral, non-telecephalon, telecephalon region
                # also find the gene set works best -> find the most informative genes
                # size of the feature sapce is more critical than whether RNA or protein

                similarity_matrix = 1 - \
                    pairwise_distances(
                        hcr_means, scrna_means, metric='correlation')

                # Match clusters
                hcr_to_scrna_mapping = {}
                for i, hcr_cluster in enumerate(hcr_means.index):
                    scrna_cluster = scrna_means.index[np.argmax(
                        similarity_matrix[i])]
                    hcr_to_scrna_mapping[hcr_cluster] = scrna_cluster

                scrna_to_hcr_mapping = {}
                for i, scrna_cluster in enumerate(scrna_means.index):
                    hcr_cluster = hcr_means.index[np.argmax(
                        similarity_matrix[:, i])]
                    scrna_to_hcr_mapping[scrna_cluster] = hcr_cluster

                # the mapping is not mutual which makes sense:
                # 1. the data are not the same
                # 2. the markers used may not represent the cell types
                # 3. the clustering methods used are not the same

                # save mapping cell type to adata or scrna (try scrna first since it has more clusters)
                # Invert the hcr_to_scrna_mapping dictionary to create the scrna_to_hcr_mapping dictionary
                # scrna_to_hcr_mapping = {v: k for k, v in hcr_to_scrna_mapping.items()}

                # Create a new column in scrna.obs called 'adata' and populate it based on scrna_to_hcr_mapping and scrna.obs['leiden']
                scrna.obs['hcr_hierarchy_{}'.format(
                    leiden_resolution)] = scrna.obs[leiden_resolution].map(scrna_to_hcr_mapping)
                adata.obs['scrna_{}'.format(leiden_resolution)] = adata.obs['hierarchy'].map(
                    hcr_to_scrna_mapping)
                # Visualize the results
                sc.pl.umap(scrna, color=[leiden_resolution], save='_scrna_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
                    leiden_resolution, *file_name_format), show=False)
                sc.pl.umap(scrna, color=['hcr_hierarchy_{}'.format(leiden_resolution)], save='_scrna_hierarchy_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
                    leiden_resolution, *file_name_format), show=False)
                sc.pl.draw_graph(adata, color='scrna_{}'.format(leiden_resolution), ncols=5,
                                    neighbors_key='neighbors_sp', save='_scrna_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

            sc.pl.umap(scrna, color=common_genes, ncols=5, save='_scrna_genes_{}_{}_{}_{}_{}_{}_{}.png'.format(
                *file_name_format), show=False)

            # NovoSpaRc
            num_cells = min(scrna.shape[0], 3000)
            scrna_copy = scrna.copy()
            sc.pp.subsample(scrna_copy, n_obs=num_cells)
            dge_rep = pd.DataFrame(scrna_copy.obsm['X_pca'])
            # num_of_locations should be larger than num_of_cells
            num_locations = max(
                num_cells*2, min(scrna.shape[0], adata.shape[0]))
            adata_copy = adata.copy()
            sc.pp.subsample(adata_copy, n_obs=num_locations)
            sub_positions = adata_copy.obsm['X_draw_graph_fa']

            adata_copy.obsm['spatial'] = sub_positions
            pl_genes = adata_copy.var.index.tolist()
            novosparc.pl.embedding(adata_copy, pl_genes)

            # visualizing loc-loc expression distances vs their physical distances
            plot_exp_loc_dists_(
                adata_copy.X, sub_positions, save='loc_exp_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

            # # calculate Moran's I for each gene in the atlas, how spatially informative is
            atlas = adata_copy  # atlas = the reference
            atlas_genes = adata_copy.var.index.tolist()
            mI, pvals = novosparc.an.get_moran_pvals(
                atlas.X, sub_positions)
            df_mI = pd.DataFrame(
                {'moransI': mI, 'pval': pvals}, index=atlas_genes)
            gene_max_mI = df_mI['moransI'].idxmax()
            gene_min_mI = df_mI['moransI'].idxmin()
            embedding_(atlas, [gene_max_mI, gene_min_mI],
                        save='NS_mI_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
            df_mI.to_csv(
                'results/mI_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format))

            # The reconstruction of scRNA-seq tissue
            for gene_removed in adata_copy.var.index.tolist():
                tissue = novosparc.cm.Tissue(
                    dataset=scrna_copy, locations=sub_positions)

                # kick one gene out
                atlas_genes = adata.var.index.tolist()
                gene_names = scrna_copy.var.index.tolist()
                # params for smooth cost
                num_neighbors_s = num_neighbors_t = 8
                # params for linear cost
                markers = list(set(atlas_genes).intersection(gene_names))

                markers.remove(gene_removed)
                num_markers = len(markers)
                num_genes = scrna_copy.shape[1]
                atlas_matrix = adata_copy.to_df()[markers].values
                markers_idx = pd.DataFrame(
                    {'markers_idx': np.arange(num_genes)}, index=gene_names)
                markers_to_use = np.concatenate(
                    markers_idx.loc[markers].values)
                # reconstruct the tissue
                tissue.setup_reconstruction(atlas_matrix=atlas_matrix, markers_to_use=markers_to_use,
                                            num_neighbors_s=num_neighbors_s, num_neighbors_t=num_neighbors_t)
                # compute optimal transport of cells to locations
                alpha_linear = 0.8
                epsilon = 5e-3
                tissue.reconstruct(
                    alpha_linear=alpha_linear, epsilon=epsilon)
                # reconstructed expression of individual genes
                # The main objective of novoSpaRc is to probabilistically map single cells onto the tissue's physical structure,
                # and infer gene expression patterns across the tissue.
                sdge = tissue.sdge
                dataset_reconst = sc.AnnData(
                    pd.DataFrame(sdge.T, columns=gene_names))
                dataset_reconst.obsm['spatial'] = sub_positions
                embedding_(dataset_reconst, pl_genes, save='NS_reconst_remove{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
                    gene_removed, *file_name_format))

            # Construct scRNA tissue object
            tissue = novosparc.cm.Tissue(
                dataset=scrna_copy, locations=sub_positions)

            # use all genes from HCR as the markers
            atlas_genes = adata.var.index.tolist()
            gene_names = scrna_copy.var.index.tolist()
            # params for smooth cost
            num_neighbors_s = num_neighbors_t = 8
            # params for linear cost
            markers = list(set(atlas_genes).intersection(gene_names))
            # markers.remove(gene_removed)
            num_markers = len(markers)
            num_genes = scrna_copy.shape[1]
            atlas_matrix = adata_copy.to_df()[markers].values
            markers_idx = pd.DataFrame(
                {'markers_idx': np.arange(num_genes)}, index=gene_names)
            markers_to_use = np.concatenate(
                markers_idx.loc[markers].values)
            # reconstruct the tissue
            tissue.setup_reconstruction(atlas_matrix=atlas_matrix, markers_to_use=markers_to_use,
                                        num_neighbors_s=num_neighbors_s, num_neighbors_t=num_neighbors_t)
            # compute optimal transport of cells to locations
            alpha_linear = 0.8
            epsilon = 5e-3
            tissue.reconstruct(alpha_linear=alpha_linear, epsilon=epsilon)
            # reconstructed expression of individual genes
            # The main objective of novoSpaRc is to probabilistically map single cells onto the tissue's physical structure,
            # and infer gene expression patterns across the tissue.
            sdge = tissue.sdge
            dataset_reconst = sc.AnnData(
                pd.DataFrame(sdge.T, columns=gene_names))
            dataset_reconst.obsm['spatial'] = sub_positions
            embedding_(dataset_reconst, pl_genes, save='NS_reconstruct_{}_{}_{}_{}_{}_{}_{}.png'.format(
                *file_name_format))
            embedding_(adata_copy, pl_genes, save='NS_adata_{}_{}_{}_{}_{}_{}_{}.png'.format(
                *file_name_format))

            # statistical test
            # using KDE to approx distribution, using KS test and KL divergence to check the closeness of distributions
            # from scRNA, HCR (adata), reconstructed by NovoSpaRc (dataset_reconst)

            # Assuming adata, scrna, and dataset_reconst are your data sources
            # markers_idx and n_genes should be defined
            # Initialize the DataFrame
            df_div = pd.DataFrame(columns=[
                "Gene",
                "KS Statistic (HCR vs scRNA-seq)", "p-value (HCR vs scRNA-seq)",
                "KS Statistic (HCR vs reconst)", "p-value (HCR vs reconst)",
                "KS Statistic (scRNA-seq vs reconst)", "p-value (scRNA-seq vs reconst)",
                "KL Divergence (HCR vs scRNA-seq)",
                "KL Divergence (HCR vs reconst)",
                "KL Divergence (scRNA-seq vs reconst)"
            ])

            # output_directory = "results"
            # os.makedirs(output_directory, exist_ok=True)

            for i, gene in enumerate(adata.var_names):
                gene_data_adata = zscore(adata.X[:, i])
                i_s = markers_idx.loc[gene].values[0]
                gene_data_scrna = zscore(scrna.X[:, i_s])
                gene_data_reconst = zscore(dataset_reconst.X[:, i_s])

                # Calculate the KDEs
                kde_adata = gaussian_kde(gene_data_adata)
                kde_scrna = gaussian_kde(gene_data_scrna)
                kde_reconst = gaussian_kde(gene_data_reconst)
                x_adata = np.linspace(
                    min(gene_data_adata), max(gene_data_adata), 1000)
                x_scrna = np.linspace(
                    min(gene_data_scrna), max(gene_data_scrna), 1000)
                x_reconst = np.linspace(
                    min(gene_data_reconst), max(gene_data_reconst), 1000)

                y_adata = kde_adata(x_adata)
                y_scrna = kde_scrna(x_scrna)
                y_reconst = kde_reconst(x_reconst)

                # Find the peaks
                # don't know the exact distance
                peaks_adata, _ = find_peaks(y_adata, distance=1000)
                peaks_scrna, _ = find_peaks(y_scrna, distance=1000)
                peaks_reconst, _ = find_peaks(y_reconst, distance=1000)

                # Create and save the PDF comparison plot
                fig, ax1 = plt.subplots(figsize=(10, 6))
                ax1.plot(x_adata, y_adata, label='HCR', color='tab:blue')
                ax1.plot(x_adata[peaks_adata], y_adata[peaks_adata],
                            "x", label='Peaks', color='tab:blue')
                ax1.set_xlabel('Gene Expression')
                ax1.set_ylabel('PDF (HCR)', color='tab:blue')
                ax1.tick_params(axis='y', labelcolor='tab:blue')

                ax2 = ax1.twinx()
                ax2.plot(x_scrna, y_scrna, label='scRNA-seq',
                            color='tab:orange')
                ax2.plot(x_scrna[peaks_scrna], y_scrna[peaks_scrna],
                            "x", label='Peaks', color='tab:orange')
                ax2.set_ylabel('PDF (scRNA-seq)', color='tab:orange')
                ax2.tick_params(axis='y', labelcolor='tab:orange')

                ax3 = ax1.twinx()
                ax3.plot(x_reconst, y_reconst,
                            label='reconst', color='tab:green')
                ax3.plot(x_reconst[peaks_reconst], y_reconst[peaks_reconst],
                            "x", label='Peaks', color='tab:green')
                # Adjust the position of the third y-axis
                ax3.spines['right'].set_position(('outward', 60))
                ax3.set_ylabel('PDF (reconst)', color='tab:green')
                ax3.tick_params(axis='y', labelcolor='tab:green')

                # Combine the legends from all axes
                lines, labels = ax1.get_legend_handles_labels()
                lines2, labels2 = ax2.get_legend_handles_labels()
                lines3, labels3 = ax3.get_legend_handles_labels()
                ax3.legend(lines + lines2 + lines3, labels +
                            labels2 + labels3, loc='upper right')

                plt.title(f'Gene {gene} - PDF Comparison')
                plt.tight_layout()

                # Save the PDF comparison plot as an image
                # plt.show()
                plt.savefig(
                    'results/{}_comparison_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
                plt.close()

                # Create and save the detailed reconst PDF plot
                fig, ax = plt.subplots(figsize=(8, 5))
                ax.plot(x_reconst, y_reconst,
                        label='reconst', color='tab:green')
                ax.set_xlabel('Gene Expression')
                ax.set_ylabel('PDF (reconst)', color='tab:green')
                ax.tick_params(axis='y', labelcolor='tab:green')
                plt.title(f'Gene {gene} - PDF (reconst) Detail')
                plt.tight_layout()

                # Save the detailed reconst PDF plot as an image
                # plt.show()
                plt.savefig(
                    'results/{}_reconst_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
                plt.close()

                # Create and save the histogram comparison plot
                fig, ax1 = plt.subplots(figsize=(12, 6))
                ax1.hist(gene_data_adata, bins=30, alpha=0.5,
                            label='HCR', color='tab:blue')
                ax1.set_xlabel('Gene Expression')
                ax1.set_ylabel('Counts', color='tab:blue')
                ax1.tick_params(axis='y', labelcolor='tab:blue')

                ax2 = ax1.twinx()
                ax2.hist(gene_data_scrna, bins=30, alpha=0.5,
                            label='scRNA-seq', color='tab:orange')
                ax2.set_ylabel('Counts', color='tab:orange')
                ax2.tick_params(axis='y', labelcolor='tab:orange')

                ax3 = ax1.twinx()
                ax3.hist(gene_data_reconst, bins=30, alpha=1,
                            label='reconst', color='tab:green')
                # Adjust the position of the third y-axis
                ax3.spines['right'].set_position(('outward', 60))
                ax3.set_ylabel('Counts', color='tab:green')
                ax3.tick_params(axis='y', labelcolor='tab:green')

                # Combine the legends from all axes
                lines, labels = ax1.get_legend_handles_labels()
                lines2, labels2 = ax2.get_legend_handles_labels()
                lines3, labels3 = ax3.get_legend_handles_labels()
                ax3.legend(lines + lines2 + lines3, labels +
                            labels2 + labels3, loc='upper right')

                plt.title(f'Gene {gene} - Histogram Comparison')
                plt.tight_layout()
                # plt.show()
                plt.savefig(
                    'results/{}_comparison_hist_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
                plt.close()

                # Create and save the detailed reconst PDF plot
                fig, ax = plt.subplots(figsize=(8, 5))
                ax.hist(gene_data_reconst, bins=30, alpha=0.5,
                        label='reconst', color='tab:green')
                ax.set_ylabel('Counts', color='tab:green')
                ax.tick_params(axis='y', labelcolor='tab:green')

                plt.title(f'Gene {gene} - Histogram (reconst) Detail')
                plt.tight_layout()
                # plt.show()
                plt.savefig(
                    'results/{}_reconst_hist_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
                plt.close()

                # Perform Kolmogorov-Smirnov tests
                ks_statistic_adata_scrna, p_value_adata_scrna = ks_2samp(
                    gene_data_adata, gene_data_scrna)
                ks_statistic_adata_reconst, p_value_adata_reconst = ks_2samp(
                    gene_data_adata, gene_data_reconst)
                ks_statistic_scrna_reconst, p_value_scrna_reconst = ks_2samp(
                    gene_data_scrna, gene_data_reconst)

                kl_divergence_adata_scrna = sum(kl_div(y_adata, y_scrna))
                kl_divergence_adata_reconst = sum(
                    kl_div(y_adata, y_reconst))
                kl_divergence_scrna_reconst = sum(
                    kl_div(y_scrna, y_reconst))

                df_div = df_div.append({
                    "Gene": gene,
                    "KS Statistic (HCR vs scRNA-seq)": ks_statistic_adata_scrna,
                    "p-value (HCR vs scRNA-seq)": p_value_adata_scrna,
                    "KS Statistic (HCR vs reconst)": ks_statistic_adata_reconst,
                    "p-value (HCR vs reconst)": p_value_adata_reconst,
                    "KS Statistic (scRNA-seq vs reconst)": ks_statistic_scrna_reconst,
                    "p-value (scRNA-seq vs reconst)": p_value_scrna_reconst,
                    "KL Divergence (HCR vs scRNA-seq)": kl_divergence_adata_scrna,
                    "KL Divergence (HCR vs reconst)": kl_divergence_adata_reconst,
                    "KL Divergence (scRNA-seq vs reconst)": kl_divergence_scrna_reconst
                }, ignore_index=True)

            # Save the DataFrame to CSV
            df_div.to_csv(
                'results/div_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format), index=False)

