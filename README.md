# VoxMerger: Spatially invariant integrative voxel embedding for morphologically variable HCR stained samples

`VoxMerger` is a powerful tool designed to provide spatially invariant integrative voxel embedding for samples stained with HCR. Although currently offering a protocol that users can apply to their files, our vision is to integrate it with Scanpy for a comprehensive analysis suite in the future.

## Installation

1. Clone the repository from GitLab:
```
% clone with https
git clone https://gitlab.ethz.ch/yihliu/VoxMerger.git
```

```
% clone with ssh
git clone git@gitlab.ethz.ch:yihliu/VoxMerger.git
```
2. Navigate to the directory:
```
cd path_to_directory
```
3. Install the required packages:
```
pip install -r environment.yml
```

## Usage

To use `VoxMerger`, run scripts sequentially the appropriate parameters:

1. `fun_img_processing.py` to generated the processed matrix from images.
```
python fun_img_processing.py --input_directory path_to_input_directory --output_directory path_to_output_directory --colnames probes_of_each_channel
```
2. `fun_meta_organoid.py` to generate the meta-organoid and downstream analysis results.
```
python fun_meta_organoid.py --directory_h5ad path_to_processed_h5ad_directory  --directory_output path_to_output_directory
```
3. `fun_region_mapping.py` to co-analyze the meta-organoid with corresponding scRNA-seq data.
```
python fun_meta_organoid.py --directory_scrna path_to_scRNA-seq_directory --directory_hcr path_to_meta-organoid_directory --directory_output path_to_output_directory
```

For a detailed list of parameters, check the function description in each script.


## Tutorial

### Step 1: Preparing your data

Make sure your data is in the supported format, specificly, `.tif` for image data, `.csv` or `.h5ad` for scRNA-seq data. If you're starting with raw count matrices, the script will process them using the `scRNA_processing` function in `fun_region_mapping.py` automatically. The step will be skipped if you already have processed data.

### Step 2: Running VoxMerger

As descriped in the Usage section, run the scripts sequentially with the appropriate parameters. The output will be saved in the specified directory.


### Step 3: Analyzing results

Once `VoxMerger` completes its processing, navigate to the output directory (`results` and `figures`) to view and analyze the results.


## Contributing

We welcome contributions! If you find any bugs or have suggestions, please open an issue on our GitLab page.

## Authors and acknowledgment

This project, `VoxMerger`, was developed as part of a master's thesis by:

- **Yihao Liu** - [GitLab Profile](https://gitlab.ethz.ch/yihliu)

Special thanks to:

- **Gilles Gut** - Supervisor and provided invaluable insights into the concept, methodology, and overall direction of the project. His deep understanding and recommendations were instrumental in shaping the core ideas and methods implemented in `VoxMerger`.

- **Dr. Akanksha Jain** - Co-supervisor, provided the data for the project. Her feedback, grounded in her expertise in biology, ensured the project's scientific accuracy and relevance.

## License

`VoxMerger` is licensed under the [MIT License](LICENSE). 

This means that anyone is free to copy, modify, publish, use, compile, sell, or distribute the software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

For the full license text, please see the [LICENSE](LICENSE) file in the project's repository.

## Project Status

**Active Development:** `VoxMerger` is currently in active development. While the core functionality has been established and is usable, expect regular updates and changes as we continue to refine and expand the tool.

We welcome contributions from the community, be it in the form of code, bug reports, or feature requests. 

If, at any point, the development pace slows down or halts, we'll update this section. Anyone interested in forking or maintaining the project under such circumstances is encouraged to reach out.

## Future Plans

- Packaging `VoxMerger` as a Python library
- Integration with Scanpy for an end-to-end analysis solution.
- Adding support for more input formats.
- Enhancing processing capabilities with machine learning models.
