# Standard libraries
import os
import itertools

# External libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import hdf5plugin
import pymeshfix
import pathlib

# Image processing libraries
from skimage import io, measure, exposure
from skimage.transform import rescale, resize, downscale_local_mean
from skimage.filters import threshold_mean, median, gaussian, try_all_threshold, threshold_otsu
from skimage.morphology import disk, area_closing, remove_small_objects
from skimage.measure import label, marching_cubes
from scipy import ndimage as ndi
import tifffile as tif
import trimesh
from trimesh.voxel.ops import matrix_to_marching_cubes
from trimesh.visual.color import ColorVisuals
from trimesh.smoothing import filter_taubin
from sklearn.metrics import silhouette_score
from sklearn.neighbors import kneighbors_graph

# Data processing and analysis libraries
import scanpy as sc
import anndata


def mask_based_on_dapi(img, z_start=0, z_end=None, sigma=1, neighborhood_size=5,
                       min_object_size=1000, dilation_iterations=50, thresh=0.2, show=False, contour=False):
    """
    Processes each slice in a 3D image, applies preprocessing steps, and identifies objects.

    Parameters:
    - img (numpy.ndarray): 3D image to be processed.
    - z_start (int): Starting slice for processing. Default is 0.
    - z_end (int, optional): Ending slice for processing. If None, processes until the last slice. Default is None.
    - sigma (float): Sigma parameter for Gaussian filter. Default is 1.
    - neighborhood_size (int): Size of the neighborhood for local thresholding. Default is 5.
    - min_object_size (int): Minimum size of objects to be retained after morphological processing. Default is 1000.
    - dilation_iterations (int): Number of iterations for morphological dilation. Default is 50.
    - thresh (float): Threshold for object identification. Default is 0.2.
    - show (bool): If True, displays intermediate processing steps. Default is False.
    - contour (bool): If True, displays object contours on original image. Default is False.

    Returns:
    - numpy.ndarray: Processed and masked 3D image.
    """

    masks = []
    contours = []
    if z_end == None:
        z_end = img.shape[0]-1

    for z in range(z_start, z_end + 1):
        image_slice = img[z, 0]
        # Option2: gaussain - median - quantile(binary)
        smoothed_image = gaussian(image_slice, sigma=sigma)

        filtered_image = median(smoothed_image, footprint=np.ones(
            (neighborhood_size, neighborhood_size)))
        threshold = np.quantile(filtered_image[filtered_image > 0], thresh)
        filtered_image = filtered_image > threshold

        labeled_image = label(filtered_image, connectivity=2)

        filtered_labels = remove_small_objects(
            labeled_image, min_size=min_object_size)

        connected_labels = ndi.binary_dilation(
            filtered_labels, iterations=dilation_iterations)
        filled_labels = ndi.binary_fill_holes(connected_labels)
        filled_labels = ndi.binary_erosion(
            filled_labels, iterations=dilation_iterations)

        contour_slice = measure.find_contours(filled_labels, level=0.5)
        contours.append(contour_slice)

        masks.append(filled_labels)

    if show:
        num_rows = z_end - z_start + 1
        fig, axes = plt.subplots(num_rows, 2, figsize=(12, 6 * num_rows))
        fig.suptitle('Masks for each slice')
        if contour:
            for i, (mask, contour_slice) in enumerate(zip(masks, contours)):
                axes[i, 0].imshow(img[z_start + i, 0],
                                  cmap='gray', vmin=0, vmax=1)
                axes[i, 0].set_title(f'Slice {z_start + i}')
                axes[i, 0].axis('off')

                axes[i, 1].imshow(mask, cmap='gray', vmin=0, vmax=1)
                axes[i, 1].set_title(f'Mask {z_start + i}')
                axes[i, 1].axis('off')

            for contour_subslice in contour_slice:
                axes[i, 1].plot(contour_subslice[:, 1],
                                contour_subslice[:, 0], linewidth=2, color='b')

        else:
            for i, mask in enumerate(masks):
                axes[i, 0].imshow(img[z_start + i, 0],
                                  cmap='gray', vmin=0, vmax=1)
                axes[i, 0].set_title(f'Slice {z_start + i}')
                axes[i, 0].axis('off')

                axes[i, 1].imshow(mask, cmap='gray', vmin=0, vmax=1)
                axes[i, 1].set_title(f'Mask {z_start + i}')
                axes[i, 1].axis('off')

        plt.tight_layout()
        plt.show()

    return masks


# def apply_equalization(image, kernel_size=(3, 3, 3), clip_limit=0.01, equalize_flag='adapt'):
#     """
#     Applies various equalization methods to the input image on a per-channel basis.

#     Parameters:
#     - image (numpy.ndarray): A 4D array representing the input image with dimensions [z, c, y, x].
#     - kernel_size (tuple, optional): Size of the neighborhood region for adaptive histogram equalization. Default is (3, 3, 3).
#     - clip_limit (float, optional): Clipping limit for contrast enhancement in adaptive histogram equalization. Default is 0.01.
#     - equalize_flag (str, optional): The type of equalization method to apply. Options are:
#         * 'adapt': Adaptive histogram equalization.
#         * 'eq': Standard histogram equalization.
#         * 're_intensity': Rescale intensity based on the 2nd and 98th percentiles.

#     Returns:
#     - numpy.ndarray: Equalized image with the same shape as the input image.

#     Notes:
#     The function processes each channel of the input image separately. Depending on the `equalize_flag` value, it applies
#     either adaptive histogram equalization, standard histogram equalization, or intensity rescaling. The results for each
#     channel are then combined to produce the final equalized image.

#     For 'adapt' and 'eq' methods, after applying the equalization, the resulting channel data is rescaled to match the
#     range of original channel data and then converted to uint8 datatype.

#     For 're_intensity' method, the function rescales the channel data based on the 2nd and 98th percentiles of its
#     intensities and then converts the result to uint8 datatype.
#     """

#     # Get the shape of the image
#     z, c, y, x = image.shape

#     # Create an empty array to store the equalized image
#     # equalized_image = np.zeros_like(image, dtype=np.uint8)
#     equalized_image = np.zeros_like(image)
#     # if masks is None:
#     #     masks = np.ones(image.shape, dtype=np.uint8)

#     # Loop over each channel and apply exposure.equalize_adapthist
#     for channel in range(c):
#         # Get the current channel's data
#         channel_data = image[:, channel, :, :]
#         if equalize_flag == 'adapt':
#             # Apply exposure.equalize_adapthist to the current channel's data
#             equalized_channel_data = exposure.equalize_adapthist(
#                 channel_data, kernel_size=kernel_size, clip_limit=clip_limit)
#             # problem here, even after rescaling, the background in channel 6 and channel 3 has value around 9
#             equalized_max = np.max(channel_data)
#             equalized_channel_data = ((equalized_channel_data-np.min(equalized_channel_data)) * equalized_max/(
#                 np.max(equalized_channel_data)-np.min(equalized_channel_data))).astype(np.uint8)

#         elif equalize_flag == 'eq':
#             equalized_channel_data = exposure.equalize_hist(
#                 channel_data)  # .astype(np.uint8)
#             equalized_max = np.max(channel_data)
#             equalized_channel_data = ((equalized_channel_data-np.min(equalized_channel_data)) * equalized_max/(
#                 np.max(equalized_channel_data)-np.min(equalized_channel_data))).astype(np.uint8)

#         elif equalize_flag == 're_intensity':
#             p2, p98 = np.percentile(channel_data, (2, 98))
#             equalized_channel_data = exposure.rescale_intensity(
#                 channel_data, in_range=(p2, p98)).astype(np.uint8)

#         # Store the equalized channel data in the new array
#         # equalized_channel_data = np.multiply(masks, equalized_channel_data)
#         equalized_image[:, channel, :, :] = equalized_channel_data

#     return equalized_image

def apply_equalization(image, kernel_size=(3, 3, 3), clip_limit=0.01):
    # Get the shape of the image
    z, c, y, x = image.shape

    # Create an empty array to store the equalized image
    equalized_image = np.zeros_like(image, dtype=np.uint8)
    # if masks is None:
    #     masks = np.ones(image.shape, dtype=np.uint8)

    # Loop over each channel and apply exposure.equalize_adapthist
    for channel in range(c):
        # Get the current channel's data
        channel_data = image[:, channel, :, :]

        # Apply exposure.equalize_adapthist to the current channel's data
        equalized_channel_data = exposure.equalize_adapthist(
            channel_data, kernel_size=kernel_size, clip_limit=clip_limit)
        # problem here, even after rescaling, the background in channel 6 and channel 3 has value around 9
        # if rescale_flat == True, rescale the image to 0-equalized_max
        equalized_max = np.max(channel_data)
        equalized_channel_data = ((equalized_channel_data-np.min(equalized_channel_data)) * equalized_max/(
            np.max(equalized_channel_data)-np.min(equalized_channel_data))).astype(np.uint8)

        # Store the equalized channel data in the new array
        # equalized_channel_data = np.multiply(masks, equalized_channel_data)
        equalized_image[:, channel, :, :] = equalized_channel_data

    return equalized_image

# Step 2(optional): Median Filter


def median_filter(img, disk_size=5):
    # Create a new image for the result
    img_median = np.zeros_like(img)
    for j in range(img.shape[1]):
        for i in range(img.shape[0]):
            img_median[i, j] = median(img[i, j], disk(disk_size))
    return img_median

# Step 3: Binary Mask based on Dapi


def apply_mask(img):
    mask = mask_based_on_dapi(img)
    for j in range(img.shape[1]):
        img[:, j] = np.multiply(mask, img[:, j])
    return img

# Step 4: Calibration based on Dapi mean
# TODO: np.uint8? for the visualization Yes, but for the analysis No!


def calibrate_image(img, calibration_factor=1):
    img = img/calibration_factor
    img = np.clip(img, 0, 255)
    img = img.astype(np.uint8)
    return img

# Step 5: Quantile thresholding
# TODO: np.uint8? for the visualization Yes, but for the analysis No!


def quantile_threshold(img, quantile=0.2):
    img_quantile = np.zeros_like(img)
    for j in range(img.shape[1]):
        # Step 5: Quantile
        if len(np.unique(img[:, j])) == 1:
            thresh = 0
        else:
            thresh = np.quantile(img[:, j][img[:, j] != 0], quantile)
        img_quantile[:, j] = np.where(img[:, j] > thresh, img[:, j], 0)
    img_quantile = np.clip(img_quantile, 0, 255)
    img_quantile = img_quantile.astype(np.uint8)
    return img_quantile

# Step 6: rescaling


def rescale_image(img, rescale_rate=1):
    img_rescale = resize(img,
                         (img.shape[0], img.shape[1], int(
                             img.shape[2]*rescale_rate), int(img.shape[3]*rescale_rate)),
                         preserve_range=True, anti_aliasing=False)
    return img_rescale

# TODO: replace process_image with preprocess_image and rewrite the __main__ function


def preprocess_image(DATA_PATH, markerset, kernel_size=None, clip_limit=0.01,
                     median_filter_flag=False, disk_size=5, quantile_thresh=0.1,
                     rescale_rate=0.0625):
    """
    Preprocess the image with given steps and save it as a .h5ad file.

    Parameters:
    - DATA_PATH (str): Path to the image.
    - markerset (list): List of markers to be used for preprocessing.
    - kernel_size (tuple, optional): Kernel size for adaptive histogram equalization.
    - clip_limit (float, optional): Clip limit for adaptive histogram equalization.
    - median_filter_flag (bool, optional): Whether to apply median filtering.
    - disk_size (int, optional): Disk size for median filtering.
    - quantile_thresh (float, optional): Quantile value for thresholding.
    - rescale_rate (float, optional): Rescale rate for resizing.

    Returns:
    - numpy array: Preprocessed image.
    """

    # Get the parent directory of the current working directory
    parent_dir = pathlib.Path.cwd().parent

    # Check if the 'results' folder exists in the parent directory
    results_dir = parent_dir / 'results'

    if not results_dir.exists():
        # Create the 'results' folder if it does not exist
        results_dir.mkdir()

    file_list = [filename for filename in os.listdir(
        DATA_PATH) if filename.startswith("HCR") and filename.endswith(".tif")]
    dapi_mean = []
    imgs_masked = []
    file_name_formats = []
    for file in file_list:
        # file_name_format to tracking the results
        hcr, hb, day, sample_id, _ = file.split('_')
        sample_id, batch_id = sample_id.split('-')
        sample_id = sample_id[-1]
        file_name_format = [hcr, hb, day, sample_id,
                            batch_id, quantile_thresh, rescale_rate]
        file_name_formats.append(file_name_format)
        # Read the image
        os.chdir(DATA_PATH)
        image = io.imread(file)
        # Create kernel based on the image size
        if kernel_size is None:
            kernel_size = (image.shape[0] // 5,
                           image.shape[2] // 50,
                           image.shape[3] // 50)
            kernel_size = np.array(kernel_size)
        # Step 1: Histogram Equalization
        equalized_image = apply_equalization(image, kernel_size, clip_limit)
        # print('equalized image saved')
        # tif.imsave(results_dir/'{}_{}_{}_pos{}_{}_thresh{}_rescale{}_equalized.tif'.format(*file_name_format), equalized_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
        # Step 2: Median Filter
        if median_filter_flag:
            equalized_image = median_filter(equalized_image, disk_size)
            # tif.imsave(results_dir/'{}_{}_{}_pos{}_{}_thresh{}_rescale{}_median.tif'.format(*file_name_format), equalized_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
        # Step 3: Binary Mask based on Dapi
        masked_image = apply_mask(equalized_image)
        # tif.imsave(results_dir/'{}_{}_{}_pos{}_{}_thresh{}_rescale{}_masked.tif'.format(*file_name_format), masked_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
        dapi_mean.append(np.mean(np.ma.masked_equal(masked_image[:, 0], 0)))
        imgs_masked.append(masked_image)
    print('End of Step 3: Binary Mask based on Dapi')
    # Calculate the calibration factors
    calibration_factors = dapi_mean/np.mean(dapi_mean)
    adata_names = []
    for masked_image, calibration_factor, file_name_format in zip(imgs_masked, calibration_factors, file_name_formats):
        # Step 4: Calibration
        calibrated_image = calibrate_image(masked_image, calibration_factor)
        # tif.imsave(results_dir/'{}_{}_{}_pos{}_{}_thresh{}_rescale{}_calibrated.tif'.format(*file_name_format), calibrated_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
        # Step 5: Quantile Thresholding
        thresholded_image = quantile_threshold(
            calibrated_image, quantile_thresh)
        # tif.imsave(results_dir/'{}_{}_{}_pos{}_{}_thresh{}_rescale{}_thresholded.tif'.format(*file_name_format), thresholded_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

        # Step 6: Rescaling
        rescaled_image = rescale_image(thresholded_image, rescale_rate)
        # tif.imsave(results_dir/'{}_{}_{}_pos{}_{}_thresh{}_rescale{}_rescaled.tif'.format(*file_name_format), rescaled_image.astype(np.uint8), imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
        # print('image saved')
        # We will not save individual images, but save the rescaled_images as a concate .h5ad file

        # Reshape 4D array into 2D array
        img_flat = rescaled_image.transpose(
            (3, 2, 0, 1)).reshape((-1, rescaled_image.shape[1]))

        # Create a dataframe with the reshaped array
        hcr, hb, day, sample_id, batch_id, quantile_thresh, rescale_rate = file_name_format

        colnames = markerset
        rownames = [(i, j, k, int(sample_id), int(batch_id)) for i in range(rescaled_image.shape[3])
                    for j in range(rescaled_image.shape[2]) for k in range(rescaled_image.shape[0])]
        # vm = pd.DataFrame(data=img_flat.astype(np.uint8),
        #                 index=rownames, columns=colnames)
        vm = pd.DataFrame(data=img_flat, index=rownames, columns=colnames)

        # Create an AnnData object from the dataframe excluding the DAPI channel
        obs_df = pd.DataFrame(index=vm.index)
        var_df = pd.DataFrame(index=vm.columns[1:])
        adata = sc.AnnData(X=vm.iloc[:, 1:].values, obs=obs_df, var=var_df)

        adata_name = f'adata_{hcr}_{hb}_{day}_{sample_id}_{batch_id}'
        globals()[adata_name] = adata
        adata.obs['name'] = adata_name[-3:]
        adata_names.append(globals()[adata_name])
    print('End of Step 6: Rescaling')

    # TODO: the calibration step might generate values lager than 255, would it be possible to use int16 values?
    # TODO: the mask creation step, delete the z-start and z-end
    # Create and save the AnnData object containing all the data
    adata_concat = anndata.concat(adata_names, label='batch')
    index = []
    for i in adata_concat.obs.index:
        result = '_'.join(str(x) for x in eval(i))
        index.append(result)
    adata_concat.obs.index = index

    # Save the adata_concat object in the 'results' folder
    adata_concat.write_h5ad(
        results_dir / f'{hcr}_{hb}_{day}_pos{sample_id}_thresh{quantile_thresh}_rescale{rescale_rate}.h5ad', compression=hdf5plugin.Zstd(clevel=3))

    return adata_concat


def process_image(
    input_directory: str,
    output_directory: str,
    colnames: list,
    quantile_threshold: float = 0.2,
    median_disk_size: int = 5,
    min_object_size: int = 1000,
    rescale_rate: float = 0.0625,
    save_tif: bool = False
):
    """
    Processes an image and saves the preprocessed data as a .h5ad file.

    Parameters:
    - input_directory (str): Directory where the image is saved.
    - output_directory (str): Directory where the output should be saved.
    - colnames (list): Channel information.
    - quantile_threshold (float, optional): Quantile threshold. Default is 0.2.
    - median_disk_size (int, optional): Median disk size. Default is 5.
    - min_object_size (int, optional): Mask minimum object size. Default is 1000.
    - rescale_rate (float, optional): Rescale rate. Default is 0.0625.
    - save_tif (bool, optional): If True, saves intermediate .tif images. Default is False.

    Returns:
    - None: The function saves the processed image as a .h5ad file in the specified output directory.
    """
    # filrnam pattern: HCR17_HB4_D11_pos2-1_ALL.tif
    file_list = [filename for filename in os.listdir(
        input_directory) if filename.endswith(".tif")]
    img_preprocessed_list = []

    # Image Processing (final version)
    # Global parameters: quantile_threshold
    adata_names = []
    dapi_mean = []
    dapi_median = []  # not used
    batch_full_sizes = []
    imgs_masked = []
    file_name_formats = []

    for file in file_list:
        hcr = file.split("_")[0]
        day = file.split("_")[2]
        pos = file.split("_")[3]
        sample_id, batch_id = pos("-")
        sample_id = sample_id[-1]
        batch_id = batch_id[0]

        file_name_format = [hcr, day, pos,
                            quantile_threshold, rescale_rate]
        file_name_formats.append(file_name_format)

        os.chdir(input_directory)
        img = io.imread(file)
        os.chdir(output_directory)

        # Get the mask based on dapi
        masks = mask_based_on_dapi(img, min_object_size=min_object_size)
        # Apply quantile threshod, median, gaussian
        img_masked = np.zeros_like(img)
        img_median = np.zeros_like(img)

        kernel_size = (img.shape[0] // 5,
                       img.shape[2] // 50,
                       img.shape[3] // 50)
        kernel_size = np.array(kernel_size)

        # Step 1: Equalization
        # rescale the image to the range of 0-equalized_max
        equalized_image = apply_equalization(img, kernel_size=kernel_size)
        if save_tif:
            tif.imsave('{}_{}_{}_{}_{}_equalized.tif'.format(*file_name_format),
                       equalized_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

        # Step 2: Median
        img_median = np.zeros_like(equalized_image)
        img_masked = np.zeros_like(img_median)
        img_quantile = np.zeros_like(img_masked)
        for j in range(img.shape[1]):
            for i in range(img.shape[0]):
                img_median[i, j] = median(
                    equalized_image[i, j], disk(median_disk_size))

            # Step 3: Mask
            img_masked[:, j] = np.multiply(masks, img_median[:, j])
        if save_tif:
            tif.imsave('{}_{}_{}_{}_{}_median.tif'.format(*file_name_format),
                       img_median, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            tif.imsave('{}_{}_{}_{}_{}_masked.tif'.format(*file_name_format),
                       img_masked, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

        dapi_mean.append(np.mean(np.ma.masked_equal(img_masked[:, 0], 0)))
        imgs_masked.append(img_masked)

    dapi_scale_rates = dapi_mean/np.mean(dapi_mean)

    for img_masked, dapi_scale_rate, file_name_format in zip(imgs_masked, dapi_scale_rates, file_name_formats):
        # Step4: Calibration
        img_quantile = np.zeros_like(img_masked)
        img_masked = (img_masked / dapi_scale_rate).astype(np.uint8)
        if save_tif:
            tif.imsave('{}_{}_{}_{}_{}_calibrated.tif'.format(*file_name_format),
                       img_masked, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

        for j in range(img_masked.shape[1]):
            # Step 5: Quantile (it should be applied to the 3D image? or 2D)
            # if all values are the same
            if len(np.unique(img_masked[:, j])) == 1:
                thresh = 0
            else:
                thresh = np.quantile(
                    img_masked[:, j][img_masked[:, j] != 0], quantile_threshold)
            img_quantile[:, j] = np.where(
                img_masked[:, j] > thresh, img_masked[:, j], 0)
        if save_tif:
            tif.imsave('{}_{}_{}_{}_{}_quantiled.tif'.format(*file_name_format),
                       img_quantile, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

        # Step 6: rescale (optional)
        img_rescale = resize(img_quantile, (img_quantile.shape[0], img_quantile.shape[1], int(img_quantile.shape[2]*rescale_rate), int(img_quantile.shape[3]*rescale_rate)),
                             preserve_range=True, anti_aliasing=False)  # .astype(np.uint8)

        batch_full_sizes.append(
            img_rescale.shape[0] * img_rescale.shape[2] * img_rescale.shape[3])

        # Reshape 4D array into 2D array
        img_flat = img_rescale.transpose(
            (3, 2, 0, 1)).reshape((-1, img_rescale.shape[1]))

        # Create a dataframe with the reshaped array
        sample_id, batch_id, quantile_threshold, rescale_rate = file_name_format
        colnames = colnames
        rownames = [(i, j, k, int(sample_id), int(batch_id)) for i in range(img_rescale.shape[3])
                    for j in range(img_rescale.shape[2]) for k in range(img_rescale.shape[0])]
        vm = pd.DataFrame(data=img_flat.astype(np.uint8),
                          index=rownames, columns=colnames)

        # data_describe(vm,sample_id = sample_id, batch_id = batch_id, name_index = 'pp')

        obs_df = pd.DataFrame(index=vm.index)
        var_df = pd.DataFrame(index=vm.columns[1:])
        adata = sc.AnnData(X=vm.iloc[:, 1:].values, obs=obs_df, var=var_df)

        adata_name = 'adata'+sample_id+'_'+batch_id
        adata_names.append(adata_name)
        globals()[adata_name] = adata

    # the code in this block should be improved as a function, since we cannot know and assign number of adata_concat_ in advance
    # Assume that `adata_names` is a list of 5 `adata` object names, e.g.,

    for adata_name in adata_names:
        adata = globals()[adata_name]
        # since here we change adata.X, we cannot rerun this code
        adata.obs['name'] = adata_name.split('adata')[1]
        img_preprocessed_list.append(adata)

    img_preprocessed = anndata.concat(img_preprocessed_list, label='batch')

    index = []
    for i in img_preprocessed.obs.index:
        result = '_'.join(str(x) for x in i)
        index.append(result)
    img_preprocessed.obs.index = index

    img_preprocessed.write_h5ad("{}_{}_pos{}_preprocessed_thresh{}_{}.h5ad".format(
        hcr, day, sample_id, quantile_threshold, rescale_rate), compression=hdf5plugin.FILTERS["zstd"])


if __name__ == "__main__":
    # TODO: test the code and make saving .h5ad file as an option or delete it (15.11.2023
    preprocess_image('/cluster/project/treutlein/USERS/yihliu/data/test',
                     ['dapi', 'RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2'])
    # process_image('/cluster/project/treutlein/USERS/yihliu/data/test',
    #               '/cluster/project/treutlein/USERS/yihliu/results', ['dapi', 'RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2'])
