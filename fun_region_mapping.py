import heapq
import os


import anndata
import matplotlib.pyplot as plt
import networkx as nx
import novosparc
import numpy as np
import pandas as pd
import scanpy as sc
import seaborn as sns
import hdf5plugin


from scipy.special import kl_div
from scipy.stats import spearmanr, entropy, gaussian_kde, ks_2samp, zscore
from sklearn.metrics import silhouette_score, pairwise_distances
from scipy.spatial import distance_matrix, squareform
from scipy.spatial.distance import pdist
from scipy.signal import find_peaks


def plot_exp_loc_dists_(exp, locations, tit_size=15, nbins=10, save=None):
    """
    Plots the relationship between expression distances and physical distances across spatial locations.

    Parameters:
    - exp (numpy.ndarray): A matrix representing spatial expression across locations, with dimensions [locations x genes].
    - locations (numpy.ndarray): A matrix representing spatial coordinates of locations, with dimensions [locations x dimensions].
    - tit_size (int, optional): Font size for plot title and axis labels. Default is 15.
    - nbins (int, optional): Number of bins to use when grouping physical distances for box plot visualization. Default is 10.
    - save (str, optional): Filename to save the resulting plot. If None, the plot is not saved. Default is None.

    Notes:
    The function first computes pairwise distances for both the expression data and the physical locations.
    It then groups the physical distances into bins and generates a box plot to visualize the distribution of expression distances 
    within each physical distance bin.

    The resulting plot provides insights into how expression similarity varies as a function of spatial proximity.
    """

    locs_exp_dist = squareform(pdist(exp))
    locs_phys_dist = squareform(pdist(locations))
    exp_col = 'Locations expression distance'
    phys_col = 'Locations physical distance'
    phys_col_bin = 'Locations physical distance bin'

    df = pd.DataFrame({exp_col: locs_exp_dist.flatten(),
                      phys_col: locs_phys_dist.flatten()})

    lower, higher = int(df[phys_col].min()), int(np.ceil(df[phys_col].max()))
    # the number of edges is 8
    edges = range(lower, higher, int((higher - lower)/nbins))
    lbs = ['(%d, %d]' % (edges[i], edges[i+1]) for i in range(len(edges)-1)]
    df[phys_col_bin] = pd.cut(df[phys_col], bins=nbins,
                              labels=lbs, include_lowest=True)

    df.boxplot(column=[exp_col], by=[phys_col_bin],
               grid=False, fontsize=tit_size)
    plt.xticks(rotation=45, ha='right')
    plt.ylabel(exp_col, size=tit_size)
    plt.xlabel(phys_col, size=tit_size)
    plt.title('')
    if save is not None:
        plt.savefig('results/{}'.format(save))
    # plt.show()
    # plt.close()


def embedding_(dataset, color, title=None, size_x=None, size_y=None,
               pt_size=10, tit_size=15, dpi=100, save=None):
    """
    Visualizes fields (color) of a Scanpy AnnData object on spatial coordinates in a scatter plot.

    Parameters:
    - dataset (Scanpy AnnData): A Scanpy AnnData object containing a 'spatial' matrix in its obsm attribute, 
      which represents the spatial coordinates of the tissue.
    - color (list of str): A list of fields, which can be either gene names or columns from the `obs` attribute of the dataset,
      to use for coloring the scatter plot.
    - title (list of str, optional): Titles for each subplot. If not provided, the values from `color` are used as titles. Default is None.
    - size_x (float, optional): Width of the entire figure in inches. Default is dynamically calculated based on the number of colors.
    - size_y (float, optional): Height of the entire figure in inches. Default is dynamically calculated based on the number of colors.
    - pt_size (int, optional): Point size for scatter plots. Default is 10.
    - tit_size (int, optional): Font size for subplot titles. Default is 15.
    - dpi (int, optional): Resolution of the resulting figure in dots per inch. Default is 100.
    - save (str, optional): Filename to save the resulting plot. If None, the plot is not saved. Default is None.

    Notes:
    The function generates a scatter plot for each field provided in the `color` parameter. Each plot visualizes the spatial 
    distribution of a specific field (gene or metadata column). The fields are represented as colors in the scatter plot, 
    with each point corresponding to a location in the tissue and its color representing the expression or metadata value 
    for that location.

    The resulting figure is organized in a grid, with up to three plots per row. The size and layout of the figure are adjustable 
    via the function's parameters.
    """

    title = color if title is None else title
    ncolor = len(color)
    per_row = 3
    per_row = ncolor if ncolor < per_row else per_row
    nrows = int(np.ceil(ncolor / per_row))
    size_x = 5 * per_row if size_x is None else size_x
    size_y = 3 * nrows if size_y is None else size_y
    fig, axs = plt.subplots(nrows, per_row, figsize=(size_x, size_y), dpi=dpi)
    xy = dataset.obsm['spatial']
    x = xy[:, 0]
    y = xy[:, 1] if xy.shape[1] > 1 else np.ones_like(x)
    axs = axs.flatten() if type(axs) == np.ndarray else [axs]
    for ax in axs:
        ax.axis('off')

    for i, g in enumerate(color):
        if g in dataset.var_names:
            values = dataset[:, g].X
        elif g in dataset.obs.columns:
            values = dataset.obs[g]
        else:
            continue
        axs[i].scatter(x, y, c=np.array(values), s=pt_size)
        axs[i].set_title(title[i], size=tit_size)
    if save is not None:
        plt.savefig('results/{}'.format(save))
    # plt.show()
    # plt.tight_layout()


def scRNA_processing(input_path):
    """
    Processes scRNA-seq raw count matrix and save it as a .h5ad file to be used in following analysis.

    Parameters:
    - input_path (str): Path to the directory or filename where the scRNA-seq raw count matrix is saved.

    Returns:
    None.

    Notes:
    This function processes the scRNA-seq raw count matrix stored in the specified path. The processing steps 
    should be designed to convert the raw count matrix into a format suitable for downstream analyses.

    """
    # read scalled RNA seq from csv file
    scrna = pd.read_csv(input_path, index_col=0)
    gene_names = scrna.index
    cell_ids = scrna.columns
    scrna = sc.AnnData(scrna.values.T)
    scrna.var_names = gene_names
    scrna.obs_names = cell_ids
    scrna.raw = scrna

    # Filter variable genes
    sc.pp.highly_variable_genes(
        scrna, min_mean=0.0125, max_mean=3, min_disp=0.5)
    # only use highly variable genes for reconstruction
    # scrna = scrna[:, scrna.var['highly_variable']]

    # Run PCA
    sc.tl.pca(scrna, svd_solver='arpack')

    # Compute the neighborhood graph
    sc.pp.neighbors(scrna, n_neighbors=15, n_pcs=40)

    # Run Leiden algorithm
    sc.tl.leiden(scrna)
    sc.tl.leiden(scrna, resolution=0.1, key_added='leiden0.1')
    sc.tl.leiden(scrna, resolution=0.5, key_added='leiden0.5')
    sc.tl.leiden(scrna, resolution=2, key_added='leiden2')
    sc.tl.leiden(scrna, resolution=5, key_added='leiden5')

    # Compute UMAP
    sc.tl.umap(scrna)

    # Visualize the results
    sc.pl.umap(scrna, color=['leiden'])
    sc.pl.umap(scrna, color=['leiden0.1'])
    sc.pl.umap(scrna, color=['leiden0.5'])
    sc.pl.umap(scrna, color=['leiden2'])
    sc.pl.umap(scrna, color=['leiden5'])

    scrna.write_h5ad(
        'scrna.h5ad',
        compression=hdf5plugin.FILTERS["zstd"]
    )

    return scrna


def region_mapping(directory_scrna, directory_hcr, directory_output, mapping_criteria='correlation'):
    """
    Maps regions based on the provided scRNA-seq data and the specified mapping criteria.

    Parameters:
    - directory_scrna (str): Path to the directory where either the 'scrna.h5ad' (processed scRNA-seq data) 
      or the raw data in '.csv' format is saved.
    - directory_hcr (str): Path to the directory where the processed 'adata_*.h5ad' is saved.
    - directory_output (str): Path to the directory where the analysis results should be saved.
    - mapping_criteria (str): Criteria to use for region mapping. Default 'correltion'.

    Returns:
    None.

    Notes:
    This function processes the scRNA-seq data based on the provided mapping criteria. The results of the 
    region mapping are saved in the specified output directory.

    """

    # Single Cell Analysis and Co analysis with HCR

    # read scalled RNA seq from csv file
    if directory_scrna.split('.')[-1] != 'h5ad':
        print('processing scRNA-seq data and save it as a .h5ad file')
        scrna = scRNA_processing(directory_scrna)
    else:
        scrna = anndata.read_h5ad(directory_scrna)
    adata = anndata.read_h5ad(directory_hcr)
    # region_mapping
    # cell type mapping based on correlation
    # Assuming you have adata and scrna with 'leiden' annotation in adata.obs and scrna.obs
    # Convert the expression matrices to pandas data frames
    os.chdir(directory_output)
    hcr, day, pos, w_coord, w_niche, quantile_ratio, rescale_ratio = directory_hcr.split(
        '-')[1:]
    rescale_ratio = rescale_ratio.split('.')[0]
    file_name_format = [hcr, day, pos, w_coord,
                        w_niche, quantile_ratio, rescale_ratio]

    scrna_df = scrna.to_df()
    scrna_df['leiden'] = scrna.obs['leiden']
    scrna_df['leiden0.1'] = scrna.obs['leiden0.1']
    scrna_df['leiden0.5'] = scrna.obs['leiden0.5']
    scrna_df['leiden2'] = scrna.obs['leiden2']
    scrna_df['leiden5'] = scrna.obs['leiden5']

    adata_df = adata.to_df()
    adata_df['hierarchy'] = adata.obs['hierarchy']
    adata_df['leiden'] = adata.obs['leiden']
    adata_df['name'] = adata.obs['name']

    for leiden_resolution in ['leiden0.1', 'leiden0.5', 'leiden', 'leiden2', 'leiden5']:
        # Calculate the mean gene expression profile for each cluster
        hcr_means = adata_df.groupby('hierarchy').mean()
        scrna_means = scrna_df.groupby(leiden_resolution).mean()

        # Make sure that both data frames have the same columns (genes) in the same order
        common_genes = hcr_means.columns.intersection(
            scrna_means.columns)
        hcr_means = hcr_means[common_genes]
        scrna_means = scrna_means[common_genes]

        # Standardize the data (optional, but can help with Pearson correlation)
        hcr_means = zscore(hcr_means, axis=1)
        scrna_means = zscore(scrna_means, axis=1)

        # Calculate the similarity matrix using Pearson correlation
        # I can apply the concept of COVET in cluster-cluster mapping
        # I can also try CCA
        # HCR uses markers to annotate region of organoids (mapping regions information to scRNA in probability / likelihood format)
        # dorsal ventral, non-telecephalon, telecephalon region
        # also find the gene set works best -> find the most informative genes
        # size of the feature sapce is more critical than whether RNA or protein
        if mapping_criteria == 'pearson' or 'correlation':
            similarity_matrix = 1 - \
                pairwise_distances(
                    hcr_means, scrna_means, metric='correlation')
        elif mapping_criteria == 'cosine':
            similarity_matrix = 1 - \
                pairwise_distances(
                    hcr_means, scrna_means, metric='cosine')
        elif mapping_criteria == 'spearman':
            print('not implemented yet, use correlation instead')
            similarity_matrix = 1 - \
                pairwise_distances(
                    hcr_means, scrna_means, metric='correlation')

        # Match clusters
        hcr_to_scrna_mapping = {}
        for i, hcr_cluster in enumerate(hcr_means.index):
            scrna_cluster = scrna_means.index[np.argmax(
                similarity_matrix[i])]
            hcr_to_scrna_mapping[hcr_cluster] = scrna_cluster

        scrna_to_hcr_mapping = {}
        for i, scrna_cluster in enumerate(scrna_means.index):
            hcr_cluster = hcr_means.index[np.argmax(
                similarity_matrix[:, i])]
            scrna_to_hcr_mapping[scrna_cluster] = hcr_cluster

        # the mapping is not mutual which makes sense:
        # 1. the data are not the same
        # 2. the markers used may not represent the cell types
        # 3. the clustering methods used are not the same

        # save mapping cell type to adata or scrna (try scrna first since it has more clusters)
        # Invert the hcr_to_scrna_mapping dictionary to create the scrna_to_hcr_mapping dictionary
        # scrna_to_hcr_mapping = {v: k for k, v in hcr_to_scrna_mapping.items()}

        # Create a new column in scrna.obs called 'adata' and populate it based on scrna_to_hcr_mapping and scrna.obs['leiden']
        scrna.obs['hcr_hierarchy_{}'.format(
            leiden_resolution)] = scrna.obs[leiden_resolution].map(scrna_to_hcr_mapping)
        adata.obs['scrna_{}'.format(leiden_resolution)] = adata.obs['hierarchy'].map(
            hcr_to_scrna_mapping)
        # Visualize the results
        sc.pl.umap(scrna, color=[leiden_resolution], save='_scrna_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
            leiden_resolution, *file_name_format), show=False)
        sc.pl.umap(scrna, color=['hcr_hierarchy_{}'.format(leiden_resolution)], save='_scrna_hierarchy_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
            leiden_resolution, *file_name_format), show=False)
        sc.pl.draw_graph(adata, color='scrna_{}'.format(leiden_resolution), ncols=5,
                         neighbors_key='neighbors_sp', save='_scrna_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

    sc.pl.umap(scrna, color=common_genes, ncols=5, save='_scrna_genes_{}_{}_{}_{}_{}_{}_{}.png'.format(
        *file_name_format), show=False)

    # NovoSpaRc
    num_cells = min(scrna.shape[0], 3000)
    scrna_copy = scrna.copy()
    sc.pp.subsample(scrna_copy, n_obs=num_cells)
    dge_rep = pd.DataFrame(scrna_copy.obsm['X_pca'])
    # num_of_locations should be larger than num_of_cells
    num_locations = max(
        num_cells*2, min(scrna.shape[0], adata.shape[0]))
    adata_copy = adata.copy()
    sc.pp.subsample(adata_copy, n_obs=num_locations)
    sub_positions = adata_copy.obsm['X_draw_graph_fa']

    adata_copy.obsm['spatial'] = sub_positions
    pl_genes = adata_copy.var.index.tolist()
    novosparc.pl.embedding(adata_copy, pl_genes)

    # visualizing loc-loc expression distances vs their physical distances
    plot_exp_loc_dists_(
        adata_copy.X, sub_positions, save='loc_exp_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

    # # calculate Moran's I for each gene in the atlas, how spatially informative is
    atlas = adata_copy  # atlas = the reference
    atlas_genes = adata_copy.var.index.tolist()
    mI, pvals = novosparc.an.get_moran_pvals(
        atlas.X, sub_positions)
    df_mI = pd.DataFrame(
        {'moransI': mI, 'pval': pvals}, index=atlas_genes)
    gene_max_mI = df_mI['moransI'].idxmax()
    gene_min_mI = df_mI['moransI'].idxmin()
    embedding_(atlas, [gene_max_mI, gene_min_mI],
               save='NS_mI_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
    df_mI.to_csv(
        'results/mI_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format))
    dataset_reconsts = []
    # The reconstruction based on scRNA-seq tissue
    for gene_removed in adata_copy.var.index.tolist():
        tissue = novosparc.cm.Tissue(
            dataset=scrna_copy, locations=sub_positions)

        # kick one gene out
        atlas_genes = adata.var.index.tolist()
        gene_names = scrna_copy.var.index.tolist()
        # params for smooth cost
        num_neighbors_s = num_neighbors_t = 8
        # params for linear cost
        markers = list(set(atlas_genes).intersection(gene_names))

        markers.remove(gene_removed)
        num_markers = len(markers)
        num_genes = scrna_copy.shape[1]
        atlas_matrix = adata_copy.to_df()[markers].values
        markers_idx = pd.DataFrame(
            {'markers_idx': np.arange(num_genes)}, index=gene_names)
        markers_to_use = np.concatenate(
            markers_idx.loc[markers].values)
        # reconstruct the tissue
        tissue.setup_reconstruction(atlas_matrix=atlas_matrix, markers_to_use=markers_to_use,
                                    num_neighbors_s=num_neighbors_s, num_neighbors_t=num_neighbors_t)
        # compute optimal transport of cells to locations
        alpha_linear = 0.8
        epsilon = 5e-3
        tissue.reconstruct(
            alpha_linear=alpha_linear, epsilon=epsilon)
        # reconstructed expression of individual genes
        # The main objective of novoSpaRc is to probabilistically map single cells onto the tissue's physical structure,
        # and infer gene expression patterns across the tissue.
        sdge = tissue.sdge
        dataset_reconst = sc.AnnData(
            pd.DataFrame(sdge.T, columns=gene_names))
        dataset_reconst.obsm['spatial'] = sub_positions
        embedding_(dataset_reconst, pl_genes, save='NS_reconst_remove{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
            gene_removed, *file_name_format))
        dataset_reconsts.append(dataset_reconst)

    # Construct scRNA tissue object
    tissue = novosparc.cm.Tissue(
        dataset=scrna_copy, locations=sub_positions)

    # use all genes from HCR as the markers
    atlas_genes = adata.var.index.tolist()
    gene_names = scrna_copy.var.index.tolist()
    # params for smooth cost
    num_neighbors_s = num_neighbors_t = 8
    # params for linear cost
    markers = list(set(atlas_genes).intersection(gene_names))
    # markers.remove(gene_removed)
    num_markers = len(markers)
    num_genes = scrna_copy.shape[1]
    atlas_matrix = adata_copy.to_df()[markers].values
    markers_idx = pd.DataFrame(
        {'markers_idx': np.arange(num_genes)}, index=gene_names)
    markers_to_use = np.concatenate(
        markers_idx.loc[markers].values)
    # reconstruct the tissue
    tissue.setup_reconstruction(atlas_matrix=atlas_matrix, markers_to_use=markers_to_use,
                                num_neighbors_s=num_neighbors_s, num_neighbors_t=num_neighbors_t)
    # compute optimal transport of cells to locations
    alpha_linear = 0.8
    epsilon = 5e-3
    tissue.reconstruct(alpha_linear=alpha_linear, epsilon=epsilon)
    # reconstructed expression of individual genes
    # The main objective of novoSpaRc is to probabilistically map single cells onto the tissue's physical structure,
    # and infer gene expression patterns across the tissue.
    sdge = tissue.sdge
    dataset_reconst = sc.AnnData(
        pd.DataFrame(sdge.T, columns=gene_names))
    dataset_reconst.obsm['spatial'] = sub_positions
    embedding_(dataset_reconst, pl_genes, save='NS_reconstruct_{}_{}_{}_{}_{}_{}_{}.png'.format(
        *file_name_format))
    embedding_(adata_copy, pl_genes, save='NS_adata_{}_{}_{}_{}_{}_{}_{}.png'.format(
        *file_name_format))

    # statistical test
    # using KDE to approx distribution, using KS test and KL divergence to check the closeness of distributions
    # from scRNA, HCR (adata), reconstructed by NovoSpaRc (dataset_reconst)

    # Assuming adata, scrna, and dataset_reconst are your data sources
    # markers_idx and n_genes should be defined
    # Initialize the DataFrame
    df_div = pd.DataFrame(columns=[
        "Gene",
        "KS Statistic (adata vs scrna)", "p-value (adata vs scrna)",
        "KS Statistic (adata vs reconst)", "p-value (adata vs reconst)",
        "KS Statistic (scrna vs reconst)", "p-value (scrna vs reconst)",
        "KL Divergence (adata vs scrna)",
        "KL Divergence (adata vs reconst)",
        "KL Divergence (scrna vs reconst)"
    ])

    # output_directory = "results"
    # os.makedirs(output_directory, exist_ok=True)

    for i, gene in enumerate(adata.var_names):
        gene_data_adata = zscore(adata.X[:, i])
        i_s = markers_idx.loc[gene].values[0]
        gene_data_scrna = zscore(scrna.X[:, i_s])
        gene_data_reconst = zscore(dataset_reconst.X[:, i_s])

        # Calculate the KDEs
        kde_adata = gaussian_kde(gene_data_adata)
        kde_scrna = gaussian_kde(gene_data_scrna)
        kde_reconst = gaussian_kde(gene_data_reconst)
        x_adata = np.linspace(
            min(gene_data_adata), max(gene_data_adata), 1000)
        x_scrna = np.linspace(
            min(gene_data_scrna), max(gene_data_scrna), 1000)
        x_reconst = np.linspace(
            min(gene_data_reconst), max(gene_data_reconst), 1000)

        y_adata = kde_adata(x_adata)
        y_scrna = kde_scrna(x_scrna)
        y_reconst = kde_reconst(x_reconst)

        # Find the peaks
        # don't know the exact distance
        peaks_adata, _ = find_peaks(y_adata, distance=1000)
        peaks_scrna, _ = find_peaks(y_scrna, distance=1000)
        peaks_reconst, _ = find_peaks(y_reconst, distance=1000)

        # Create and save the PDF comparison plot
        fig, ax1 = plt.subplots(figsize=(10, 6))
        ax1.plot(x_adata, y_adata, label='HCR', color='tab:blue')
        ax1.plot(x_adata[peaks_adata], y_adata[peaks_adata],
                 "x", label='Peaks', color='tab:blue')
        ax1.set_xlabel('Gene Expression')
        ax1.set_ylabel('PDF (HCR)', color='tab:blue')
        ax1.tick_params(axis='y', labelcolor='tab:blue')

        ax2 = ax1.twinx()
        ax2.plot(x_scrna, y_scrna, label='scRNA-seq', color='tab:orange')
        ax2.plot(x_scrna[peaks_scrna], y_scrna[peaks_scrna],
                 "x", label='Peaks', color='tab:orange')
        ax2.set_ylabel('PDF (scRNA-seq)', color='tab:orange')
        ax2.tick_params(axis='y', labelcolor='tab:orange')

        ax3 = ax1.twinx()
        ax3.plot(x_reconst, y_reconst, label='reconst', color='tab:green')
        ax3.plot(x_reconst[peaks_reconst], y_reconst[peaks_reconst],
                 "x", label='Peaks', color='tab:green')
        # Adjust the position of the third y-axis
        ax3.spines['right'].set_position(('outward', 60))
        ax3.set_ylabel('PDF (reconst)', color='tab:green')
        ax3.tick_params(axis='y', labelcolor='tab:green')

        # Combine the legends from all axes
        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        lines3, labels3 = ax3.get_legend_handles_labels()
        ax3.legend(lines + lines2 + lines3, labels +
                   labels2 + labels3, loc='upper right')

        plt.title(f'Gene {gene} - PDF Comparison')
        plt.tight_layout()

        # Save the PDF comparison plot as an image
        # plt.show()
        plt.savefig(
            'results/{}_comparison_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
        plt.close()

        # Create and save the histogram comparison plot
        fig, ax1 = plt.subplots(figsize=(12, 6))
        ax1.hist(gene_data_adata, bins=30, alpha=0.5,
                 label='HCR', color='tab:blue')
        ax1.set_xlabel('Gene Expression')
        ax1.set_ylabel('Counts', color='tab:blue')
        ax1.tick_params(axis='y', labelcolor='tab:blue')

        ax2 = ax1.twinx()
        ax2.hist(gene_data_scrna, bins=30, alpha=0.5,
                 label='scRNA-seq', color='tab:orange')
        ax2.set_ylabel('Counts', color='tab:orange')
        ax2.tick_params(axis='y', labelcolor='tab:orange')

        ax3 = ax1.twinx()
        ax3.hist(gene_data_reconst, bins=30, alpha=1,
                 label='reconst', color='tab:green')
        # Adjust the position of the third y-axis
        ax3.spines['right'].set_position(('outward', 60))
        ax3.set_ylabel('Counts', color='tab:green')
        ax3.tick_params(axis='y', labelcolor='tab:green')

        # Combine the legends from all axes
        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        lines3, labels3 = ax3.get_legend_handles_labels()
        ax3.legend(lines + lines2 + lines3, labels +
                   labels2 + labels3, loc='upper right')

        plt.title(f'Gene {gene} - Histogram Comparison')
        plt.tight_layout()
        # plt.show()
        plt.savefig(
            'results/{}_comparison_hist_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
        plt.close()

        # Perform Kolmogorov-Smirnov tests
        ks_statistic_adata_scrna, p_value_adata_scrna = ks_2samp(
            gene_data_adata, gene_data_scrna)
        ks_statistic_adata_reconst, p_value_adata_reconst = ks_2samp(
            gene_data_adata, gene_data_reconst)
        ks_statistic_scrna_reconst, p_value_scrna_reconst = ks_2samp(
            gene_data_scrna, gene_data_reconst)

        kl_divergence_adata_scrna = sum(kl_div(y_adata, y_scrna))
        kl_divergence_adata_reconst = sum(
            kl_div(y_adata, y_reconst))
        kl_divergence_scrna_reconst = sum(
            kl_div(y_scrna, y_reconst))

        df_div = df_div.append({
            "Gene": gene,
            "KS Statistic (HCR vs scRNA-seq)": ks_statistic_adata_scrna,
            "p-value (HCR vs scRNA-seq)": p_value_adata_scrna,
            "KS Statistic (HCR vs reconst)": ks_statistic_adata_reconst,
            "p-value (HCR vs reconst)": p_value_adata_reconst,
            "KS Statistic (scRNA-seq vs reconst)": ks_statistic_scrna_reconst,
            "p-value (scRNA-seq vs reconst)": p_value_scrna_reconst,
            "KL Divergence (HCR vs scRNA-seq)": kl_divergence_adata_scrna,
            "KL Divergence (HCR vs reconst)": kl_divergence_adata_reconst,
            "KL Divergence (scRNA-seq vs reconst)": kl_divergence_scrna_reconst
        }, ignore_index=True)

    # Save the DataFrame to CSV
    df_div.to_csv(
        'results/div_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format), index=False)

    # Perform KS and KL tests on the reconst with one gene kicked out
    dataset_reconst_1 = dataset_reconsts[0]
    dataset_reconst_2 = dataset_reconsts[1]
    dataset_reconst_3 = dataset_reconsts[2]
    dataset_reconst_4 = dataset_reconsts[3]
    dataset_reconst_5 = dataset_reconsts[4]

    df_div = pd.DataFrame(columns=[
        "Gene",
        "KS Statistic (HCR vs scRNA-seq)", "p-value (HCR vs scRNA-seq)",
        "KS Statistic (HCR vs reconst_RAX)", "p-value (HCR vs reconst_RAX)",
        "KS Statistic (scRNA-seq vs reconst_RAX)", "p-value (scRNA-seq vs reconst_RAX)",
        "KS Statistic (HCR vs reconst_TCF7L2)", "p-value (HCR vs reconst_TCF7L2)",
        "KS Statistic (scRNA-seq vs reconst_TCF7L2)", "p-value (scRNA-seq vs reconst_TCF7L2)",
        "KS Statistic (HCR vs reconst_BMP4)", "p-value (HCR vs reconst_BMP4)",
        "KS Statistic (scRNA-seq vs reconst_BMP4)", "p-value (scRNA-seq vs reconst_BMP4)",
        "KS Statistic (HCR vs reconst_WNT8B)", "p-value (HCR vs reconst_WNT8B)",
        "KS Statistic (scRNA-seq vs reconst_WNT8B)", "p-value (scRNA-seq vs reconst_WNT8B)",
        "KS Statistic (HCR vs reconst_SOX2)", "p-value (HCR vs reconst_SOX2)",
        "KS Statistic (scRNA-seq vs reconst_SOX2)", "p-value (scRNA-seq vs reconst_SOX2)",
        "KL Divergence (HCR vs scRNA-seq)",
        "KL Divergence (HCR vs reconst_RAX)",
        "KL Divergence (scRNA-seq vs reconst_RAX)",
        "KL Divergence (HCR vs reconst_TCF7L2)",
        "KL Divergence (scRNA-seq vs reconst_TCF7L2)",
        "KL Divergence (HCR vs reconst_BMP4)",
        "KL Divergence (scRNA-seq vs reconst_BMP4)",
        "KL Divergence (HCR vs reconst_WNT8B)",
        "KL Divergence (scRNA-seq vs reconst_WNT8B)",
        "KL Divergence (HCR vs reconst_SOX2)",
        "KL Divergence (scRNA-seq vs reconst_SOX2)"

    ])

    # output_directory = "results"
    # os.makedirs(output_directory, exist_ok=True)

    for i, gene in enumerate(adata.var_names):
        gene_data_adata = zscore(adata.X[:, i])
        i_s = markers_idx.loc[gene].values[0]
        gene_data_scrna = zscore(scrna.X[:, i_s])
        gene_data_reconst_1 = zscore(dataset_reconst_1.X[:, i_s])
        gene_data_reconst_2 = zscore(dataset_reconst_2.X[:, i_s])
        gene_data_reconst_3 = zscore(dataset_reconst_3.X[:, i_s])
        gene_data_reconst_4 = zscore(dataset_reconst_4.X[:, i_s])
        gene_data_reconst_5 = zscore(dataset_reconst_5.X[:, i_s])

        # Calculate the KDEs
        kde_adata = gaussian_kde(gene_data_adata)
        kde_scrna = gaussian_kde(gene_data_scrna)
        kde_reconst_1 = gaussian_kde(gene_data_reconst_1)
        kde_reconst_2 = gaussian_kde(gene_data_reconst_2)
        kde_reconst_3 = gaussian_kde(gene_data_reconst_3)
        kde_reconst_4 = gaussian_kde(gene_data_reconst_4)
        kde_reconst_5 = gaussian_kde(gene_data_reconst_5)

        x_adata = np.linspace(
            min(gene_data_adata), max(gene_data_adata), 1000)
        x_scrna = np.linspace(
            min(gene_data_scrna), max(gene_data_scrna), 1000)
        x_reconst_1 = np.linspace(
            min(gene_data_reconst_1), max(gene_data_reconst_1), 1000)
        x_reconst_2 = np.linspace(
            min(gene_data_reconst_2), max(gene_data_reconst_2), 1000)
        x_reconst_3 = np.linspace(
            min(gene_data_reconst_3), max(gene_data_reconst_3), 1000)
        x_reconst_4 = np.linspace(
            min(gene_data_reconst_4), max(gene_data_reconst_4), 1000)
        x_reconst_5 = np.linspace(
            min(gene_data_reconst_5), max(gene_data_reconst_5), 1000)

        y_adata = kde_adata(x_adata)
        y_scrna = kde_scrna(x_scrna)
        y_reconst_1 = kde_reconst_1(x_reconst_1)
        y_reconst_2 = kde_reconst_2(x_reconst_2)
        y_reconst_3 = kde_reconst_3(x_reconst_3)
        y_reconst_4 = kde_reconst_4(x_reconst_4)
        y_reconst_5 = kde_reconst_5(x_reconst_5)

        # Create and save the PDF comparison plot
        fig, ax1 = plt.subplots(figsize=(10, 6))
        ax1.plot(x_adata, y_adata, label='HCR', color='tab:blue')
        ax1.set_xlabel('Gene Expression')
        ax1.set_ylabel('PDF (HCR)', color='tab:blue')
        ax1.tick_params(axis='y', labelcolor='tab:blue')

        ax2 = ax1.twinx()
        ax2.plot(x_scrna, y_scrna, label='scRNA-seq',
                 color='tab:orange')
        ax2.set_ylabel('PDF (scRNA-seq)', color='tab:orange')
        ax2.tick_params(axis='y', labelcolor='tab:orange')

        ax3 = ax1.twinx()
        ax3.plot(x_reconst_1, y_reconst_1,
                 label='reconst_RAX', color='tab:green')
        # Adjust the position of the third y-axis
        ax3.spines['right'].set_position(('outward', 60))
        ax3.set_ylabel('PDF (reconst_RAX)', color='tab:green')
        ax3.tick_params(axis='y', labelcolor='tab:green')

        ax4 = ax1.twinx()
        ax4.plot(x_reconst_2, y_reconst_2,
                 label='reconst_TCF7L2', color='tab:pink')
        # Adjust the position of the third y-axis
        ax4.spines['right'].set_position(('outward', 120))
        ax4.set_ylabel('PDF (reconst_TCF7L2)', color='tab:pink')
        ax4.tick_params(axis='y', labelcolor='tab:pink')

        ax5 = ax1.twinx()
        ax5.plot(x_reconst_3, y_reconst_3,
                 label='reconst_BMP4', color='tab:gray')
        # Adjust the position of the third y-axis
        ax5.spines['right'].set_position(('outward', 180))
        ax5.set_ylabel('PDF (reconst_BMP4)', color='tab:gray')
        ax5.tick_params(axis='y', labelcolor='tab:gray')

        ax6 = ax1.twinx()
        ax6.plot(x_reconst_4, y_reconst_4,
                 label='reconst_WNT8B', color='tab:purple')
        # Adjust the position of the third y-axis
        ax6.spines['right'].set_position(('outward', 240))
        ax6.set_ylabel('PDF (reconst_WNT8B)', color='tab:purple')
        ax6.tick_params(axis='y', labelcolor='tab:purple')

        ax7 = ax1.twinx()
        ax7.plot(x_reconst_5, y_reconst_5,
                 label='reconst_SOX2', color='tab:brown')
        # Adjust the position of the third y-axis
        ax7.spines['right'].set_position(('outward', 300))
        ax7.set_ylabel('PDF (reconst_SOX2)', color='tab:brown')
        ax7.tick_params(axis='y', labelcolor='tab:brown')

        # Combine the legends from all axes
        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        lines3, labels3 = ax3.get_legend_handles_labels()
        lines4, labels4 = ax4.get_legend_handles_labels()
        lines5, labels5 = ax5.get_legend_handles_labels()
        lines6, labels6 = ax6.get_legend_handles_labels()
        lines7, labels7 = ax7.get_legend_handles_labels()
        ax7.legend(lines + lines2 + lines3 + lines4 + lines5 + lines6 + lines7, labels +
                   labels2 + labels3 + labels4 + labels5 + labels6 + labels7, loc='best')

        plt.title(f'Gene {gene} - PDF Comparison')
        plt.tight_layout()

        # Save the PDF comparison plot as an image

        plt.savefig(f'results/gene_{gene}_kickout_comparison.png')
        plt.show()
        plt.close()

        # Perform Kolmogorov-Smirnov tests
        ks_statistic_adata_scrna, p_value_adata_scrna = ks_2samp(
            gene_data_adata, gene_data_scrna)
        ks_statistic_adata_reconst_1, p_value_adata_reconst_1 = ks_2samp(
            gene_data_adata, gene_data_reconst_1)
        ks_statistic_scrna_reconst_1, p_value_scrna_reconst_1 = ks_2samp(
            gene_data_scrna, gene_data_reconst_2)
        ks_statistic_adata_reconst_2, p_value_adata_reconst_2 = ks_2samp(
            gene_data_adata, gene_data_reconst_2)
        ks_statistic_scrna_reconst_2, p_value_scrna_reconst_2 = ks_2samp(
            gene_data_scrna, gene_data_reconst_2)
        ks_statistic_adata_reconst_3, p_value_adata_reconst_3 = ks_2samp(
            gene_data_adata, gene_data_reconst_3)
        ks_statistic_scrna_reconst_3, p_value_scrna_reconst_3 = ks_2samp(
            gene_data_scrna, gene_data_reconst_3)
        ks_statistic_adata_reconst_4, p_value_adata_reconst_4 = ks_2samp(
            gene_data_adata, gene_data_reconst_4)
        ks_statistic_scrna_reconst_4, p_value_scrna_reconst_4 = ks_2samp(
            gene_data_scrna, gene_data_reconst_4)
        ks_statistic_adata_reconst_5, p_value_adata_reconst_5 = ks_2samp(
            gene_data_adata, gene_data_reconst_5)
        ks_statistic_scrna_reconst_5, p_value_scrna_reconst_5 = ks_2samp(
            gene_data_scrna, gene_data_reconst_5)

        # Calculate KL divergences

    #     kl_divergence_adata_scrna = np.sum(y_adata * np.log(y_adata / y_scrna))
    #     kl_divergence_adata_reconst = np.sum(y_adata * np.log(y_adata / y_reconst))
    #     kl_divergence_scrna_reconst = np.sum(y_scrna * np.log(y_scrna / y_reconst))

        kl_divergence_adata_scrna = sum(kl_div(y_adata, y_scrna))
        kl_divergence_adata_reconst_1 = sum(
            kl_div(y_adata, y_reconst_1))
        kl_divergence_scrna_reconst_1 = sum(
            kl_div(y_scrna, y_reconst_1))
        kl_divergence_adata_reconst_2 = sum(
            kl_div(y_adata, y_reconst_2))
        kl_divergence_scrna_reconst_2 = sum(
            kl_div(y_scrna, y_reconst_2))
        kl_divergence_adata_reconst_3 = sum(
            kl_div(y_adata, y_reconst_3))
        kl_divergence_scrna_reconst_3 = sum(
            kl_div(y_scrna, y_reconst_3))
        kl_divergence_adata_reconst_4 = sum(
            kl_div(y_adata, y_reconst_4))
        kl_divergence_scrna_reconst_4 = sum(
            kl_div(y_scrna, y_reconst_4))
        kl_divergence_adata_reconst_5 = sum(
            kl_div(y_adata, y_reconst_5))
        kl_divergence_scrna_reconst_5 = sum(
            kl_div(y_scrna, y_reconst_5))

        df_div = df_div.append({
            "Gene": gene,
            "KS Statistic (HCR vs scRNA-seq)": ks_statistic_adata_scrna,
            "p-value (HCR vs scRNA-seq)": p_value_adata_scrna,
            "KS Statistic (HCR vs reconst_RAX)": ks_statistic_adata_reconst_1,
            "p-value (HCR vs reconst_RAX)": p_value_adata_reconst_1,
            "KS Statistic (scRNA-seq vs reconst_RAX)": ks_statistic_scrna_reconst_1,
            "p-value (scRNA-seq vs reconst_RAX)": p_value_scrna_reconst_1,
            "KL Divergence (HCR vs scRNA-seq)": kl_divergence_adata_scrna,
            "KL Divergence (HCR vs reconst_RAX)": kl_divergence_adata_reconst_1,
            "KL Divergence (scRNA-seq vs reconst_RAX)": kl_divergence_scrna_reconst_1,
            "KS Statistic (HCR vs reconst_TCF7L2)": ks_statistic_adata_reconst_2,
            "p-value (HCR vs reconst_TCF7L2)": p_value_adata_reconst_2,
            "KS Statistic (scRNA-seq vs reconst_TCF7L2)": ks_statistic_scrna_reconst_2,
            "p-value (scRNA-seq vs reconst_TCF7L2)": p_value_scrna_reconst_2,
            "KL Divergence (HCR vs reconst_TCF7L2)": kl_divergence_adata_reconst_2,
            "KL Divergence (scRNA-seq vs reconst_TCF7L2)": kl_divergence_scrna_reconst_2,
            "KS Statistic (HCR vs reconst_BMP4)": ks_statistic_adata_reconst_3,
            "p-value (HCR vs reconst_BMP4)": p_value_adata_reconst_3,
            "KS Statistic (scRNA-seq vs reconst_BMP4)": ks_statistic_scrna_reconst_3,
            "p-value (scRNA-seq vs reconst_BMP4)": p_value_scrna_reconst_3,
            "KL Divergence (HCR vs reconst_BMP4)": kl_divergence_adata_reconst_3,
            "KL Divergence (scRNA-seq vs reconst_BMP4)": kl_divergence_scrna_reconst_3,
            "KS Statistic (HCR vs reconst_WNT8B)": ks_statistic_adata_reconst_4,
            "p-value (HCR vs reconst_WNT8B)": p_value_adata_reconst_4,
            "KS Statistic (scRNA-seq vs reconst_WNT8B)": ks_statistic_scrna_reconst_4,
            "p-value (scRNA-seq vs reconst_WNT8B)": p_value_scrna_reconst_4,
            "KL Divergence (HCR vs reconst_WNT8B)": kl_divergence_adata_reconst_4,
            "KL Divergence (scRNA-seq vs reconst_WNT8B)": kl_divergence_scrna_reconst_4,
            "KS Statistic (HCR vs reconst_SOX2)": ks_statistic_adata_reconst_5,
            "p-value (HCR vs reconst_SOX2)": p_value_adata_reconst_5,
            "KS Statistic (scRNA-seq vs reconst_SOX2)": ks_statistic_scrna_reconst_5,
            "p-value (scRNA-seq vs reconst_SOX2)": p_value_scrna_reconst_5,
            "KL Divergence (HCR vs reconst_SOX2)": kl_divergence_adata_reconst_5,
            "KL Divergence (scRNA-seq vs reconst_SOX2)": kl_divergence_scrna_reconst_5

        }, ignore_index=True)

    # Save the DataFrame to CSV
    df_div.to_csv("results/div_gene_removed.csv", index=False)


if __name__ == '__main__':
    region_mapping("/cluster/project/treutlein/USERS/yihliu/data/scrna.h5ad",
                   "/cluster/project/treutlein/USERS/yihliu/data/adata_HCR18_pos4_0.1_0.1_0.2_0.0625.h5ad", "/cluster/project/treutlein/USERS/yihliu")
