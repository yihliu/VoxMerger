# Standard libraries
import os
import itertools

# External libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import hdf5plugin
import pymeshfix

# Image processing libraries
from skimage import io, measure, exposure
from skimage.transform import rescale, resize, downscale_local_mean
from skimage.filters import threshold_mean, median, gaussian, try_all_threshold, threshold_otsu
from skimage.morphology import disk, area_closing, remove_small_objects
from skimage.measure import label, marching_cubes
from scipy import ndimage as ndi
import tifffile as tif
import trimesh
from trimesh.voxel.ops import matrix_to_marching_cubes
from trimesh.visual.color import ColorVisuals
from trimesh.smoothing import filter_taubin
from sklearn.metrics import silhouette_score
from sklearn.neighbors import kneighbors_graph

# Data processing and analysis libraries
import scanpy as sc
import anndata


os.chdir("/cluster/project/treutlein/USERS/yihliu")


def mask_based_on_dapi(img, z_start=0, z_end=None, sigma=1, neighborhood_size=5,
                       min_object_size=1000, dilation_iterations=50, thresh=0.2, show=False, contour=False):
    """
    Processes each slice in a 3D image, applies preprocessing steps, and identifies objects.

    Parameters:
    - img (numpy.ndarray): 3D image to be processed.
    - z_start (int): Starting slice for processing. Default is 0.
    - z_end (int, optional): Ending slice for processing. If None, processes until the last slice. Default is None.
    - sigma (float): Sigma parameter for Gaussian filter. Default is 1.
    - neighborhood_size (int): Size of the neighborhood for local thresholding. Default is 5.
    - min_object_size (int): Minimum size of objects to be retained after morphological processing. Default is 1000.
    - dilation_iterations (int): Number of iterations for morphological dilation. Default is 50.
    - thresh (float): Threshold for object identification. Default is 0.2.
    - show (bool): If True, displays intermediate processing steps. Default is False.
    - contour (bool): If True, displays object contours on original image. Default is False.

    Returns:
    - numpy.ndarray: Processed and masked 3D image.
    """

    masks = []
    contours = []
    if z_end == None:
        z_end = img.shape[0]-1

    for z in range(z_start, z_end + 1):
        image_slice = img[z, 0]
        # Option2: gaussain - median - quantile(binary)
        smoothed_image = gaussian(image_slice, sigma=sigma)

        filtered_image = median(smoothed_image, footprint=np.ones((neighborhood_size, neighborhood_size)))
        threshold = np.quantile(filtered_image[filtered_image > 0], thresh)
        filtered_image = filtered_image > threshold

        labeled_image = label(filtered_image, connectivity=2)

        filtered_labels = remove_small_objects(
            labeled_image, min_size=min_object_size)

        connected_labels = ndi.binary_dilation(filtered_labels, iterations=dilation_iterations)
        filled_labels = ndi.binary_fill_holes(connected_labels)
        filled_labels = ndi.binary_erosion(filled_labels, iterations=dilation_iterations)

        contour_slice = measure.find_contours(filled_labels, level=0.5)
        contours.append(contour_slice)

        masks.append(filled_labels)

    if show:
        num_rows = z_end - z_start + 1
        fig, axes = plt.subplots(num_rows, 2, figsize=(12, 6 * num_rows))
        fig.suptitle('Masks for each slice')
        if contour:
            for i, (mask, contour_slice) in enumerate(zip(masks, contours)):
                axes[i, 0].imshow(img[z_start + i, 0],
                                  cmap='gray', vmin=0, vmax=1)
                axes[i, 0].set_title(f'Slice {z_start + i}')
                axes[i, 0].axis('off')

                axes[i, 1].imshow(mask, cmap='gray', vmin=0, vmax=1)
                axes[i, 1].set_title(f'Mask {z_start + i}')
                axes[i, 1].axis('off')

            for contour_subslice in contour_slice:
                axes[i, 1].plot(contour_subslice[:, 1],
                                contour_subslice[:, 0], linewidth=2, color='b')

        else:
            for i, mask in enumerate(masks):
                axes[i, 0].imshow(img[z_start + i, 0],
                                  cmap='gray', vmin=0, vmax=1)
                axes[i, 0].set_title(f'Slice {z_start + i}')
                axes[i, 0].axis('off')

                axes[i, 1].imshow(mask, cmap='gray', vmin=0, vmax=1)
                axes[i, 1].set_title(f'Mask {z_start + i}')
                axes[i, 1].axis('off')

        plt.tight_layout()
        plt.show()

    return masks

# define the function to calculate the proportion of clusters and of batchs


# def get_cluster_proportions(adata,
#                             cluster_key="cluster_final",
#                             sample_key="replicate",
#                             drop_values=None):
#     """
#     Input
#     =====
#     adata : AnnData object
#     cluster_key : key of `adata.obs` storing cluster info
#     sample_key : key of `adata.obs` storing sample/replicate info
#     drop_values : list/iterable of possible values of `sample_key` that you don't want

#     Returns
#     =======
#     pd.DataFrame with samples as the index and clusters as the columns and 0-100 floats
#     as values
#     """

#     adata_tmp = adata.copy()
#     sizes = adata_tmp.obs.groupby([cluster_key, sample_key]).size()
#     props = sizes.groupby(level=1).apply(
#         lambda x: 100 * x / x.sum()).reset_index()
#     props = props.pivot(columns=sample_key, index=cluster_key).T
#     props.index = props.index.droplevel(0)
#     props.fillna(0, inplace=True)

#     if drop_values is not None:
#         for drop_value in drop_values:
#             props.drop(drop_value, axis=0, inplace=True)
#     return props


# def plot_cluster_proportions(cluster_props,
#                              cluster_palette=None,
#                              xlabel_rotation=0):

#     fig, ax = plt.subplots(dpi=100)
#     fig.patch.set_facecolor("white")

#     cmap = None
#     if cluster_palette is not None:
#         cmap = sns.palettes.blend_palette(
#             cluster_palette,
#             n_colors=len(cluster_palette),
#             as_cmap=True)

#     cluster_props.plot(
#         kind="bar",
#         stacked=True,
#         ax=ax,
#         legend=None,
#         colormap=cmap
#     )

#     ax.legend(bbox_to_anchor=(1.01, 1), frameon=False, title="Cluster")
#     sns.despine(fig, ax)
#     ax.tick_params(axis="x", rotation=xlabel_rotation)
#     ax.set_xlabel(cluster_props.index.name.capitalize())
#     ax.set_ylabel("Proportion")
#     plt.grid(False)
#     fig.tight_layout()

    # return fig


# def data_describe(data: pd.DataFrame, print_flag: bool = True, save: bool = True, sample_id: str = None, batch_id: str = None, name_index: str = None):
#     '''
#     Calculate the basic statistical information of the data.
#     Parameters
#     ----------
#     data: pd.DataFrame
#         The data need to be check.
#     print_flag: boolean
#         If print the description.
#     save: boolean
#         If the description should be saved.
#     Returns
#     -------
#     '''
#     if print_flag:
#         print(sample_id, '_', batch_id)
#         print(data.describe())
#         print(data.corr())
#         print('---nonzero---')
#         print(data[data != 0].describe())
#         print(data[data != 0].corr())
#         print('-----------------')
#     if save:
#         if not os.path.exists(os.getcwd()+'/results'):
#             os.makedirs(os.getcwd()+'/results')
#         data.describe().to_csv(
#             "results/describe_{}_{}_{}.csv".format(name_index, sample_id, batch_id))
#         data.corr().to_csv("results/corr_{}_{}_{}.csv".format(name_index, sample_id, batch_id))
#         data[data != 0].describe().to_csv(
#             "results/describe_nonzero_{}_{}_{}.csv".format(name_index, sample_id, batch_id))
#         data[data != 0].corr().to_csv(
#             "results/corr_nonzero_{}_{}_{}.csv".format(name_index, sample_id, batch_id))


def apply_equalization(image, kernel_size=(3, 3, 3), clip_limit=0.01, equalize_flag='adapt'):
    # Get the shape of the image
    z, c, y, x = image.shape

    # Create an empty array to store the equalized image
    # equalized_image = np.zeros_like(image, dtype=np.uint8)
    equalized_image = np.zeros_like(image)
    # if masks is None:
    #     masks = np.ones(image.shape, dtype=np.uint8)

    # Loop over each channel and apply exposure.equalize_adapthist
    for channel in range(c):
        # Get the current channel's data
        channel_data = image[:, channel, :, :]
        if equalize_flag == 'adapt':
            # Apply exposure.equalize_adapthist to the current channel's data
            equalized_channel_data = exposure.equalize_adapthist(
                channel_data, kernel_size=kernel_size, clip_limit=clip_limit)
            # problem here, even after rescaling, the background in channel 6 and channel 3 has value around 9
            equalized_max = np.max(channel_data)
            equalized_channel_data = ((equalized_channel_data-np.min(equalized_channel_data)) * equalized_max/(
                np.max(equalized_channel_data)-np.min(equalized_channel_data))).astype(np.uint8)

        elif equalize_flag == 'eq':
            equalized_channel_data = exposure.equalize_hist(
                channel_data)  # .astype(np.uint8)
            equalized_max = np.max(channel_data)
            equalized_channel_data = ((equalized_channel_data-np.min(equalized_channel_data)) * equalized_max/(
                np.max(equalized_channel_data)-np.min(equalized_channel_data))).astype(np.uint8)


        elif equalize_flag == 're_intensity':
            p2, p98 = np.percentile(channel_data, (2, 98))
            equalized_channel_data = exposure.rescale_intensity(
                channel_data, in_range=(p2, p98)).astype(np.uint8)

        # Store the equalized channel data in the new array
        # equalized_channel_data = np.multiply(masks, equalized_channel_data)
        equalized_image[:, channel, :, :] = equalized_channel_data

    return equalized_image


# PP with option 0
dir_path = os.getcwd()+'/data'

# this is the pattern of our data
# file_pattern = "HCR17_HB4_D11/15_pos{}-{}_ALL.tif"
# file_pattern = "HCR18_HB4_D15_pos{}-{}_ALL.tif"

file_list = [filename for filename in os.listdir(
    dir_path) if filename.startswith("HCR18_HB4") and filename.endswith("_ALL.tif")]


pp_threshes = [0.1,0.3]
rescale_rates =[0.0625]

for pp_thresh in pp_threshes:
    for rescale_rate in rescale_rates:

        # Loop over the `adata` object names and concatenate them into `adata_concat`
        HCR18_D15_pos3_list = []
        HCR18_D15_pos4_list = []

        # Image Processing (final version)
        # Global parameters: pp_thresh
        adata_names = []
        dapi_mean = []
        dapi_median = [] # not used
        batch_full_sizes = []
        imgs_masked = []
        file_name_formats = []

        for file in file_list:
            sample_id, batch_id = file.split("-")
            sample_id = sample_id[-1]
            batch_id = batch_id[0]

            if sample_id != '3' and sample_id != '4':
                continue
            file_name_format = [sample_id,batch_id,pp_thresh,rescale_rate]
            file_name_formats.append(file_name_format)
            

            os.chdir(dir_path)
            img = io.imread(file)
            os.chdir("/cluster/project/treutlein/USERS/yihliu")
            
            # Get the mask based on dapi
            masks = mask_based_on_dapi(img)
            # Apply quantile threshod, median, gaussian
            img_masked = np.zeros_like(img)
            img_median = np.zeros_like(img)

            kernel_size = (img.shape[0] // 5,
                        img.shape[2] // 50,
                        img.shape[3] // 50)
            kernel_size = np.array(kernel_size)

            # Step 1: Equalization
            # rescale the image to the range of 0-equalized_max
            equalized_image = apply_equalization(img, kernel_size=kernel_size)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_equalized.tif'.format(*file_name_format), equalized_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            # Step 2: Median
            img_median = np.zeros_like(equalized_image)
            img_masked = np.zeros_like(img_median)
            img_quantile = np.zeros_like(img_masked)
            for j in range(img.shape[1]):
                for i in range(img.shape[0]):
                    img_median[i, j] = median(equalized_image[i, j], disk(5))
                    
                # Step 3: Mask
                img_masked[:, j] = np.multiply(masks, img_median[:, j])
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_median.tif'.format(*file_name_format), img_median, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_masked.tif'.format(*file_name_format), img_masked, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            dapi_mean.append(np.mean(np.ma.masked_equal(img_masked[:,0],0)))
            imgs_masked.append(img_masked)
            
        dapi_scale_rates = dapi_mean/np.mean(dapi_mean)

        for img_masked, dapi_scale_rate, file_name_format in zip(imgs_masked, dapi_scale_rates, file_name_formats):
            # Step4: Calibration
            img_quantile = np.zeros_like(img_masked)
            img_masked = img_masked / dapi_scale_rate
            clipped_arr = np.clip(img_masked, 0, 255)
            clipped_arr = clipped_arr.astype(np.uint8)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_calibrated.tif'.format(*file_name_format), clipped_arr, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            for j in range(img_masked.shape[1]):
                # Step 5: Quantile (it should be applied to the 3D image? or 2D)
                if len(np.unique(img_masked[:, j])) == 1:  # if all values are the same
                    thresh = 0
                else:
                    thresh = np.quantile(img_masked[:, j][img_masked[:, j] != 0], pp_thresh)
                img_quantile[:, j] = np.where(img_masked[:, j] > thresh, img_masked[:, j], 0)

            clipped_arr = np.clip(img_quantile, 0, 255)
            clipped_arr = clipped_arr.astype(np.uint8)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_quantiled.tif'.format(*file_name_format), clipped_arr, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

            # Step 6: rescale (optional)
            img_rescale = resize(img_quantile, (img_quantile.shape[0], img_quantile.shape[1], int(img_quantile.shape[2]*rescale_rate), int(img_quantile.shape[3]*rescale_rate)),
                                preserve_range=True, anti_aliasing=False)  # .astype(np.uint8)

            batch_full_sizes.append(
                img_rescale.shape[0] * img_rescale.shape[2] * img_rescale.shape[3])

            # Reshape 4D array into 2D array
            img_flat = img_rescale.transpose(
                (3, 2, 0, 1)).reshape((-1, img_rescale.shape[1]))

            # Create a dataframe with the reshaped array
            sample_id, batch_id, pp_thresh, rescale_rate = file_name_format
            
            colnames = ['dapi', 'RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']
            rownames = [(i, j, k, int(sample_id), int(batch_id)) for i in range(img_rescale.shape[3])
                        for j in range(img_rescale.shape[2]) for k in range(img_rescale.shape[0])]
            # vm = pd.DataFrame(data=img_flat.astype(np.uint8),
            #                 index=rownames, columns=colnames)
            vm = pd.DataFrame(data=img_flat,
                            index=rownames, columns=colnames)

            # data_describe(vm,sample_id = sample_id, batch_id = batch_id, name_index = 'pp')

            obs_df = pd.DataFrame(index=vm.index)
            var_df = pd.DataFrame(index=vm.columns[1:])
            adata = sc.AnnData(X=vm.iloc[:, 1:].values, obs=obs_df, var=var_df)

            adata_name = 'adata'+sample_id+'_'+batch_id
            adata_names.append(adata_name)
            globals()[adata_name] = adata

        # the code in this block should be improved as a function, since we cannot know and assign number of adata_concat_ in advance
        # Assume that `adata_names` is a list of 5 `adata` object names, e.g.,

        # Initialize an empty `AnnData` object to hold the concatenated data
        adata_concat = anndata.AnnData()


        for adata_name in adata_names:
            adata = globals()[adata_name]
            # since here we change adata.X, we cannot rerun this code
            adata.obs['name'] = adata_name.split('adata')[1]
            if adata_name.split('_')[0][-1] == '3':
                HCR18_D15_pos3_list.append(adata)
            elif adata_name.split('_')[0][-1] == '4':
                HCR18_D15_pos4_list.append(adata)


        HCR18_D15_pos3 = anndata.concat(HCR18_D15_pos3_list, label='batch')
        HCR18_D15_pos4 = anndata.concat(HCR18_D15_pos4_list, label='batch')

        index = []
        for i in HCR18_D15_pos3.obs.index:
            result = '_'.join(str(x) for x in i)
            index.append(result)
        HCR18_D15_pos3.obs.index = index

        index = []
        for i in HCR18_D15_pos4.obs.index:
            result = '_'.join(str(x) for x in i)
            index.append(result)
        HCR18_D15_pos4.obs.index = index



        HCR18_D15_pos3.write_h5ad("HCR18_D15_pos3_preprocessed_thresh{}_{}.h5ad".format(
            str(pp_thresh), str(rescale_rate)), compression=hdf5plugin.FILTERS["zstd"])
        HCR18_D15_pos4.write_h5ad("HCR18_D15_pos4_preprocessed_thresh{}_{}.h5ad".format(
            str(pp_thresh), str(rescale_rate)), compression=hdf5plugin.FILTERS["zstd"])


for pp_thresh in pp_threshes:
    for rescale_rate in rescale_rates:

        # Loop over the `adata` object names and concatenate them into `adata_concat`
        HCR18_D15_pos1_list = []
        HCR18_D15_pos2_list = []

        # Image Processing (final version)
        # Global parameters: pp_thresh
        adata_names = []
        dapi_mean = []
        dapi_median = [] # not used
        batch_full_sizes = []
        imgs_masked = []
        file_name_formats = []

        for file in file_list:
            sample_id, batch_id = file.split("-")
            sample_id = sample_id[-1]
            batch_id = batch_id[0]

            if sample_id != '1' and sample_id != '2':
                continue
            file_name_format = [sample_id,batch_id,pp_thresh,rescale_rate]
            file_name_formats.append(file_name_format)
            

            os.chdir(dir_path)
            img = io.imread(file)
            os.chdir("/cluster/project/treutlein/USERS/yihliu")
            
            # Get the mask based on dapi
            masks = mask_based_on_dapi(img)
            # Apply quantile threshod, median, gaussian
            img_masked = np.zeros_like(img)
            img_median = np.zeros_like(img)

            kernel_size = (img.shape[0] // 5,
                        img.shape[2] // 50,
                        img.shape[3] // 50)
            kernel_size = np.array(kernel_size)

            # Step 1: Equalization
            # rescale the image to the range of 0-equalized_max
            equalized_image = apply_equalization(img, kernel_size=kernel_size)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_equalized.tif'.format(*file_name_format), equalized_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            # Step 2: Median
            img_median = np.zeros_like(equalized_image)
            img_masked = np.zeros_like(img_median)
            img_quantile = np.zeros_like(img_masked)
            for j in range(img.shape[1]):
                for i in range(img.shape[0]):
                    img_median[i, j] = median(equalized_image[i, j], disk(5))
                    
                # Step 3: Mask
                img_masked[:, j] = np.multiply(masks, img_median[:, j])
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_median.tif'.format(*file_name_format), img_median, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_masked.tif'.format(*file_name_format), img_masked, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            dapi_mean.append(np.mean(np.ma.masked_equal(img_masked[:,0],0)))
            imgs_masked.append(img_masked)
            
        dapi_scale_rates = dapi_mean/np.mean(dapi_mean)

        for img_masked, dapi_scale_rate, file_name_format in zip(imgs_masked, dapi_scale_rates, file_name_formats):
            # Step4: Calibration
            img_quantile = np.zeros_like(img_masked)
            img_masked = img_masked / dapi_scale_rate
            clipped_arr = np.clip(img_masked, 0, 255)
            clipped_arr = clipped_arr.astype(np.uint8)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_calibrated.tif'.format(*file_name_format), clipped_arr, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            for j in range(img_masked.shape[1]):
                # Step 5: Quantile (it should be applied to the 3D image? or 2D)
                if len(np.unique(img_masked[:, j])) == 1:  # if all values are the same
                    thresh = 0
                else:
                    thresh = np.quantile(img_masked[:, j][img_masked[:, j] != 0], pp_thresh)
                img_quantile[:, j] = np.where(img_masked[:, j] > thresh, img_masked[:, j], 0)
            clipped_arr = np.clip(img_quantile, 0, 255)
            clipped_arr = clipped_arr.astype(np.uint8)
            tif.imsave('results/HCR18_HB4_pos{}_{}_{}_{}_quantiled.tif'.format(*file_name_format), clipped_arr, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

            # Step 6: rescale (optional)
            img_rescale = resize(img_quantile, (img_quantile.shape[0], img_quantile.shape[1], int(img_quantile.shape[2]*rescale_rate), int(img_quantile.shape[3]*rescale_rate)),
                                preserve_range=True, anti_aliasing=False)  # .astype(np.uint8)

            batch_full_sizes.append(
                img_rescale.shape[0] * img_rescale.shape[2] * img_rescale.shape[3])

            # Reshape 4D array into 2D array
            img_flat = img_rescale.transpose(
                (3, 2, 0, 1)).reshape((-1, img_rescale.shape[1]))

            # Create a dataframe with the reshaped array
            sample_id, batch_id, pp_thresh, rescale_rate = file_name_format
            
            colnames = ['dapi', 'SFRP2', 'DCX', 'SIX3', 'FOXG1', 'FEZF1']
            rownames = [(i, j, k, int(sample_id), int(batch_id)) for i in range(img_rescale.shape[3])
                        for j in range(img_rescale.shape[2]) for k in range(img_rescale.shape[0])]
            # vm = pd.DataFrame(data=img_flat.astype(np.uint8), index=rownames, columns=colnames)
            vm = pd.DataFrame(data=img_flat,
                            index=rownames, columns=colnames)

            # data_describe(vm,sample_id = sample_id, batch_id = batch_id, name_index = 'pp')

            obs_df = pd.DataFrame(index=vm.index)
            var_df = pd.DataFrame(index=vm.columns[1:])
            adata = sc.AnnData(X=vm.iloc[:, 1:].values, obs=obs_df, var=var_df)

            adata_name = 'adata'+sample_id+'_'+batch_id
            adata_names.append(adata_name)
            globals()[adata_name] = adata

        # the code in this block should be improved as a function, since we cannot know and assign number of adata_concat_ in advance
        # Assume that `adata_names` is a list of 5 `adata` object names, e.g.,

        # Initialize an empty `AnnData` object to hold the concatenated data
        adata_concat = anndata.AnnData()


        for adata_name in adata_names:
            adata = globals()[adata_name]
            # since here we change adata.X, we cannot rerun this code
            adata.obs['name'] = adata_name.split('adata')[1]
            if adata_name.split('_')[0][-1] == '1':
                HCR18_D15_pos1_list.append(adata)
            elif adata_name.split('_')[0][-1] == '2':
                HCR18_D15_pos2_list.append(adata)


        HCR18_D15_pos1 = anndata.concat(HCR18_D15_pos1_list, label='batch')
        HCR18_D15_pos2 = anndata.concat(HCR18_D15_pos2_list, label='batch')

        index = []
        for i in HCR18_D15_pos1.obs.index:
            result = '_'.join(str(x) for x in i)
            index.append(result)
        HCR18_D15_pos1.obs.index = index

        index = []
        for i in HCR18_D15_pos2.obs.index:
            result = '_'.join(str(x) for x in i)
            index.append(result)
        HCR18_D15_pos2.obs.index = index



        HCR18_D15_pos1.write_h5ad("HCR18_D15_pos1_preprocessed_thresh{}_{}.h5ad".format(
            str(pp_thresh), str(rescale_rate)), compression=hdf5plugin.FILTERS["zstd"])
        HCR18_D15_pos2.write_h5ad("HCR18_D15_pos2_preprocessed_thresh{}_{}.h5ad".format(
            str(pp_thresh), str(rescale_rate)), compression=hdf5plugin.FILTERS["zstd"])


file_list = [filename for filename in os.listdir(dir_path) if filename.startswith("HCR17_HB4_D15_pos") and filename.endswith("_ALL.tif")]


for pp_thresh in pp_threshes:
    for rescale_rate in rescale_rates:

        # Loop over the `adata` object names and concatenate them into `adata_concat`
        HCR17_D15_pos3_list = []
        HCR17_D15_pos4_list = []

        # Image Processing (final version)
        # Global parameters: pp_thresh
        adata_names = []
        dapi_mean = []
        dapi_median = [] # not used
        batch_full_sizes = []
        imgs_masked = []
        file_name_formats = []


        for file in file_list:
            sample_id, batch_id = file.split("-")
            sample_id = sample_id[-1]
            batch_id = batch_id[0]

            if sample_id != '3' and sample_id != '4':
                continue
            file_name_format = [sample_id,batch_id,pp_thresh,rescale_rate]
            file_name_formats.append(file_name_format)
            

            os.chdir(dir_path)
            img = io.imread(file)
            os.chdir("/cluster/project/treutlein/USERS/yihliu")
            
            # Get the mask based on dapi
            masks = mask_based_on_dapi(img)
            # Apply quantile threshod, median, gaussian
            img_masked = np.zeros_like(img)
            img_median = np.zeros_like(img)

            kernel_size = (img.shape[0] // 5,
                        img.shape[2] // 50,
                        img.shape[3] // 50)
            kernel_size = np.array(kernel_size)

            # Step 1: Equalization
            # rescale the image to the range of 0-equalized_max
            equalized_image = apply_equalization(img, kernel_size=kernel_size)
            tif.imsave('results/HCR17_HB4_pos{}_{}_{}_{}_equalized.tif'.format(*file_name_format), equalized_image, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            # Step 2: Median
            img_median = np.zeros_like(equalized_image)
            img_masked = np.zeros_like(img_median)
            img_quantile = np.zeros_like(img_masked)
            for j in range(img.shape[1]):
                for i in range(img.shape[0]):
                    img_median[i, j] = median(equalized_image[i, j], disk(5))
                    
                # Step 3: Mask
                img_masked[:, j] = np.multiply(masks, img_median[:, j])
            tif.imsave('results/HCR17_HB4_pos{}_{}_{}_{}_median.tif'.format(*file_name_format), img_median, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            tif.imsave('results/HCR17_HB4_pos{}_{}_{}_{}_masked.tif'.format(*file_name_format), img_masked, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            dapi_mean.append(np.mean(np.ma.masked_equal(img_masked[:,0],0)))
            imgs_masked.append(img_masked)
            
        dapi_scale_rates = dapi_mean/np.mean(dapi_mean)

        for img_masked, dapi_scale_rate, file_name_format in zip(imgs_masked, dapi_scale_rates, file_name_formats):
            # Step4: Calibration
            img_quantile = np.zeros_like(img_masked)
            img_masked = img_masked / dapi_scale_rate
            clipped_arr = np.clip(img_masked, 0, 255)
            clipped_arr = clipped_arr.astype(np.uint8)
            tif.imsave('results/HCR17_HB4_pos{}_{}_{}_{}_calibrated.tif'.format(*file_name_format), clipped_arr, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)
            
            for j in range(img_masked.shape[1]):
                # Step 5: Quantile (it should be applied to the 3D image? or 2D)
                if len(np.unique(img_masked[:, j])) == 1:  # if all values are the same
                    thresh = 0
                else:
                    thresh = np.quantile(img_masked[:, j][img_masked[:, j] != 0], pp_thresh)
                img_quantile[:, j] = np.where(img_masked[:, j] > thresh, img_masked[:, j], 0)
            clipped_arr = np.clip(img_quantile, 0, 255)
            clipped_arr = clipped_arr.astype(np.uint8)
            tif.imsave('results/HCR17_HB4_pos{}_{}_{}_{}_quantiled.tif'.format(*file_name_format), clipped_arr, imagej=True, metadata={'axes': 'ZCYX'}, dtype=np.uint8)

            # Step 6: rescale (optional)
            img_rescale = resize(img_quantile, (img_quantile.shape[0], img_quantile.shape[1], int(img_quantile.shape[2]*rescale_rate), int(img_quantile.shape[3]*rescale_rate)),
                                preserve_range=True, anti_aliasing=False)  # .astype(np.uint8)

            batch_full_sizes.append(
                img_rescale.shape[0] * img_rescale.shape[2] * img_rescale.shape[3])

            # Reshape 4D array into 2D array
            img_flat = img_rescale.transpose(
                (3, 2, 0, 1)).reshape((-1, img_rescale.shape[1]))

            # Create a dataframe with the reshaped array
            sample_id, batch_id, pp_thresh, rescale_rate = file_name_format
            
            colnames = ['dapi', 'SFRP2','HESX1','SIX3','FOXG1','WLS']
            rownames = [(i, j, k, int(sample_id), int(batch_id)) for i in range(img_rescale.shape[3])
                        for j in range(img_rescale.shape[2]) for k in range(img_rescale.shape[0])]
            # vm = pd.DataFrame(data=img_flat.astype(np.uint8),
            #                 index=rownames, columns=colnames)
            vm = pd.DataFrame(data=img_flat,
                            index=rownames, columns=colnames)
            # data_describe(vm,sample_id = sample_id, batch_id = batch_id, name_index = 'pp')

            obs_df = pd.DataFrame(index=vm.index)
            var_df = pd.DataFrame(index=vm.columns[1:])
            adata = sc.AnnData(X=vm.iloc[:, 1:].values, obs=obs_df, var=var_df)

            adata_name = 'adata'+sample_id+'_'+batch_id
            adata_names.append(adata_name)
            globals()[adata_name] = adata

        # the code in this block should be improved as a function, since we cannot know and assign number of adata_concat_ in advance
        # Assume that `adata_names` is a list of 5 `adata` object names, e.g.,

        # Initialize an empty `AnnData` object to hold the concatenated data
        adata_concat = anndata.AnnData()


        for adata_name in adata_names:
            adata = globals()[adata_name]
            # since here we change adata.X, we cannot rerun this code
            adata.obs['name'] = adata_name.split('adata')[1]
            if adata_name.split('_')[0][-1] == '3':
                HCR17_D15_pos3_list.append(adata)
            elif adata_name.split('_')[0][-1] == '4':
                HCR17_D15_pos4_list.append(adata)


        HCR17_D15_pos3 = anndata.concat(HCR17_D15_pos3_list, label='batch')
        HCR17_D15_pos4 = anndata.concat(HCR17_D15_pos4_list, label='batch')

        index = []
        for i in HCR17_D15_pos3.obs.index:
            result = '_'.join(str(x) for x in i)
            index.append(result)
        HCR17_D15_pos3.obs.index = index

        index = []
        for i in HCR17_D15_pos4.obs.index:
            result = '_'.join(str(x) for x in i)
            index.append(result)
        HCR17_D15_pos4.obs.index = index



        HCR17_D15_pos3.write_h5ad("HCR17_D15_pos3_preprocessed_thresh{}_{}.h5ad".format(
            str(pp_thresh), str(rescale_rate)), compression=hdf5plugin.FILTERS["zstd"])
        HCR17_D15_pos4.write_h5ad("HCR17_D15_pos4_preprocessed_thresh{}_{}.h5ad".format(
            str(pp_thresh), str(rescale_rate)), compression=hdf5plugin.FILTERS["zstd"])
