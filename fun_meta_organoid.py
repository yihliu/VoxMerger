import heapq
import os


import anndata
import igraph as ig
import leidenalg
import matplotlib.colors as mcolors
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import networkx as nx
import novosparc
import numpy as np
import pandas as pd
import scanpy as sc
import seaborn as sns
import hdf5plugin


from fa2 import ForceAtlas2
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap
from scipy.cluster import hierarchy
from scipy.linalg import block_diag, sqrtm
from scipy.spatial import distance_matrix, squareform
from scipy.spatial.distance import pdist
from scipy.special import kl_div
from scipy.sparse import csc_matrix
from scipy.stats import spearmanr, entropy, gaussian_kde, ks_2samp, zscore
from skimage.exposure import rescale_intensity
from sklearn.metrics import silhouette_score, pairwise_distances
from sklearn.neighbors import kneighbors_graph, NearestNeighbors


def complex_array_pdist(x, y=None):
    """
    Computes pairwise Euclidean distances for arrays of complex numbers.

    Parameters:
    - x (numpy.ndarray): A 2D array where each row represents an array of complex numbers.
    - y (numpy.ndarray, optional): Another 2D array of complex numbers. If provided, the function computes distances 
        between rows of `x` and `y`. If not provided, the function computes pairwise distances within `x`. Default is None.

    Returns:
    - numpy.ndarray: If `y` is not provided, returns pairwise distances in a condensed form. If `y` is provided, returns 
        pairwise distances in a matrix form where rows represent elements from `x` and columns represent elements from `y`.

    Notes:
    The function computes the Euclidean distance between two arrays of complex numbers by treating the real and imaginary 
    parts as separate dimensions. The distance is then computed using the formula:
    \[ \text{distance} = \sqrt{\text{sum}((\text{real}_1 - \text{real}_2)^2 + (\text{imag}_1 - \text{imag}_2)^2)} \]
    """

    if y is None:
        n = len(x)
        distances = []

        for i in range(n):
            diff = x[i] - x[i+1:]
            distance = np.sqrt(np.sum(diff.real**2 + diff.imag**2, axis=1))
            distances.append(distance)
        # Concatenate results from all iterations and return condensed matrix
        return squareform(np.concatenate(distances))

    else:
        n, m = len(x), len(y)
        distances = np.zeros((n, m))
        for i in range(n):
            diff = x[i] - y
            distances[i, :] = np.sqrt(
                np.sum(diff.real**2 + diff.imag**2, axis=1))

        return distances


def knn_adjacency_matrix_from_distance(distances, k=50):
    """
    Constructs a k-nearest neighbors (k-NN) adjacency matrix from a distance matrix.

    Parameters:
    - distances (numpy.ndarray): A 2D square matrix representing pairwise distances between points.
    - k (int, optional): The number of nearest neighbors to consider for each point. Default is 50.

    Returns:
    - scipy.sparse.csc_matrix: A sparse adjacency matrix in Compressed Sparse Column (CSC) format. For each point, 
      the matrix has ones in positions corresponding to its k-nearest neighbors and zeros elsewhere.

    Notes:
    The function constructs the adjacency matrix by iterating over each point in the distance matrix and selecting 
    its k-nearest neighbors (excluding itself). The resulting adjacency matrix is a binary matrix where a cell at 
    position (i, j) is 1 if point j is among the k-nearest neighbors of point i, and 0 otherwise.
    """

    num_points = distances.shape[0]
    rows = []
    cols = []
    for i in range(num_points):
        # Exclude the diagonal (distance to self is always 0)
        neighbors = heapq.nsmallest(
            k+1, range(num_points), key=lambda j: distances[i, j])[1:]
        rows.extend(neighbors)
        cols.extend([i] * k)

    data = np.ones(len(rows))
    adjacency_matrix = csc_matrix(
        (data, (rows, cols)), shape=(num_points, num_points))

    return adjacency_matrix


def plot_clusters_with_entropy_annotation(proportions, title, file_name_format):
    """
    Generates a stacked bar plot to visualize cluster proportions with entropy annotations.

    Parameters:
    - proportions (pandas.DataFrame): A DataFrame where each row represents an organoid and columns represent cluster proportions.
      There should be an additional 'entropy' column with entropy values for each organoid.
    - title (str): Title for the resulting plot.
    - file_name_format (str): The file name (and path) to save the resulting plot.

    Notes:
    The function generates a stacked bar plot where each bar represents an organoid and the height of each segment within a bar 
    corresponds to the proportion of a cluster. The entropy values for each organoid are annotated at the top of each bar.

    This visualization provides insights into the distribution of clusters across organoids and the diversity (entropy) of 
    cluster assignments for each organoid.
    """

    fig, ax = plt.subplots(figsize=(10, 6))

    proportions.drop('entropy', axis=1).plot(kind='bar', stacked=True, ax=ax)
    ax.set_ylabel('Proportion')

    # Annotate bars with the entropy value
    for idx, value in enumerate(proportions['entropy']):
        ax.text(idx, 1, f"Entropy: {value:.2f}",
                ha='center', va='bottom', fontsize=10)

    ax.legend(loc='upper left', bbox_to_anchor=(1, 1), title='cluster')
    plt.title(title)
    plt.xlabel('Organoid Name')
    plt.xticks(rotation=45)
    plt.tight_layout()
    plt.savefig(file_name_format)
    # plt.show()


def meta_organoid_and_cluster(directory_h5ad, directory_output, weights_coordinate=0.1, weights_niche=0.1,
                              divide_ratio=10, real_size=[1.1838, 1.1838, 10], n_neighbors=50, hierarchy_c='ss'):
    """
    Create meta-organoid and do clustering and analysis based on the given parameters.

    Parameters:
    - directory_h5ad (str): Path to the directory where the images are saved.
    - directory_output (str): Path to the directory where the analysis results should be saved.
    - weights_coordinate (list of float): List of weights, no larger than 1, representing coordinate weights.
    - weights_niche (list of float): List of weights, no larger than 1, representing niche weights.
    - divide_ratio (int): Ratio to divide the dataset, aiming to reduce the computational power needed.
    - real_size (list of float): List containing the real sizes of the x, y, and z dimensions.
    - n_neighbors (int): The number of neighbors for k-nearest neighbors clustering.
    - hierarchy_c (int or str): Hierarchical clustering method, can be 'ss', 'aic', 'bic', or an integer value.

    Returns:
    - None

    Notes:
    This function processes organoid saved in .h5ad format to perform clustering and analysis based on the provided parameters.
    The output dictionary encapsulates various metrics and results that are essential for the thesis.
    """
    # TODO: modular 1, fused adjacency matrix creation
    # TODO: modular 2, meta-organoid creation
    # TODO: modular ?, is there anything in between?
    # TODO: modular 3, clustering
    # TODO: modular 4, analysis
    # TODO: for processing we can create a separate function and use .tl.name as the function
    # TODO: for visualizaiton we can create a separate function and use .pl.name as the function
    # Information: the input is a anndata object, which is created with scanpy

    organoid_list = [file for file in os.listdir(directory_h5ad)]
    os.chdir(directory_h5ad)
    for file in organoid_list:
        for w_coord in weights_coordinate:
            for w_niche in weights_niche:

                adata = anndata.read_h5ad(file)
                os.chdir(directory_output)
                hcr = file.split("_")[0]
                day = file.split("_")[1]
                pos = file.split("_")[2]
                quantile_ratio = float(file.split("_")[-2][-3:])
                rescale_ratio = float(file.split(
                    "_")[-1].split("h5ad")[0][:-1])
                file_name_format = [hcr, day, pos, w_coord, w_niche, str(
                    quantile_ratio), str(rescale_ratio)]
                # if hcr == 'HCR18' and day == 'D15':
                #     adata = adata[adata.obs['name'] != "4_3"]
                #     adata = adata[adata.obs['name'] != "4_4"]

                adata.obs_names_make_unique()
                sc.pp.filter_cells(adata, min_genes=1)
                sc.pp.filter_cells(adata, min_counts=1)
                sc.pp.log1p(adata)
                adata.raw = adata
                sc.pp.scale(adata)

                # Create voxel_df to stroe the voxel coordinates
                voxel_x, voxel_y, voxel_z = real_size

                # distance matrix based on the coordinates (should I consider the difference between the scale of z and x, y)
                voxels = adata.obs_names

                # Split the voxel names into separate dimensions"
                voxel_index = [voxel.strip("\'\'").split("_")
                               for voxel in voxels]

                # Convert this list of lists into a DataFrame"
                voxel_df = pd.DataFrame(voxel_index, columns=[
                                        'x', 'y', 'z', 'sample', 'batch'])
                voxel_df['name'] = voxel_df['sample'] + '_' + voxel_df['batch']
                # Convert the string coordinates into integers (or floats if necessary)
                for col in ['x', 'y', 'z']:
                    # Change to .astype(float) if these are floating point numbers
                    voxel_df[col] = voxel_df[col].astype(int)

                # get the real size of the x, y, z dimensions, assuming x as 1
                voxel_df['y'] = voxel_df['y'] * voxel_y / voxel_x
                voxel_df['z_real'] = voxel_df['z'] * \
                    voxel_z / voxel_x * rescale_ratio

                adata.obs['z'] = voxel_df['z_real'].values

                voxel_df[adata.var_names] = adata.X
                grouped = voxel_df.groupby(['sample', 'batch'])

                coordinates_dfs = [group_df.copy() for _, group_df in grouped]

                # Create a graph based on voxel intensities
                intensity_graph = kneighbors_graph(
                    adata.X, n_neighbors=n_neighbors, mode='connectivity')

                # Create a graph based on voxel coordinates within one organoid separately
                coordinate_graphs = []

                # Voxel niche
                genes = adata.var_names
                voxel_niches = []
                for df in coordinates_dfs:
                    # Only consider the x, y, z columns for the KNN graph
                    X = df[['x', 'y', 'z_real']].values

                    # Create the coordinate KNN graph
                    A = kneighbors_graph(
                        X, n_neighbors=n_neighbors, mode='connectivity')

                    # Calculate covariance
                    # cov = np.cov(X)

                    # Calculate voxel niche
                    nbrs = NearestNeighbors(n_neighbors=9, algorithm='auto', metric='euclidean').fit(
                        df[['x', 'y', 'z_real']])
                    distances, indices = nbrs.kneighbors(
                        df[['x', 'y', 'z_real']])

                    filtered_indices = [indices[i][dist <= 10]
                                        for i, dist in enumerate(distances)]

                    voxel_niche = np.zeros((df.shape[0], 9, len(genes)))

                    for i, idx in enumerate(filtered_indices):
                        # Use iloc here
                        voxel_niche[i, :len(
                            idx), :] = df.iloc[idx][genes].values

                    voxel_niches.append(voxel_niche)

                    # Append the resulting graph to the list
                    coordinate_graphs.append(A)
                    # coordinate_cov.append(cov)

                # Convert sparse matrices to dense? or just to array? what's the difference
                # is it possible to optimise this step?
                dense_coordinate_graphs = [A.toarray()
                                           for A in coordinate_graphs]

                # Create block diagonal matrix
                block_coordinate_graph = block_diag(*dense_coordinate_graphs)

                # block_coordinate_graph = block_coordinate_graph > 0

                voxel_niches_concatenated = np.concatenate(
                    voxel_niches, axis=0)
                total_mean_exp = np.mean(voxel_df[genes])
                # Initialize an array to store the niche covariances
                niche_covariances = np.zeros(
                    (voxel_niches_concatenated.shape[0], len(genes), len(genes)))

                # Loop through each row and calculate the covariance
                for i, niche in enumerate(voxel_niches_concatenated):
                    deviations = niche - total_mean_exp.values
                    cov_matrix = np.dot(
                        deviations.T, deviations) / (niche.shape[0] - 1)
                    niche_covariances[i] = cov_matrix

                msqr_results = []
                for cov_matrix in niche_covariances:
                    # .real.flatten()  # Compute the matrix square root
                    msqr_result = sqrtm(cov_matrix)
                    msqr_results.append(msqr_result)

                # Convert the MSQR results to a 2D array with shape (number of niches, 5 * 5)
                msqr_array = np.array([msqr_result.flatten()
                                       for msqr_result in msqr_results])

                # Divide the array into 5 chunks
                chunk_size = msqr_array.shape[0] // divide_ratio
                pairwise_distances_matrix = np.zeros(
                    (msqr_array.shape[0], msqr_array.shape[0]))

                for i in range(divide_ratio):
                    # compute diagonal covariance distance
                    start = i * chunk_size
                    end = start + chunk_size
                    if i == (divide_ratio-1):
                        end = msqr_array.shape[0]

                    chunk = msqr_array[start:end, :]
                    # pairwise_distances_chunk = complex_array_pdist(chunk)
                    pairwise_distances_matrix[start:end,
                                              start:end] = complex_array_pdist(chunk)

                    for j in range(i + 1, divide_ratio):
                        start_j = j * chunk_size
                        end_j = start_j + chunk_size
                        if j == (divide_ratio-1):
                            end_j = msqr_array.shape[0]

                        chunk_j = msqr_array[start_j:end_j, :]
                        pairwise_distances_cross = complex_array_pdist(
                            chunk, chunk_j)
                        pairwise_distances_matrix[start:end,
                                                  start_j:end_j] = pairwise_distances_cross
                        pairwise_distances_matrix[start_j:end_j,
                                                  start:end] = pairwise_distances_cross.T

                # approx 40 mins
                # Assuming you already have the full_matrix from the previous steps
                cell_niche_sparse = knn_adjacency_matrix_from_distance(
                    pairwise_distances_matrix, k=n_neighbors)

                # Convert intensity_graph to a sparse matrix
                intensity_sparse = csc_matrix(intensity_graph)
                block_coordinate_graph = csc_matrix(block_coordinate_graph)

                # Perform a weighted sum of the adjacency matrices of the two graphs

                # fused_adjacency = w * block_coordinate_graph + (1 - w) * intensity_graph.toarray()
                fused_adjacency = block_coordinate_graph.multiply(
                    w_coord) + intensity_sparse.multiply(1-w_coord-w_niche) + cell_niche_sparse.multiply(w_niche)

                # Now `fused_adjacency` is the adjacency matrix of the fused graph
                # the normalization range do not influence the PAGA graph, I have tried 1-10 and 1-100, the PAGA graphs are the same
                # Create a graph based on voxel intensities
                intensity_graph_distances = kneighbors_graph(
                    adata.X, n_neighbors=n_neighbors, mode='distance', include_self=False)
                # use the indices from adjacency KNN graph
                intensity_indices = intensity_graph.nonzero()
                # distinguish the (neighbor) voxel with the same value from the voxels that are not neighbors
                intensity_graph_distances[intensity_indices] += 1

                # Normalize the value between 1 and 10
                intensity_min = intensity_graph_distances[intensity_indices].min(
                )
                intensity_max = intensity_graph_distances[intensity_indices].max(
                )
                intensity_scale = 9 / (intensity_max-intensity_min)
                intensity_shift = 1 - intensity_min * intensity_scale

                intensity_graph_distances[intensity_indices] = intensity_graph_distances[intensity_indices] * \
                    intensity_scale + intensity_shift

                # Create a graph based on voxel coordinates within one organoid separately
                coordinate_graphs_distances = []
                for df in coordinates_dfs:
                    # Only consider the x, y, z columns for the KNN graph
                    X = df[['x', 'y', 'z_real']].values

                    # Create the KNN graph
                    A = kneighbors_graph(
                        X, n_neighbors=n_neighbors, mode='distance', include_self=False)

                    # Append the resulting graph to the list
                    # Normalize between 1 and 10
                    A_indices = A.nonzero()
                    A_min = A[A_indices].min()
                    A_max = A[A_indices].max()
                    A_scale = 9 / (A_max-A_min)
                    A_shift = 1 - A_min * A_scale

                    A[A_indices] = A[A_indices] * A_scale + A_shift
                    coordinate_graphs_distances.append(A)

                # Convert sparse matrices to dense
                dense_coordinate_graphs_distances = [
                    A.toarray() for A in coordinate_graphs_distances]

                # Create block diagonal matrix
                block_coordinate_graph_distances = block_diag(
                    *dense_coordinate_graphs_distances)

                # Cell Niche Distance KNN Graph
                # Extract non-zero row and column indices from the cell_niche_sparse
                rows = cell_niche_sparse.indices
                cols = np.repeat(np.arange(cell_niche_sparse.shape[1]), np.diff(
                    cell_niche_sparse.indptr))
                cell_niche_distances = np.zeros(
                    pairwise_distances_matrix.shape)

                # Directly extract the pairwise distances using row and column indices
                cell_niche_distances[rows,
                                     cols] = pairwise_distances_matrix[rows, cols]

                # Rescale the distance to 1-10
                niche_min = cell_niche_distances[rows, cols].min()
                niche_max = cell_niche_distances[rows, cols].max()
                niche_scale = 9 / (niche_max-niche_min)
                niche_shift = 1 - niche_min * niche_scale

                cell_niche_distances[rows, cols] = cell_niche_distances[rows,
                                                                        cols] * niche_scale + niche_shift

                # are KNN graphes for both undirected? no, but we should make them undirected
                # Convert intensity_graph to a sparse matrix
                intensity_sparse_distances = csc_matrix(
                    intensity_graph_distances)
                block_coordinate_graph_distances = csc_matrix(
                    block_coordinate_graph_distances)
                cell_niche_sparse_distances = csc_matrix(cell_niche_distances)

                # Perform a weighted sum of the adjacency matrices of the two graphs

                # fused_adjacency = w * block_coordinate_graph + (1 - w) * intensity_graph.toarray()
                fused_adjacency_distances = block_coordinate_graph_distances.multiply(
                    w_coord) + cell_niche_sparse_distances.multiply(w_niche) + intensity_sparse_distances.multiply(1-w_coord-w_niche)
                # clustering with Leiden
                # still need to find a way to speed up this step

                # Initialize an empty graph with the correct number of vertices
                n_vertices = fused_adjacency.shape[0]
                G = ig.Graph(n_vertices)

                # Add edges
                rows, cols = fused_adjacency.nonzero()
                edges = list(zip(rows, cols))
                G.add_edges(edges)

                # Add edge weights
                # Here, we directly access the data of the csc_matrix
                G.es['weight'] = fused_adjacency.data

                # compute the optimal partition with the Leiden algorithm
                leiden_partition = leidenalg.find_partition(
                    G, leidenalg.ModularityVertexPartition, weights='weight', seed=42)

                # ig.plot(partition)
                cluster_sizes = leiden_partition.sizes()
                # get cluster memberships and total number of clusters
                membership = np.array(leiden_partition.membership)
                n_clusters = len(np.unique(membership))

                # Convert sparse matrix to CSR format for efficient row slicing
                fused_csr = fused_adjacency.tocsr()

                # Initialize connectivity matrix
                connectivity = np.zeros((n_clusters, n_clusters))

                # calculate connectivity
                for cluster in range(n_clusters):
                    # get the nodes in this cluster
                    nodes_in_cluster = np.where(membership == cluster)[0]

                    # calculate connectivity for each pair of clusters
                    for other_cluster in range(n_clusters):
                        if cluster != other_cluster:
                            # get the nodes in the other cluster
                            nodes_in_other_cluster = np.where(
                                membership == other_cluster)[0]

                            # calculate connectivity
                            connectivity[cluster, other_cluster] = np.sum(
                                fused_csr[nodes_in_cluster][:, nodes_in_other_cluster])
                refined_connectivity = connectivity + connectivity.T
                # which one should I use?
                # refined_connectivity /= np.add.outer(cluster_sizes, cluster_sizes)

                refined_connectivity /= np.outer(cluster_sizes, cluster_sizes)
                # normalize the connectivity
                # the smaller the value the closer the clusters are
                refined_connectivity = refined_connectivity / \
                    np.max(refined_connectivity)
                refined_connectivity = 1 - refined_connectivity
                np.fill_diagonal(refined_connectivity, 0)
                # Convert to condensed distance matrix
                condensed_dist = squareform(refined_connectivity)

                # Cluster number selection: Silhouette method, Akaike Information Criterion (AIC) and Bayesian Information Criterion (BIC)
                # Perform hierarchical clustering
                Z = hierarchy.linkage(
                    condensed_dist, method='single', metric='euclidean')

                # Range of number of clusters to try (you can adjust this based on your needs)
                # Total number of data points
                n_clusters = len(refined_connectivity)
                k_values = range(2, n_clusters)

                # Store silhouette scores, AIC, and BIC for each number of clusters
                silhouette_scores = []
                aic_values = []
                bic_values = []

                # Loop over different values of k (number of clusters)
                for k in k_values:
                    # Obtain the cluster assignments
                    clusters = hierarchy.fcluster(Z, t=k, criterion='maxclust')

                    # Calculate AIC and BIC
                    n_data_points = len(refined_connectivity)
                    cluster_distances = []
                    for cluster_id in np.unique(clusters):
                        cluster_mask = (clusters == cluster_id)
                        cluster_data = refined_connectivity[cluster_mask,
                                                            :][:, cluster_mask]
                        cluster_dist = np.sum(cluster_data)
                        cluster_distances.append(cluster_dist)

                    num_parameters = k

                    aic = n_data_points * \
                        np.log(np.sum(cluster_distances)) + 2 * num_parameters
                    bic = n_data_points * \
                        np.log(np.sum(cluster_distances)) + \
                        num_parameters * np.log(n_data_points)

                    aic_values.append(aic)
                    bic_values.append(bic)

                    if len(np.unique(clusters)) == 1:
                        silhouette_scores.append(np.nan)
                        continue
                    # Compute the silhouette score for the current number of clusters
                    silhouette_avg = silhouette_score(
                        refined_connectivity, clusters, metric='precomputed')
                    silhouette_scores.append(silhouette_avg)


                # Plot the silhouette scores, AIC, and BIC
                plt.figure(figsize=(12, 6))
                plt.subplot(1, 2, 1)
                plt.plot(k_values, silhouette_scores, marker='o')
                plt.xlabel('Number of Clusters (k)')
                plt.ylabel('Silhouette Score')
                plt.title('Silhouette Method for Hierarchical Clustering')
                plt.xlim(min(k_values), max(k_values))

                plt.subplot(1, 2, 2)
                plt.plot(k_values, aic_values, marker='o', label='AIC')
                plt.plot(k_values, bic_values, marker='o', label='BIC')
                plt.xlabel('Number of Clusters (k)')
                plt.ylabel('Information Criterion')
                plt.title('AIC and BIC for Hierarchical Clustering')
                plt.legend()
                plt.xlim(min(k_values), max(k_values))

                plt.tight_layout()
                plt.savefig(
                    'results/clustering_info_criteria_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                # plt.show()

                # Specify the desired number of clusters
                # Replace with your desired number of clusters
                if hierarchy_c == 'ss':
                    m = k_values[np.nanargmax(silhouette_scores)]
                elif hierarchy_c == 'aic':
                    m = k_values[np.nanargmin(aic_values)]
                elif hierarchy_c == 'bic':
                    m = k_values[np.nanargmin(bic_values)]

                # Obtain the cluster assignments
                clusters = hierarchy.fcluster(Z, t=m, criterion='maxclust')
                clusters -= 1
                # Plot the dendrogram
                plt.figure(figsize=(10, 6))
                dn = hierarchy.dendrogram(Z)
                plt.xlabel('Leiden Clusters')
                plt.ylabel('Distance')
                plt.title('Hierarchical Clustering Dendrogram')
                plt.savefig(
                    'results/hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                # plt.show()

                # Print the cluster assignments
                # print("Cluster Assignments:", clusters)

                # hierarchy : leiden dictionary
                clusters_dict = {}
                for i, cluster in enumerate(clusters):
                    if cluster not in clusters_dict:
                        clusters_dict[cluster] = []
                    clusters_dict[cluster].append(i)

                # leiden : voxel (index) dictionary
                leiden_clusters_dict = {}
                for i, leiden_cluster in enumerate(leiden_partition.membership):
                    if leiden_cluster not in leiden_clusters_dict:
                        leiden_clusters_dict[leiden_cluster] = []
                    leiden_clusters_dict[leiden_cluster].append(i)

                # New dictionary for hierarchy to voxel (index) relationship
                hierarchy_voxel_dict = {}

                # Iterate over clusters_dict
                for hierarchy_cluster, leiden_clusters in clusters_dict.items():
                    voxel_indices = []

                    # Iterate over leiden_clusters in each hierarchy
                    for leiden_cluster in leiden_clusters:
                        # if leiden_cluster in leiden_clusters_dict:
                        voxel_indices.extend(
                            leiden_clusters_dict[leiden_cluster])

                    # Add hierarchy and corresponding voxel indices to hierarchy_voxel_dict
                    hierarchy_voxel_dict[hierarchy_cluster] = voxel_indices

                # Create colormap
                # colormap = plt.cm.get_cmap('tab10', len(hierarchy_voxel_dict))

                viridis = plt.cm.get_cmap('tab20', 256)
                # resample the colors
                newcolors = viridis(np.linspace(0, 0.6, m))
                newcmp = ListedColormap(newcolors)
                # newcmp.resampled(m) # not working?

                # create cmp for Leiden
                newcolors_leiden = viridis(np.linspace(
                    0, 0.6, len(clusters)))  # resample the colors
                # newcolors = viridis(np.linspace(0, 1, 256))
                # gray = np.array([211/256, 211/256, 211/256, 1])
                # newcolors[:25, :] = gray
                newcmp_leiden = ListedColormap(newcolors_leiden)
                # newcmp.resampled(m) # not working?

                # colormap = plt.cm.get_cmap('YlGnBu', 10)
                # Create an array to store the colors for each voxel index
                max_index = max(max(v) for v in hierarchy_voxel_dict.values())
                colors = np.zeros(max_index + 1)

                # Assign colors to each voxel index based on hierarchy
                for hierarchy_cluster, voxel_indices in hierarchy_voxel_dict.items():
                    color_index = int(hierarchy_cluster)
                    # rgb_index = newcmp(int(color_index/m*newcmp.N+1))[:-1]
                    colors[voxel_indices] = color_index
                # Assign colors to each voxel index based on hierarchy
                for hierarchy_cluster, voxel_indices in hierarchy_voxel_dict.items():
                    color_index = int(hierarchy_cluster)
                    # rgb_index = newcmp(int(color_index/m*newcmp.N+1))[:-1]
                    colors[voxel_indices] = color_index

                # creat colors based on Leiden results
                colors_leiden = np.zeros(len(colors))

                for leiden_cluster, voxel_indices in leiden_clusters_dict.items():
                    color_index = int(leiden_cluster)
                    # rgb_index = newcmp(int(color_index/m*newcmp.N+1))[:-1]
                    colors_leiden[voxel_indices] = color_index

                # Visualize the hierarchy clusters
                # Get the unique Z values in voxel_df and sort them in ascending order
                unique_z = voxel_df['z'].unique()
                unique_z.sort()
                # unique_z = unique_z / voxel_z * voxel_x / rescale_ratio
                unique_z = [z for z in unique_z if z % 5 == 0]

                reps = voxel_df['name'].unique()

                # Create a subplot for each unique Z value, arranging the subplots vertically
                n_rows = len(unique_z)
                n_cols = len(reps)

                fig, axes = plt.subplots(
                    n_rows, n_cols, figsize=(5*n_cols+2, 5*n_rows))

                # Create a list to store the color patches for the legend
                color_patches = []

                for i, z in enumerate(unique_z):
                    # Select only the rows in voxel_df where 'z' is the current Z value
                    plane_df = voxel_df[voxel_df['z'] == z]
                    for j, b in enumerate(reps):
                        plane_batch_df = plane_df[plane_df['name'] == b]
                        # Create a scatter plot for the current Z plane, with the color of each point determined by the cluster membership
                        c = [newcmp(int(colors[i]))[:-1]
                             for i in plane_batch_df.index]
                        scatter = axes[i, j].scatter(
                            plane_batch_df['x'], plane_batch_df['y'], c=c, s=10)

                        axes[i, j].set_title(f'Hierarchy Clustering (Z = {z})')
                        # you should save this information from the image
                        axes[i, j].set_xlim([0, int(1024*rescale_ratio)])
                        axes[i, j].set_ylim([int(1024*rescale_ratio), 0])
                        plt.gca().invert_yaxis()

                # Create color patches for the legend
                for i in range(len(set(clusters))):
                    color_patches.append(mpatches.Patch(
                        color=newcmp(i), label=f'Cluster {i}'))

                # Add legend to the figure
                fig.legend(handles=color_patches, bbox_to_anchor=(1, 0.8))
                plt.savefig(
                    'results/hierarchy_bp_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format), bbox_inches='tight')

                # Visualize the Leiden clusters
                fig, axes = plt.subplots(
                    n_rows, n_cols, figsize=(5*n_cols+2, 5*n_rows))

                # Create a list to store the color patches for the legend
                color_patches = []

                for i, z in enumerate(unique_z):
                    # Select only the rows in voxel_df where 'z' is the current Z value
                    plane_df = voxel_df[voxel_df['z'] == z]
                    for j, b in enumerate(reps):
                        plane_batch_df = plane_df[plane_df['name'] == b]
                        # Create a scatter plot for the current Z plane, with the color of each point determined by the cluster membership
                        c = [newcmp_leiden(int(colors_leiden[i]))[:-1]
                             for i in plane_batch_df.index]
                        scatter = axes[i, j].scatter(
                            plane_batch_df['x'], plane_batch_df['y'], c=c, s=10)

                        axes[i, j].set_title(f'Leiden Clustering (Z = {z})')
                        # you should save this information from the image
                        axes[i, j].set_xlim([0, int(1024*rescale_ratio)])
                        axes[i, j].set_ylim([int(1024*rescale_ratio), 0])
                        plt.gca().invert_yaxis()

                # Create color patches for the legend
                for i in range(len(clusters)):
                    color_patches.append(mpatches.Patch(
                        color=newcmp_leiden(i), label=f'Cluster {i}'))

                # Add legend to the figure
                fig.legend(handles=color_patches, bbox_to_anchor=(1, 0.8))
                plt.savefig(
                    'results/leiden_bp_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format), bbox_inches='tight')

                # PAGA based on leiden
                leiden_cat = pd.Series(
                    leiden_partition.membership).astype('category')
                # leiden_cat = leiden_cat.astype('category')

                adata.obs['leiden'] = leiden_cat.values.astype(str)
                adata.uns['neighbors_sp'] = {'connectivities_key': 'connectivities_sp',
                                             'distances_key': 'distances_sp',
                                             'params': {'n_neighbors': n_neighbors,
                                                        # 'method': 'umap',
                                                        'random_state': 0,
                                                        'metric': 'euclidean'}}
                adata.obsp['connectivities_sp'] = fused_adjacency
                adata.obsp['distances_sp'] = fused_adjacency_distances

                adata.uns['leiden_colors'] = [mcolors.rgb2hex(
                    newcmp_leiden.colors[i, :-1]) for i in range(newcmp_leiden.N)]
                sc.tl.paga(adata, neighbors_key='neighbors_sp')
                sc.pl.paga(adata, node_size_scale=5, node_size_power=1.5,
                           save='_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.paga(adata, color='name', node_size_scale=5, node_size_power=1.5,
                           save='_leiden_name_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.paga(adata, color=adata.var_names, node_size_scale=5, node_size_power=1.5,
                           save='_leiden_genes_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

                # PAGA based on Hierarchy
                hierarchy_cat = np.zeros(voxel_df.shape[0]).astype(np.int64)
                for i, v in hierarchy_voxel_dict.items():
                    hierarchy_cat[v] = int(i)
                hierarchy_cat = pd.Series(hierarchy_cat).astype('category')

                adata.obs['hierarchy'] = hierarchy_cat.values.astype(str)
                adata.uns['hierarchy_colors'] = [mcolors.rgb2hex(
                    newcmp.colors[i, :-1]) for i in range(m)]

                # define the name to save the paga
                sc.tl.paga(adata, groups='hierarchy',
                           neighbors_key='neighbors_sp')
                sc.pl.paga(adata, node_size_scale=5, node_size_power=1.5,
                           save='_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.paga(adata, color='name', node_size_scale=5, node_size_power=1.5,
                           save='_hierarchy_name_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                # TODO: could return an error here, we can use for loop to avoid it
                sc.pl.paga(adata, color=adata.var_names, node_size_scale=5, node_size_power=1.5,
                           save='_hierarchy_genes_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

                # Forced directed layout
                forceatlas2 = ForceAtlas2(
                    # Behavior alternatives
                    outboundAttractionDistribution=False,  # Dissuade hubs
                    linLogMode=False,  # NOT IMPLEMENTED
                    adjustSizes=False,  # Prevent overlap (NOT IMPLEMENTED)
                    edgeWeightInfluence=1.0,
                    # Performance
                    jitterTolerance=1.0,  # Tolerance
                    barnesHutOptimize=True,
                    barnesHutTheta=1.2,
                    multiThreaded=False,  # NOT IMPLEMENTED
                    # Tuning
                    scalingRatio=2.0,
                    strongGravityMode=False,
                    gravity=1.0,
                    # Log
                    verbose=False,
                )
                np.random.seed(0)
                init_coords = np.random.random((fused_adjacency.shape[0], 2))
                iterations = 100
                positions = forceatlas2.forceatlas2(
                    fused_adjacency, pos=init_coords, iterations=iterations)
                positions = np.array(positions)
                adata.uns['draw_graph'] = {}
                adata.uns['draw_graph']['params'] = dict(
                    layout='fa', random_state=0)
                key_added = f'X_draw_graph_fa'
                adata.obsm[key_added] = positions
                sc.pl.draw_graph(adata, color=adata.var_names, show=False, neighbors_key='neighbors_sp',
                                 ncols=5, save='_FDG_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.draw_graph(adata, color='leiden', show=False, neighbors_key='neighbors_sp',
                                 ncols=5, save='_FDG_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.draw_graph(adata, color='hierarchy', show=False, neighbors_key='neighbors_sp',
                                 ncols=5, save='_FDG_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.draw_graph(adata, color='z', show=False, neighbors_key='neighbors_sp',
                                 ncols=5, save='_FDG_z_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.draw_graph(adata, color='name', show=False, neighbors_key='neighbors_sp',
                                 ncols=5, save='_FDG_name_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                # sc.pl.draw_graph(adata,color='condition', show=False, neighbors_key='neighbors_sp',ncols=5,save='_FDG_condition_{}_{}_{}_{}_{}.png'.format(hcr,day,pos,w, str(rescale_ratio)))

                # Meta Organoid
                # Sample RGB values
                # in matplot lime has rgb (0,1,0) according to X11/CSS4
                # I need different weights for different colors, otherwise I only see purple
                # we'd better put this color_weight setting to the place where we can tune it without running all the code again
                hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
                # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']

                # Color weights
                # color_weights = [5,0,0,30,0]
                color_weights = [0.7, 0.7, 0.7, 0.7, 0.7]

                rgb_values = []
                for color_name, color_weight in zip(hcr_colors, color_weights):
                    if color_name == 'green':
                        color_name = 'lime'
                    elif color_name == 'grey':
                        color_name = 'white'
                    # Extract the RGB values, ignoring alpha (transparency)
                    rgb_tuple = mcolors.to_rgba(color_name)[:3]
                    # print(rgb_tuple,color_weight)
                    rgb_tuple = np.array(rgb_tuple)*color_weight
                    # rgb_8bit = rgb_to_8bit(rgb_tuple)
                    rgb_values.append(rgb_tuple)

                # Perform element-wise multiplication and summation using broadcasting

                result = adata.raw.X.copy()

                for i in range(result.shape[1]):
                    p2, p98 = np.percentile(result[:, i], (2, 98))
                    result[:, i] = rescale_intensity(
                        result[:, i], in_range=(p2, p98))

                result = result[:, :, np.newaxis] * np.array(rgb_values)

                # Sum along the appropriate axis to get the new RGB values for each row
                # this step create new color
                fiji_colors = np.sum(result, axis=1)

                fiji_colors = np.clip(fiji_colors, 0, 1)
                fiji_colors_hex = [mcolors.to_hex(rgb) for rgb in fiji_colors]
                adata.obs['meta_organoid'] = fiji_colors_hex
                # Get position data
                positions = adata.obsm['X_draw_graph_fa']

                colors = adata.obs['meta_organoid']

                # Create a single subplot
                fig, ax = plt.subplots(
                    1, 1, figsize=(10, 8), facecolor='black')

                # if edge:
                # Convert sparse matrix to COO format for faster indexing
                coo_matrix = adata.obsp['connectivities_sp'].tocoo()
                row_indices = coo_matrix.row
                col_indices = coo_matrix.col

                # Create a list of edge positions
                edge_positions = np.column_stack(
                    (positions[row_indices], positions[col_indices]))

                # Reshape edge_positions to 2D array
                edge_positions = edge_positions.reshape(-1, 2, 2)

                # Create a LineCollection object for efficient edge plotting
                edges = LineCollection(
                    edge_positions, colors='white', linewidths=0.05, alpha=0.01)

                # Draw edges
                ax.add_collection(edges)

                # Draw nodes
                ax.scatter(positions[:, 0], positions[:, 1],
                           c=colors, alpha=1, s=5)

                # Set x-axis and y-axis labels
                ax.set_xlabel('FA1', color='white', fontsize=12)
                ax.set_ylabel('FA2', color='white', fontsize=12)

                # Set the plot title
                # ax.set_title('Meta Organoid', color='white', fontsize=14,fontweight='bold',x=0.01,y=1)

                # Add text in a different color to the top left of the plot
                # hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
                # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']
                for i, (text_color, text_content) in enumerate(zip(hcr_colors, adata.var_names)):
                    if text_color == 'green':
                        text_color = 'lime'
                #     elif text_color == 'grey':
                #         text_color = 'white'
                # white can also be the case the voxle has very high expression level
                    plt.text(0.99, 1-0.03*i, text_content, transform=ax.transAxes,
                             color=text_color, fontsize=14, fontweight='bold')

                # Hide the axes
                ax.axis('off')

                # Save the plot
                plt.savefig('results/meta_organoid_{}_{}_{}_{}_{}_{}_{}.png'.format(*
                            file_name_format), facecolor='black')
                # Show the plot
                # plt.show()

                # for each gene save the meta-organoid
                for idx_color in range(len(adata.var_names)):
                    # Sample RGB values
                    # in matplot lime has rgb (0,1,0) according to X11/CSS4
                    # I need different weights for different colors, otherwise I only see purple
                    # we'd better put this color_weight setting to the place where we can tune it without running all the code again
                    hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
                    # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']

                    # Color weights
                    # color_weights = [5,0,0,30,0]
                    color_weights = [0, 0, 0, 0, 0]
                    color_weights[idx_color] = 1

                    rgb_values = []
                    for color_name, color_weight in zip(hcr_colors, color_weights):
                        if color_name == 'green':
                            color_name = 'lime'
                        elif color_name == 'grey':
                            color_name = 'white'
                        # Extract the RGB values, ignoring alpha (transparency)
                        rgb_tuple = mcolors.to_rgba(color_name)[:3]
                        # print(rgb_tuple,color_weight)
                        rgb_tuple = np.array(rgb_tuple)*color_weight
                        # rgb_8bit = rgb_to_8bit(rgb_tuple)
                        rgb_values.append(rgb_tuple)

                    # Perform element-wise multiplication and summation using broadcasting
                    # how do decide 200?
                    result = adata.raw.X.copy()

                    for i in range(result.shape[1]):
                        p2, p98 = np.percentile(result[:, i], (2, 98))
                        result[:, i] = rescale_intensity(
                            result[:, i], in_range=(p2, p98))

                    result = result[:, :, np.newaxis] * np.array(rgb_values)

                    # Sum along the appropriate axis to get the new RGB values for each row
                    fiji_colors = np.sum(result, axis=1)
                    fiji_colors = np.clip(fiji_colors, 0, 1)
                    fiji_colors_hex = [mcolors.to_hex(
                        rgb) for rgb in fiji_colors]

                    adata.obs['meta_organoid'] = fiji_colors_hex

                #     # sampled meta-organoid
                #     # Get position data
                #     adata_copy = adata.copy()
                #     if adata.shape[0] > 100000:
                #         num_locations = 10000
                #         sc.pp.subsample(adata_copy, n_obs=num_locations)
                #     else:
                #         sc.pp.subsample(adata_copy,fraction=0.1)
                #     sub_positions = adata_copy.obsm['X_draw_graph_fa']

                #     sub_colors = adata_copy.obs['meta_organoid']

                #     # Create a single subplot
                #     fig, ax = plt.subplots(1, 1, figsize=(10, 8),facecolor='black')

                #     # if edge:
                #     # Convert sparse matrix to COO format for faster indexing
                #     coo_matrix = adata_copy.obsp['connectivities_sp'].tocoo()
                #     row_indices = coo_matrix.row
                #     col_indices = coo_matrix.col

                #     # Create a list of edge positions
                #     edge_positions = np.column_stack((sub_positions[row_indices], sub_positions[col_indices]))

                #     # Reshape edge_positions to 2D array
                #     edge_positions = edge_positions.reshape(-1, 2, 2)

                #     # Create a LineCollection object for efficient edge plotting
                #     edges = LineCollection(edge_positions, colors='white', linewidths=0.07, alpha=0.15)

                #     # Draw edges
                #     ax.add_collection(edges)

                #     # Draw nodes
                #     ax.scatter(sub_positions[:, 0], sub_positions[:, 1], c=sub_colors, alpha=1, s=5)

                    # Get position data
                    positions = adata.obsm['X_draw_graph_fa']

                    colors = adata.obs['meta_organoid']

                    # Create a single subplot
                    fig, ax = plt.subplots(
                        1, 1, figsize=(10, 8), facecolor='black')

                    # if edge:
                    # Convert sparse matrix to COO format for faster indexing
                    # coo_matrix = adata.obsp['connectivities_sp'].tocoo()
                    # row_indices = coo_matrix.row
                    # col_indices = coo_matrix.col

                    # # Create a list of edge positions
                    # edge_positions = np.column_stack((positions[row_indices], positions[col_indices]))

                    # # Reshape edge_positions to 2D array
                    # edge_positions = edge_positions.reshape(-1, 2, 2)

                    # # Create a LineCollection object for efficient edge plotting
                    # edges = LineCollection(edge_positions, colors='white', linewidths=0.05, alpha=0.01)

                    # # Draw edges
                    # ax.add_collection(edges)

                    # Draw nodes
                    ax.scatter(positions[:, 0], positions[:, 1],
                               c=colors, alpha=1, s=5)

                    # Set x-axis and y-axis labels
                    ax.set_xlabel('FA1', color='white', fontsize=12)
                    ax.set_ylabel('FA2', color='white', fontsize=12)

                    # Set the plot title
                    # ax.set_title('Meta Organoid', color='white', fontsize=14,fontweight='bold',x=0.01,y=1)

                    # Add text in a different color to the top left of the plot
                    # hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
                    # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']
                    text_color = hcr_colors[idx_color]
                    if text_color == 'green':
                        text_color = 'lime'
                    text_content = adata.var_names[idx_color]
                    plt.text(0.97, 1-0.03, text_content, transform=ax.transAxes,
                             color=text_color, fontsize=14, fontweight='bold')

                    # Hide the axes
                    ax.axis('off')

                    # Save the plot
                    plt.savefig('results/meta_organoid_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(
                        text_content, *file_name_format), facecolor='black')

                # best color weight searching
                for w_color in range(1, 10, 1):
                    w_color = w_color/10.
                    w_color = w_color * 3
                    # Sample RGB values
                    # in matplot lime has rgb (0,1,0) according to X11/CSS4
                    # I need different weights for different colors, otherwise I only see purple
                    # we'd better put this color_weight setting to the place where we can tune it without running all the code again
                    hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
                    # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']

                    # Color weights
                    # color_weights = [5,0,0,30,0]
                    color_weights = [w_color, w_color,
                                     w_color, w_color, w_color]

                    rgb_values = []
                    for color_name, color_weight in zip(hcr_colors, color_weights):
                        if color_name == 'green':
                            color_name = 'lime'
                        elif color_name == 'grey':
                            color_name = 'white'
                        # Extract the RGB values, ignoring alpha (transparency)
                        rgb_tuple = mcolors.to_rgba(color_name)[:3]
                        # print(rgb_tuple,color_weight)
                        rgb_tuple = np.array(rgb_tuple)*color_weight
                        # rgb_8bit = rgb_to_8bit(rgb_tuple)
                        rgb_values.append(rgb_tuple)

                    # Perform element-wise multiplication and summation using broadcasting
                    # how do decide 200?
                    result = adata.raw.X.copy()

                    for i in range(result.shape[1]):
                        p2, p98 = np.percentile(result[:, i], (2, 98))
                        result[:, i] = rescale_intensity(
                            result[:, i], in_range=(p2, p98))

                    result = result[:, :, np.newaxis] * np.array(rgb_values)

                    # Sum along the appropriate axis to get the new RGB values for each row
                    fiji_colors = np.sum(result, axis=1)
                    fiji_colors = np.clip(fiji_colors, 0, 1)
                    fiji_colors_hex = [mcolors.to_hex(
                        rgb) for rgb in fiji_colors]

                    adata.obs['meta_organoid'] = fiji_colors_hex

                    # Get position data
                    positions = adata.obsm['X_draw_graph_fa']

                    colors = adata.obs['meta_organoid']

                    # Create a single subplot
                    fig, ax = plt.subplots(
                        1, 1, figsize=(10, 8), facecolor='black')

                    # if edge:
                    # Convert sparse matrix to COO format for faster indexing
                    # coo_matrix = adata.obsp['connectivities_sp'].tocoo()
                    # row_indices = coo_matrix.row
                    # col_indices = coo_matrix.col

                    # # Create a list of edge positions
                    # edge_positions = np.column_stack((positions[row_indices], positions[col_indices]))

                    # # Reshape edge_positions to 2D array
                    # edge_positions = edge_positions.reshape(-1, 2, 2)

                    # # Create a LineCollection object for efficient edge plotting
                    # edges = LineCollection(edge_positions, colors='white', linewidths=0.05, alpha=0.01)

                    # # Draw edges
                    # ax.add_collection(edges)

                    # Draw nodes
                    ax.scatter(positions[:, 0], positions[:, 1],
                               c=colors, alpha=1, s=5)

                    # Set x-axis and y-axis labels
                    ax.set_xlabel('FA1', color='white', fontsize=12)
                    ax.set_ylabel('FA2', color='white', fontsize=12)

                    # Set the plot title
                    # ax.set_title('Meta Organoid', color='white', fontsize=14,fontweight='bold',x=0.01,y=1)

                    # Add text in a different color to the top left of the plot
                    # hcr_colors = ['green', 'blue', 'grey', 'cyan', 'magenta']
                    # gene_names = ['RAX', 'TCF7L2', 'BMP4', 'WNT8B', 'SOX2']
                    for i, (text_color, text_content) in enumerate(zip(hcr_colors, adata.var_names)):
                        if text_color == 'green':
                            text_color = 'lime'
                    #     elif text_color == 'grey':
                    #         text_color = 'white'
                    # white can also be the case the voxle has very high expression level
                        plt.text(0.99, 1-0.03*i, text_content, transform=ax.transAxes,
                                 color=text_color, fontsize=14, fontweight='bold')

                    # Hide the axes
                    ax.axis('off')

                    # Save the plot
                    plt.savefig('results/meta_organoid_{}_{}_{}_{}_{}_{}_{}_colorw{:.1f}.png'.format(
                        *file_name_format, w_color), facecolor='black')

                # draw_graph_with_color(adata, adata.var_names, save='_{}_{}_{}_{}.png'.format(hcr,day,pos,w))
                # draw_graph_with_color(adata, 'leiden', save='_leiden_{}_{}_{}_{}.png'.format(hcr,day,pos,w))
                # draw_graph_with_color(adata, 'hierarchy', save='_hierarchy_{}_{}_{}_{}.png'.format(hcr,day,pos,w))
                # draw_graph_with_color(adata, 'name', save='_name_{}_{}_{}_{}.png'.format(hcr,day,pos,w))

                # Downstream analysis
                # 1. Barplot: clusters proportions (hierarchy and leiden)

                # barplot with entropy

                # Hierarchy
                adata_df = pd.DataFrame(
                    data=adata.obs['name'], index=adata.obs_names, columns=['name'])
                adata_df['hierarchy'] = adata.obs['hierarchy']
                hierarchy_proportions = adata_df.groupby(['name', 'hierarchy']).size().unstack(
                    fill_value=0).div(adata_df.groupby('name').size(), axis=0)
                hierarchy_proportions['entropy'] = hierarchy_proportions.apply(
                    lambda x: entropy(x, base=2), axis=1)
                plot_clusters_with_entropy_annotation(hierarchy_proportions, 'Hierarchy Cluster Proportions and Entropy for Each Organoid',
                                                      'results/barplot_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

                # Leiden
                adata_df['leiden'] = adata.obs['leiden']
                leiden_proportions = adata_df.groupby(['name', 'leiden']).size().unstack(
                    fill_value=0).div(adata_df.groupby('name').size(), axis=0)
                leiden_proportions['entropy'] = leiden_proportions.apply(
                    lambda x: entropy(x, base=2), axis=1)
                plot_clusters_with_entropy_annotation(leiden_proportions, 'Leiden Cluster Proportions and Entropy for Each Organoid',
                                                      'results/barplot_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

                # 2. Matrixplot & Dotplot: gene expression across clusters
                sc.pl.matrixplot(adata, adata.var_names, 'hierarchy', standard_scale='var',
                                 save='hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.matrixplot(adata, adata.var_names, 'leiden', standard_scale='var',
                                 save='leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.dotplot(adata, adata.var_names, groupby='hierarchy', standard_scale='var',
                              save='hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                sc.pl.dotplot(adata, adata.var_names, groupby='leiden', standard_scale='var',
                              save='leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

                # Gene-gene correlation: while we only have five genes in HCR
                plt.figure(figsize=(8, 6))
                gene_names = adata.var_names
                # Calculate the Spearman correlation between all pairs of genes
                correlation_matrix, _ = spearmanr(adata.X)
                # cmap='PRGn'
                sns.heatmap(correlation_matrix, cmap='PRGn', center=0,
                            xticklabels=gene_names, yticklabels=gene_names, annot=True, fmt=".2f")
                # plt.xlabel("Genes")
                # plt.ylabel("Genes")
                plt.title("Gene-Gene Correlation Matrix")
                plt.savefig(
                    'results/gene-gene_cor_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))

                # 3. Violinplot: gene expression across clusters
                # Plot expression distribution across multiple organoids
                for gene in adata.var_names:

                    plt.figure(figsize=(10, 6))
                    sns.violinplot(x='name', y=gene, data=adata.obs.join(
                        adata.to_df()), inner='quartile')
                    plt.title(
                        f'Gene Expression Distribution for {gene} Across Organoids')
                    plt.savefig(
                        'results/violin_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
                    # plt.show()
                    plt.close()

                # Convert the raw data to a DataFrame
                raw_df = pd.DataFrame(
                    adata.raw.X, columns=adata.raw.var_names, index=adata.obs.index)

                # Join the observational data with the raw data
                combined_df = adata.obs.join(raw_df)
                for gene in adata.var_names:
                    plt.figure(figsize=(10, 6))
                    sns.violinplot(x='name', y=gene,
                                   data=combined_df, inner='quartile')
                    plt.title(
                        f'Raw Gene Expression Distribution for {gene} Across Organoids')
                    plt.savefig(
                        'results/violin_raw_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(gene, *file_name_format))
                    # plt.show()
                    plt.close()

                # 4. Heatmap: gene expression across clusters
                # Add cluster and organoid information to the DataFrame
                adata_df = adata.to_df()
                adata_df['hierarchy'] = adata.obs['hierarchy']
                adata_df['leiden'] = adata.obs['leiden']
                adata_df['name'] = adata.obs['name']

                # Aggregate the data
                # Here we're computing the mean expression of each gene across each cluster and organoid combination
                agg_data = adata_df.groupby(['hierarchy', 'name']).mean()

                # Draw the heatmap
                plt.figure(figsize=(15, 8))
                # viridis
                sns.heatmap(agg_data.T, cmap='viridis', cbar_kws={
                            'label': 'Expression Level'}, annot=True)
                plt.title('Gene Expression Across Clusters and Organoids')
                plt.tight_layout()
                plt.savefig(
                    'results/heatmap_hierarchy_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                plt.close()

                # Aggregate the data
                # Here we're computing the mean expression of each gene across each cluster and organoid combination
                agg_data = adata_df.groupby(['leiden', 'name']).mean()

                # Draw the heatmap
                plt.figure(figsize=(20, 8))
                # viridis
                sns.heatmap(agg_data.T, cmap='viridis', cbar_kws={
                            'label': 'Expression Level'}, annot=True)
                plt.title('Gene Expression Across Clusters and Organoids')
                plt.tight_layout()
                plt.savefig(
                    'results/heatmap_leiden_{}_{}_{}_{}_{}_{}_{}.png'.format(*file_name_format))
                plt.close()

                # 5. the coefficient of variation (CV) of each gene
                df_cv = pd.DataFrame(columns=adata.var_names)
                for name in np.unique(adata.obs['name']):
                    cv_values = np.std(adata.raw.X[adata.obs['name'] == name], axis=0) / np.mean(
                        adata.raw.X[adata.obs['name'] == name], axis=0)
                    df_cv.loc[name] = cv_values

                cv_values_all = np.std(adata.raw.X, axis=0) / \
                    np.mean(adata.raw.X, axis=0)
                print(cv_values_all)
                df_cv.loc['All'] = cv_values_all
                df_cv.to_csv(
                    'results/cv_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format))

                # 6. the Entropy of each gene
                df_entropy = pd.DataFrame(columns=adata.var_names)

                for name in np.unique(adata.obs['name']):
                    # Calculate Shannon index for each gene within an organoid
                    pk = adata.raw.X[adata.obs['name'] == name]
                    pk = (pk-np.min(pk, axis=0)) / \
                        (np.max(pk, axis=0)-np.min(pk, axis=0))
                    shannon_indices = entropy(pk, axis=0)
                    df_entropy.loc[name] = shannon_indices

                pk = adata.raw.X
                pk = (pk-np.min(pk, axis=0)) / \
                    (np.max(pk, axis=0)-np.min(pk, axis=0))
                shannon_indices = entropy(pk, axis=0)
                df_entropy.loc['All'] = shannon_indices
                df_entropy.to_csv(
                    'results/entropy_{}_{}_{}_{}_{}_{}_{}.csv'.format(*file_name_format))

                # 7. save the adata file
                adata.write_h5ad(
                    'adata_{}_{}_{}_{}_{}_{}_{}.h5ad'.format(
                        *file_name_format),
                    compression=hdf5plugin.FILTERS["zstd"]
                )


if __name__ == '__main__':
    # example
    meta_organoid_and_cluster(
        "/cluster/project/treutlein/USERS/yihliu/data", "/cluster/project/treutlein/USERS/yihliu")
